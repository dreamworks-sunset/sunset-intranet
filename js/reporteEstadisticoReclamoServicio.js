/*$(function () {
    getMorris('line', 'line_chart');
    getMorris('bar', 'bar_chart');
    getMorris('area', 'area_chart');
    getMorris('donut', 'donut_chart');
});


function getMorris(type, element) {
    if (type === 'bar') {
        Morris.Bar({
            element: element,
            data: [{
                    'period': '2017 Q3',
                    'reductor': 1,
                    'antiestres': 2,
                    'deportivos': 1,
                }, {
                    'period': '2017 Q3',
                    'reductor': 3,
                    'antiestres': 3,
                    'deportivos': 0,
                }, {
                    'period': '2017 Q4',
                    'reductor': 2,
                    'antiestres': 4,
                    'deportivos': 2,
                }, {
                    'period': '2018 Q1',
                    'reductor': 0,
                    'antiestres': 1,
                    'deportivos': 1,
                }, {
                    'period': '2018 Q2',
                    'reductor': 1,
                    'antiestres': 0,
                    'deportivos': 0,
                }, {
                    'period': '2018 Q2',
                    'reductor': 0,
                    'antiestres': 1,
                    'deportivos': 0,
                }, {
                    'period': '2018 Q3',
                    'reductor': 0,
                    'antiestres': 1,
                    'deportivos':1,
                }, {
                    'period': '2018 Q4',
                    'reductor': 1,
                    'antiestres': 2,
                    'deportivos':0,
                }],
            xkey: 'period',
            ykeys: ['reductor', 'antiestres','deportivos'],
            labels: ['reductor', 'Antiestrés', 'Deportivos'],
            lineColors: ['rgb(233, 30, 99)', 'rgb(0, 188, 212)','rgb(0, 18, 212)'],
            lineWidth: 3
        });
    } 
}
*/
$(document).ready(function () {

     consultarServicio();
    $("#tablaReporte").hide();
    
    $("#grafica").hide();


    $('#btnGenerar').click(function() {
        $("#tablaReporte").show()
        $("#grafica").show();
        $('#bar_chart').empty();
        consultarReclamoServicio();
       
        
    });
     $('#btnCancelar').click(function() {
        $("#tablaReporte").hide();
        $("#grafica").hide();
    });
});

$(function () {
   

    $('#btnGenerar').click(function() {
        $('#bar_chart').empty();
    });

});


function getMorris(type, element, datos,tipo, colores) {
    if (type === 'bar') {
        Morris.Bar({
            element: element,
            data: datos,
            xkey: 'periodo',
            ykeys : tipo,
            labels: tipo,
            colors: colores,
            
        });
    }
}

function consultarReclamoServicio() {
    $.ajax(
    {
        url: HOST + '/api/vista_reporte_reclamos_por_servicio',
        type: 'GET',
        success: function(result) {
            //Vaciamos la tabla
            //Destruir la tabla para luego reinicializarla más adelante
            if($.fn.DataTable.isDataTable("#tablaReporteReclamoServicio")) {
                $('#tablaReporteReclamoServicio').DataTable().clear().destroy();
            }
            //$('#tablaReporteReclamoServicio tbody').empty();
            
            var tiposRespuesta = []; //arreglo que almacena la cantidad de respuestas por tipo
            var idsTiposRespuesta = {}; //diccionario para almacenar los id de tipo de respuesta con sus valores
            var colors = {}; //dicionario para almacenar los colores de cada tipo de respuesta
            var fechas = {};
            var idServicio = $('#cmbServicio').val();
            var fecha_ini = formatFechaDMYToYMD($('#dtpFechaInicio').val());
            var fecha_fin = formatFechaDMYToYMD($('#dtpFechaFin').val());
               
            $.each(result.data, function(index, data) {
                var fecha_creacion = new Date(data.fecha);

                //Filtrar por fecha
                if(fecha_creacion.getTime() < fecha_ini.getTime() || fecha_creacion.getTime() > fecha_fin.getTime()) {
                    return true; //continúa en la siguiente iteración
                }

                //Filtrar por servicio
                if(idServicio != 0 && $('#cmbServicio option:selected').text() != data.nombre) {
                    return true; //continúa en la siguiente iteración
                }

                $('#tablaReporteReclamoServicio tbody').append(
                    '<tr>' +
                        '<td>' +
                            formatFecha(data.fecha) +
                        '</td>' +
                        '<td>' +
                            data.tipo +
                        '</td>' +
                         '<td>' +
                            data.nombre +
                        '</td>' +
                        '<td>' +
                            data.cantidad +
                        '</td>' +
                        
                    '</tr>'
                );

                
                if(tiposRespuesta[data.nombre] == undefined) {
                    tiposRespuesta[data.nombre] = 1.0;
                } else {
                    tiposRespuesta[data.nombre]++;
                }
                var fecha = new Date(data.fecha);
                var fechaAMD = fecha.getFullYear() + '-' + ((fecha.getMonth() + 1 < 10) ? '0' + (fecha.getMonth() + 1) : (fecha.getMonth() + 1)) + '-' + ((fecha.getDate() < 10) ? '0' + fecha.getDate() : fecha.getDate());
                fechas[fechaAMD] = fechaAMD;
                idsTiposRespuesta[data.nombre] = tiposRespuesta[data.nombre];
                colors[fechaAMD] = getRandomColor();
                
            });

            //Aquí se reinicializa la tabla
            $(".js-basic-example").dataTable({
                responsive: true
            });

           //console.log(idsTiposRespuesta)
            var datos = []; //arreglo con los datos que se le pasarán al gráfico
            console.log('Estas son las fechas');
            console.log(fechas)
            $.each(fechas, function(key, value) {
                var obj = {};
                obj['periodo'] = key;
                console.log('key =' + key) 
                $.each(result.data, function(key1, value1) {
                    if(key == formatFechaAMD(value1.fecha)) {
                        if(obj[value1.nombre] == undefined) {
                            obj[value1.nombre] = 1;
                        } else {
                            obj[value1.nombre]++;
                        }
                    }
                    //console.log(key2)
                })
                datos.push(obj)

            });
            console.log('Estos son los datos')
            console.log(datos)

            getMorris('bar','bar_chart', datos, Object.keys(idsTiposRespuesta), Object.values(colors));
        },
        error: function(xhr) {
            if(xhr.responseJSON == undefined) {
                swal('Ocurrió un error', 'No se pudo conectar con el servidor', 'error');
                return;
            }
            swal('Ocurrió un error',
                    xhr.status + ' ' + xhr.statusText + ': ' +
                    xhr.responseJSON.data.message,
                    'error');
        }
    });
}

function consultarServicio() {
    $('#cmbServicio').html('<option value="0" selected >Todos</option>');

    $.ajax(
    {
        url: HOST + '/api/reclamo',
        type: 'GET',
        success: function(result) {

            $.each(result.data, function(index, data) {
                var servicio = data.orden_servicio.solicitud.servicio;
                if($('#cmbServicio option[value="' + servicio.id + '"]').length == 0) {
                    $('#cmbServicio').append('<option data-id-servicio="'+ servicio.id +'" value="' + servicio.id + '">' + servicio.nombre + '</option>');
                }
                
            });
            $('#cmbServicio').selectpicker('refresh');
        },
        error: function(xhr) {
            if(xhr.responseJSON == undefined) {
                swal('Ocurrió un error', 'No se pudo conectar con el servidor', 'error');
                return;
            }
            swal('Ocurrió un error',
                    xhr.status + ' ' + xhr.statusText + ': ' +
                    xhr.responseJSON.data.message,
                    'error');
        }

    });
}

function formatFechaDMYToYMD(date) {
    var fecha = date.split('/');
    var fec = new Date(fecha[2], parseInt(fecha[1]) - 1, fecha[0]);
    return fec;
}

function getRandomColor() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 3; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}


function formatFecha(myDate) {
    var date = new Date(myDate).toLocaleDateString('es-ve', {
        year: 'numeric',
        month: '2-digit',
        day: '2-digit'

    });

    return date;
}

function formatFechaAMD(myDate) {
    var date = new Date(myDate);

    return date.getFullYear() + '-' + ((date.getMonth() + 1 < 10) ? '0' + (date.getMonth() + 1) : (date.getMonth() + 1)) + '-' + ((date.getDate() < 10) ? '0' + date.getDate() : date.getDate());
}
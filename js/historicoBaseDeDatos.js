$(document).ready(function() {
    $("#fechas").hide();


    $('#selOperacion').change(function() {

      if($('#selOperacion option:selected').val()==5)
      {
        $("#fechas").show();
      }
      else{
         $("#fechas").hide();

      }
  })

$('#btnProcesar').on('click', function(){
        
    llenarBarra();
})
});
let opcion="";
let gestion="";

function validarForm(){
    if($('#selOperacion').val()==0){        
        swal( `Operación`);
        return false;
    }    
    opcion = $('select[name="operacion"] option:selected').text();
    if($('#check1').prop('checked')){
        gestion="Gestión de Cliente";
    }else if($('#check2').prop('checked')){
        gestion="Gestión de Servicio";
    }else if($('#check3').prop('checked')){
        gestion="Gestión de Post-Servicio";
    }else if($('#check4').prop('checked')){
        gestion="Gestión de Escucha al Cliente";
    }else{
        gestion="Todas las Gestiones";
    }
    return true;
}

function llenarBarra(){
if(validarForm()){
$('#myModal').modal('show');
    let ancho=0;
    for (var i=0; i<100; i++){                 
            $(".progress-bar").delay(50).show(400, function() {
                ancho=ancho+1;
                $("#barraProgreso").css('width', ancho + '%');
                if(ancho==100){
                    $('#btnCerrar').css('display', 'inline')
                    swal( opcion + " de " + gestion + ` Listo`);
                }
            });
        } 
}

}


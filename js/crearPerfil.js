

let valuesPerfil = {}; //Diccionario donde se almacenarán los valores parametros de un cliente 
let valuesPerfilPorEliminar = {} //Diccionario donde se almacenarán los valores parametrosde un cliente que serán removidos de la base de datos
var abreviatura="";

$(document).ready(function() {

        consultarTipoParametros();

        //console.log(localStorage.mensaje);

        consultarCliente(localStorage.idCliente);

    $('#cmbTipoParametro').change(function() {

        $('#valorNumerico').hide();
        var idTipoParametro = $(this).val();
        consultarParametrosPorTipoParametro(idTipoParametro);
        $('#cmbValorParametro').html('<option value="" selected disabled hidden>Valor Parámetro</option>');
        $('#cmbValorParametro').selectpicker('refresh');
        

    });
    $('#cmbParametro').change(function() {
        var idValorParametro = $(this).val();
        consultarValorParametroPorParametro(idValorParametro);

    });

    $('#cmbValorParametro').change(function() {
        var idClasificacion = $(this).val();
        consultarClasificacionValorParametro(idClasificacion);

    });


    $('#btnAnadirValor').click(function() {
        var tipoParametro = $('#cmbTipoParametro option:selected');
        var parametro = $('#cmbParametro option:selected');
        var valorParametro = $('#cmbValorParametro option:selected');
        var valor = $('#txtValor').val();

        var idValorParametro = valorParametro.val();      
        console.log(idValorParametro);
        consultarUnidadPorValorParametro(idValorParametro);


        // Voy a añadir un insumo, elimino la fila "No se han añadido insumos"
        $('#sinValor').remove();
        

        // Añade un insumo a la tabla
        $('#tablaPerfil tbody').append(
            '<tr data-id-tipo-parametro="' + tipoParametro.val() + '" data-id-parametro="' +
            parametro.val() + '" data-id-valor-parametro="' + valorParametro.val() + '">' +
                '<td>' + tipoParametro.text() + '</td>' +
                '<td>' + parametro.text() + '</td>' +
                '<td>' + valorParametro.text() + '</td>' +
                '<td>' + valor + '</td>'+
                '<td>' + abreviatura + '</td>' +
                ((valor=="") ?  '<td class="centrado"> <button type="button" rel="tooltip"  title="Eliminar" class="btn btn-danger btn-circle waves-effect waves-circle waves-float perfil-tabla" onclick=""> <i class="material-icons">delete</i> </button> </td>' : '<td class="centrado"> <button type="button" rel="tooltip"  title="Editar" class="btn btn-primary btn-circle waves-effect waves-circle waves-float perfil-tabla" onclick=""> <i class="material-icons">edit</i> </button></td>')+
               // (($('#valorNumerico').is(':hidden')) ?  '<td></td> <td class="centrado"> <button type="button" rel="tooltip"  title="Eliminar" class="btn btn-danger btn-circle waves-effect waves-circle waves-float perfil-tabla" onclick=""> <i class="material-icons">delete</i> </button> </td>' :  '<td>' + valor + ' </td> <td class="centrado"> <button type="button" rel="tooltip"  title="Editar" class="btn btn-primary btn-circle waves-effect waves-circle waves-float perfil-tabla" onclick=""> <i class="material-icons">edit</i> </button></td>')+
                //'<td>' + "hola" + '</td>' +

            '</tr>'
        );
        limpiarValorParametro();

            var idValorParametroPorAnadir = $('#tablaPerfil tbody tr:last-child').data('id-valor-parametro');
            var idClientePorAnadir = localStorage.idCliente;
            if (valor=="")
            {
                 var valorPorAnadir=null;

            }else{
            var valorPorAnadir = valor;
                    }
            //console.log(idClientePorAnadir);

            valuesPerfil[idValorParametroPorAnadir] = {
                id_valor_parametro: idValorParametroPorAnadir,
                valor_concreto: valorPorAnadir
            }
            valuesPerfilPorEliminar[idValorParametroPorAnadir] = false;





        $('[rel="tooltip"]').tooltip();                                         

        // Manejo del evento 'click' que elimina una fila de la tabla
        $('.perfil-tabla').click(function() {
            // Elimina la fila que contiene al icono 'delete' cliqueado
            $(this).parent().parent().remove();

            // Este if evalúa si el tbody de la tabla se quedó sin filas
            if($('#tablaPerfil tbody tr').length === 0) {
                $('#tablaPerfil tbody').html(
                    '<tr id="sinValor">' +
                        '<td class="centrado" colspan="5">' +
                            '<i>No se han añadido datos al perfil</i>' +
                        '</td>' +
                    '</tr>'
                );
            }
            // Fin if
        });
        // Fin $('.perfil-tabla').click(...)
    });
  // Evento click del botón para guardar el detalle de la sesión
    $('#btnGuardarPerfilCliente').click(function() {

        var form = document.getElementById("formPerfil");
        var putData = new FormData(form);

        var idCliente = localStorage.idCliente;
        // perfil
        var valoresParametros = Object.values(valuesPerfil);
        var valoresPerfil = [];
        valoresParametros.forEach(valor => {
            if(valor) {
                valoresPerfil.push(valor);
            }
        });
        console.log('perfil =',valoresPerfil);
        // fin perfil
    
        putData.append('perfil', JSON.stringify(valoresPerfil));
        $.ajax(
            {
                url: HOST + '/api/cliente/'+idCliente,
                type: 'PUT',
                dataType: 'json',
                data: putData,
                processData: false,
                contentType: false,
                success: function(data) {
                       swal({
                    title: "¡Perfil Creado!",
                    type: "success",
                    text: "En breve será redirigido a la página de 'Clientes'",
                    timer: 2000
                }, function () {                
                    window.location.replace('Clientes.html');
                });
                    /*limpiarCampos();
                    consultarServicio();*/
                },
                error: function(xhr) {
                    if(xhr.responseJSON == undefined) {
                        swal('Ocurrió un error', 'No se pudo conectar con el servidor', 'error');
                        return;
                    }
                    swal('Ocurrió un error',
                            xhr.status + ' ' + xhr.statusText + ': ' +
                            xhr.responseJSON.data.message,
                            'error');
                }
            }
        );
       


    });

});

function consultarTipoParametros() {
     
    $.ajax(
    {
        url: HOST + '/api/tipo_parametro',
        type: 'GET',
        success: function(result) {
            $('#cmbTipoParametro').html('<option value="" selected disabled hidden>Tipo Parámetro</option>');
            $.each(result.data, function(index, data) {
                $('#cmbTipoParametro').append('<option data-id-tipo-parametro="' + data.id_tipo_parametro + '" value="' + data.id + '">' + data.descripcion + '</option>')
            });
            $('#cmbTipoParametro').selectpicker('refresh');
        },
        error: function(xhr) {
            console.log('Ocurrió un error: ' + xhr.status + ' ' + xhr.statusText);
        }
      }
    );

}

function consultarParametrosPorTipoParametro(id) {
       
    $.ajax(
    {

        url: HOST + '/api/tipo_parametro/'+id,
        type: 'GET',
        success: function(result) {
            $('#cmbParametro').html('<option value="" selected disabled hidden>Parámetro</option>');
            $.each(result.data.parametro, function(index, data) {
                $('#cmbParametro').append('<option value="' + data.id + '">' + data.descripcion + '</option>')
            });
            $('#cmbParametro').selectpicker('refresh');
        },
        error: function(xhr) {
            console.log('Ocurrió un error: ' + xhr.status + ' ' + xhr.statusText);
        }
      }
    );

}

function consultarValorParametroPorParametro(id) {
       
    $.ajax(
    {

        url: HOST + '/api/parametro/'+id,
        type: 'GET',
        success: function(result) {
            $('#cmbValorParametro').html('<option value="" selected disabled hidden>Valor Parámetro</option>');
            $.each(result.data.valor_parametro, function(index, data) {
                $('#cmbValorParametro').append('<option value="' + data.id + '">' + data.descripcion + '</option>')
            });
            $('#cmbValorParametro').selectpicker('refresh');

        },
        error: function(xhr) {
            console.log('Ocurrió un error: ' + xhr.status + ' ' + xhr.statusText);
        }
      }

    );

}
function consultarUnidadPorValorParametro(id){
       
    $.ajax(
    {

        url: HOST + '/api/valor_parametro/'+id,
        type: 'GET',
        async: false,
        success: function(result) {

            if(result.data.id_unidad!=null){
            abreviatura = result.data.unidad.abreviatura 
           console.log(abreviatura);
       }
                   
                 
        },
        error: function(xhr) {
            console.log('Ocurrió un error: ' + xhr.status + ' ' + xhr.statusText);
        }
      }

    );

}


function consultarClasificacionValorParametro(id){
       
    $.ajax(
    {

        url: HOST + '/api/valor_parametro/'+id,
        type: 'GET',
        success: function(result) {
           if(result.data.clasificacion_parametro.descripcion == "Real"){
                   $('#valorNumerico').show();
           }
                 
        },
        error: function(xhr) {
            console.log('Ocurrió un error: ' + xhr.status + ' ' + xhr.statusText);
        }
      }

    );

}

function consultarCliente(idCliente){
    $.ajax(
        {
            url: HOST + '/api/cliente/'+ idCliente,
            type: 'GET',
            success: function(result) {
               
                    
                        //console.log(data);
                        $('#spanNombre').text( result.data.nombres+' '+ result.data.apellidos);
                        $('#spanCedula').text( result.data.cedula);
                        $('#spanDireccion').text(result.data.direccion);
                       // $('#spanCorreo').text(result.data.correo);
                        $('#spanFechaNacimiento').text(formatFecha(result.data.fecha_nacimiento));
                        $('#spanTelefono').text(result.data.telefono);
                        $('#imgImagen').attr('src',result.data.imagen);
                        //$('#imgImagen').show();

                

               
            },
            error: function(xhr) {
                swal('Ocurrió un error',
                        xhr.status + ' ' + xhr.statusText + ': ' +
                        xhr.responseJSON.data.message,
                        'error');
            }
        }
    );
}

function formatFecha(date) {
    //Ejemplo: '2018-04-27T04:34:03.687Z'
    var fechaHoraStr = String(date).split('T'); //Separa la fecha de la hora
    var fecha = fechaHoraStr[0].split('-'); //Separa año, mes y día

    //Concatena cada valor de fecha en formato dd/mm/yyyy
    return fecha[2] + '/' + fecha[1] + '/' + fecha[0];
}
function formatFechaHora(date) {
    //Ejemplo: '2018-04-27T04:34:03.687Z'
    var fechaHoraStr = String(date).split('T'); //Separa la fecha de la hora
    var fecha = fechaHoraStr[0].split('-'); //Separa año, mes y día

    //Concatena cada valor de fecha en formato dd/mm/yyyy
    var f = fecha[2] + '/' + fecha[1] + '/' + fecha[0];

    var hora = fechaHoraStr[1].split(':'); //Separa hora, minuto y segundo
    var h = parseInt(hora[0]); //Convierte hora a entero en formato 24h
    var min = parseInt(hora[1]);
    var a = (h < 12) ? 'AM' : 'PM'; //Si h es menor a 12 es AM, si no, PM
    if(h == 0) {
        h = 12;
    } else if(h > 12) {
        h = (h % 12 < 10) ? '0' + (h % 12) : (h % 12);
    }
    min = (min < 10) ? '0' + min : min;
    return f + ' ' + h + ':' + min + ' ' + a;
}

function limpiarValorParametro() {
    $('#txtValor').val("");
  /*  $('#cmbTipoParametro').selectpicker('val', 0);
    $('#cmbParametro').selectpicker('val', 0);
    $('#cmbValorParametro').selectpicker('val', 0);*/
    
}
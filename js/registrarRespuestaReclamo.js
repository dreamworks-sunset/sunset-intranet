

$(document).ready(function() {

	$("#tablaprobando").hide();

	consultarReclamos();
	consultarTiposRespuestaReclamo();

	$('#btnEnviarRespuesta').click(function() {
		enviarRespuesta();
	});

	$('#btnVerCondiciones').click(function() {
		$("#tablaprobando").show();
	});

});

function consultarReclamos() {
	$.ajax(
		{
			url: HOST + '/api/reclamo',
			type: 'GET',
			success: function(result) {
				//Destruir la tabla para luego reinicializarla más adelante
                if($.fn.DataTable.isDataTable("#tablaReclamos")) {
                    $('#tablaReclamos').DataTable().clear().destroy();
                }
				//$('#tablaReclamos tbody').empty();
				$.each(result.data, function(index, data) {
					if(data.estatus == 1) {
						$('#tablaReclamos tbody').append(
							'<tr id="' + data.id + '" data-id-servicio="' + data.orden_servicio.solicitud.id_servicio + '" nombre-servicio="' + data.orden_servicio.solicitud.servicio.nombre + '">' +
								'<td>' + formatFechaHora(data.fecha_creacion) + '</td>' +
								'<td>' + data.id_orden_servicio + '</td>' +
								'<td>' + data.tipo_reclamo.descripcion + '</td>' +
								'<td>' + data.cliente.nombres + ' ' + data.cliente.apellidos + '</td>' +
								'<td class="acciones">' +
									'<button type="button" rel="tooltip" data-toggle="modal" ' +
											'data-target="#modalReclamo" data-placement="top" data-container="body" ' +
	                                        'title="Responder" class="btn btn-success btn-circle waves-effect waves-circle waves-float" ' +
                                        	'onclick="cargarReclamo(this,\'' + data.descripcion + '\') ; consultarCondicionesGarantia(this)">' +
                                    	'<i class="material-icons">reply </i>' +
                                	'</button>' +
								'</td>' +
							'</tr>'
						);
					}
				});
				//Aquí se reinicializa la tabla
                $("#tablaReclamos").dataTable({
                    responsive: true
                });
			},
			error: function(xhr) {
				swal('Ocurrió un error',
						xhr.status + ' ' + xhr.statusText + ': ' +
						xhr.responseJSON.data.message,
						'error');
			}
		}
	);
}

function consultarCondicionesGarantia(el) {
	var idServicio = $(el).parent().parent().data("id-servicio");
	console.log(idServicio);

	$.ajax(
		{

			url: HOST + '/api/servicio/'+idServicio,
			type: 'GET',
			success: function(result) {
				//Destruir la tabla para luego reinicializarla más adelante
                if($.fn.DataTable.isDataTable("#tablaCondicionesGarantia")) {
                    $('#tablaCondicionesGarantia').DataTable().clear().destroy();
                }
				//$('#tablaCondicionesGarantia tbody').empty();
				$.each(result.data.condicion_garantia, function(index, data) {
					if(data.estatus == 1) {
						$('#tablaCondicionesGarantia tbody').append(
							'<tr id="' + data.id + '">' +
								'<td>' + data.clausula + '</td>' +
							'</tr>'
						);
					}
				});
				//Aquí se reinicializa la tabla
                $("#tablaCondicionesGarantia").dataTable({
                    responsive: true
                });
			},
			error: function(xhr) {
				swal('Ocurrió un error',
						xhr.status + ' ' + xhr.statusText + ': ' +
						xhr.responseJSON.data.message,
						'error');
			}
		}
	);
}
function consultarTiposRespuestaReclamo() {
	$.ajax(
		{
			url: HOST + '/api/tipo_respuesta_reclamo',
			type: 'GET',
			success: function(result) {
				$('#cmbTipoRespuestaReclamo').empty();
				$.each(result.data, function(index, data) {
					$('#cmbTipoRespuestaReclamo').append(
						'<option value="' + data.id + '">' + data.descripcion + '</option>'
					);
				});
				$('#cmbTipoRespuestaReclamo').selectpicker('refresh');
			},
			error: function(xhr) {
				swal('Ocurrió un error',
						xhr.status + ' ' + xhr.statusText + ': ' +
						xhr.responseJSON.data.message,
						'error');
			}
		}
	);
}

function cargarReclamo(el, contenido) {
	$("#tablaprobando").hide();
	var nombreServicio = $(el).parent().parent().attr('nombre-servicio');
	var id = $(el).parent().parent().attr('id');
	var fila = $(el).parent().parent().children();
	var cliente = $(fila).eq(3).text();
	var ordenServicio = $(fila).eq(1).text();
	var tipoReclamo = $(fila).eq(2).text();
	$('#spanCliente').text(cliente);
	$('#spanTipoReclamo').text(tipoReclamo);
	$('#spanOrdenServicio').text(ordenServicio);
	$('#spanServicio').text(nombreServicio);
	$('#txtaContenidoReclamo').text(contenido);
	$('#txtIdReclamo').val(id);
}

function enviarRespuesta() {
	var form = document.getElementById('formRespuestaReclamo');
	var postData = new FormData(form);
	$.ajax(
		{
			url: HOST + '/api/respuesta_reclamo',
			type: 'POST',
			dataType: 'json',
			data: postData,
			processData: false,
			contentType: false,
			success: function(data) {
				consultarReclamos();
				swal('Respuesta enviada exitosamente', '', 'success');
				$('#modalReclamo').modal('toggle');
			},
			error: function(xhr) {
				swal('Ocurrió un error',
						xhr.status + ' ' + xhr.statusText + ': ' +
						xhr.responseJSON.data.message,
						'error');
			}
		}
	);
}

function formatFechaHora(date) {
    //Ejemplo: '2018-04-27T04:34:03.687Z'
	var date = new Date(date).toLocaleDateString('es-ve', {
		year: 'numeric',
		month: '2-digit',
		day: '2-digit',
		hour: '2-digit',
		minute: '2-digit',
		hour12: true

	});

	return date;
}

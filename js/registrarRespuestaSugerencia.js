

$(document).ready(function() {

	consultarComentarios();
	consultarTiposRespuestaComentario();

	$('#btnEnviarRespuesta').click(function() {
		enviarRespuesta();
	});

});

function consultarComentarios() {
	$.ajax(
		{
			url: HOST + '/api/comentario',
			type: 'GET',
			success: function(result) {
				//Destruir la tabla para luego reinicializarla más adelante
                if($.fn.DataTable.isDataTable("#tablaComentarios")) {
                    $('#tablaComentarios').DataTable().clear().destroy();
                }
				//$('#tablaComentarios tbody').empty();
				$.each(result.data, function(index, data) {
					
					if(data.estatus == 1) {
						$('#tablaComentarios tbody').append(
							'<tr id="' + data.id + '">' +
								'<td>' + formatFechaHora(data.fecha_creacion) + '</td>' +
								'<td>' + data.tipo_comentario.descripcion + '</td>' +
								'<td>' + data.categoria_comentario.descripcion + '</td>' +
								'<td>' + data.cliente.nombres + ' ' + data.cliente.apellidos + '</td>' +
								'<td class="acciones">' +
									'<button type="button" rel="tooltip" data-toggle="modal" ' +
											'data-target="#modalSugerencia" data-placement="top" data-container="body" ' +
	                                        'title="Responder" class="btn btn-success btn-circle waves-effect waves-circle waves-float" ' +
                                        	'onclick="cargarComentario(this,\'' + data.contenido + '\')">' +
                                    	'<i class="material-icons">reply </i>' +
                                	'</button>' +
								'</td>' +
							'</tr>'
						);
					}
				});
				//Aquí se reinicializa la tabla
                $(".js-basic-example").dataTable({
                    responsive: true
                });
			},
			error: function(xhr) {
				swal('Ocurrió un error',
						xhr.status + ' ' + xhr.statusText + ': ' +
						xhr.responseJSON.data.message,
						'error');
			}
		}
	);
}

function consultarTiposRespuestaComentario() {
	$.ajax(
		{
			url: HOST + '/api/tipo_respuesta_comentario',
			type: 'GET',
			success: function(result) {
				$('#cmbTipoRespuestaComentario').empty();
				$.each(result.data, function(index, data) {
					$('#cmbTipoRespuestaComentario').append(
						'<option value="' + data.id + '">' + data.descripcion + '</option>'
					);
				});
				$('#cmbTipoRespuestaComentario').selectpicker('refresh');
			},
			error: function(xhr) {
				swal('Ocurrió un error',
						xhr.status + ' ' + xhr.statusText + ': ' +
						xhr.responseJSON.data.message,
						'error');
			}
		}
	);
}

function cargarComentario(el, contenido) {
	var id = $(el).parent().parent().attr('id');
	var fila = $(el).parent().parent().children();
	var cliente = $(fila).eq(3).text();
	var tipoComentario = $(fila).eq(1).text();
	var categoriaComentario = $(fila).eq(2).text();
	$('#spanCliente').text(cliente);
	$('#spanTipoComentario').text(tipoComentario);
	$('#spanCategoriaComentario').text(categoriaComentario);
	$('#txtaContenidoComentario').text(contenido);
	$('#txtaContenidoComentario').data('id_comentario', id);
}

function enviarRespuesta() {
	var form = document.getElementById('formRespuestaComentario');
	var postData = new FormData(form);
	postData.append('id_comentario', $('#txtaContenidoComentario').data('id_comentario'));
	$.ajax(
		{
			url: HOST + '/api/respuesta_comentario',
			type: 'POST',
			dataType: 'json',
			data: postData,
			processData: false,
			contentType: false,
			success: function(data) {
				consultarComentarios();
				swal('Respuesta enviada exitosamente', '', 'success');
				$('#modalSugerencia').modal('toggle');
			},
			error: function(xhr) {
				swal('Ocurrió un error',
						xhr.status + ' ' + xhr.statusText + ': ' +
						xhr.responseJSON.data.message,
						'error');
			}
		}
	);
}

function formatFechaHora(date) {
    //Ejemplo: '2018-04-27T04:34:03.687Z'
    var fechaHoraStr = String(date).split('T'); //Separa la fecha de la hora
    var fecha = fechaHoraStr[0].split('-'); //Separa año, mes y día

    //Concatena cada valor de fecha en formato dd/mm/yyyy
    var f = fecha[2] + '/' + fecha[1] + '/' + fecha[0];

    var hora = fechaHoraStr[1].split(':'); //Separa hora, minuto y segundo
    var h = parseInt(hora[0]); //Convierte hora a entero en formato 24h
    var min = parseInt(hora[1]);
    var a = (h < 12) ? 'AM' : 'PM'; //Si h es menor a 12 es AM, si no, PM
    if(h == 0) {
        h = 12;
    } else if(h > 12) {
        h = (h % 12 < 10) ? '0' + (h % 12) : (h % 12);
    }
    min = (min < 10) ? '0' + min : min;
    return f + ' ' + h + ':' + min + ' ' + a;
}

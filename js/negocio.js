

$(document).ready(function() {
	consultarNegocio();
	consultarFilosofia();
	consultarRedesSociales();
	consultarRedesSocialesEditar();
	consultarRedesSocialesNegocio();
	consultarTablaRedesSocialesNegocio();
	consultarPais();

	$('#cmbPais').change(function() {

        var idPais = $(this).val();
        consultarEstadoPorPais(idPais);
        $('#cmbCiudad').html('<option value="" selected disabled hidden>Ciudad</option>');
        $('#cmbCiudad').selectpicker('refresh');
        

	});
	
	$('#cmbEstado').change(function() {

        var idCiudad = $(this).val();
        consultarCiudadPorEstado(idCiudad);

	});

	
	$('#editarFilosofia').click(function() {
		
		$.ajax(
			{
				url: HOST + '/api/filosofia/1' ,
				type: 'GET',
				success: function(result) {
					$('#txtMision').val(result.data.mision);
					$('#txtVision').val(result.data.vision);
					$('#txtObjG').val(result.data.objetivo_general);
					$('#txtObjE1').val(result.data.objetivo_especifico_1);
					$('#txtObjE2').val(result.data.objetivo_especifico_2);
					$('#txtObjE3').val(result.data.objetivo_especifico_3);
					$('#txtObjE4').val(result.data.objetivo_especifico_4);
					
			},
				error: function(xhr) {
					swal('Ocurrió un error',
							xhr.status + ' ' + xhr.statusText + ': ' +
							xhr.responseJSON.data.message,
							'error');
				}
			}
		);
	});
	//NEGOCIO
	$('#editarNegocio').click(function() {
		consultarPais();

		$('#cmbPais').change(function() {
	
			var idPais = $(this).val();
			consultarEstadoPorPais(idPais);
			$('#cmbCiudad').html('<option value="" selected disabled hidden>Ciudad</option>');
			$('#cmbCiudad').selectpicker('refresh');
			
	
		});
		$('#cmbEstado').change(function() {

			var idCiudad = $(this).val();
			consultarCiudadPorEstado(idCiudad);
	
		});
		$.ajax(
			{
				url: HOST + '/api/negocio/1' ,
				type: 'GET',
				success: function(result) {
					$('#txtRif').val(result.data.rif);
					$('#txtRazon').val(result.data.nombre);
					$('#txtCorreo').val(result.data.correo);
					$('#txtTlf1').val(result.data.telefono_1);
					$('#txtTlf2').val(result.data.telefono_2);
					$('#txtTlf3').val(result.data.telefono_3);
					$('#imgImagen2').attr('src',result.data.logo);
					$('#imgImagen2').show();
					$('#cmbCiudad').selectpicker('val',result.data.id_ciudad);
					$('#txtDireccion').val(result.data.direccion);
					$('#txtUrlUbicacion').val(result.data.url_direccion);
					
					

			},
				error: function(xhr) {
					swal('Ocurrió un error',
							xhr.status + ' ' + xhr.statusText + ': ' +
							xhr.responseJSON.data.message,
							'error');
				}
			}
		);
		
	});

    $('#btnGuardarNegocio').click(function() {
		var form = document.getElementById("formDatosNegocio");
		var putData = new FormData(form);
		$.ajax(
			{
				url: HOST + '/api/negocio/1',
				type: 'PUT',
				dataType: 'json',
				data: putData,
				processData: false,
				contentType: false,
				success: function(data) {
					swal('Actualización exitosa', '', 'success');
					consultarNegocio();
					$('#ModalDatosNegocio').modal('toggle');

				},
				error: function(xhr) {
					if(xhr.responseJSON == undefined) {
						swal('Ocurrió un error', 'No se pudo conectar con el servidor', 'error');
						return;
					}
					swal('Ocurrió un error',
							xhr.status + ' ' + xhr.statusText + ': ' +
							xhr.responseJSON.data.message,
							'error');
				}
			}
		);
	});

    $('#btnGuardarFilosofia').click(function() {
		var form = document.getElementById("formFilosofia");
		var putData = new FormData(form);
		
	
		$.ajax(
			{
				url: HOST + '/api/filosofia/1',
				type: 'PUT',
				dataType: 'json',
				data: putData,
				processData: false,
				contentType: false,
				success: function(data) {
					swal('Actualización exitosa', '', 'success');
					$('#ModalFilosofia').modal('toggle');
					consultarFilosofia();

					
				},
				error: function(xhr) {
					if(xhr.responseJSON == undefined) {
						swal('Ocurrió un error', 'No se pudo conectar con el servidor', 'error');
						return;
					}
					swal('Ocurrió un error',
							xhr.status + ' ' + xhr.statusText + ': ' +
							xhr.responseJSON.data.message,
							'error');
				}
			}
		);
	});

	$('#btnGuardarRedes').click(function() {
		var form = document.getElementById("formRedesSociales");
		var postData = new FormData(form);
		
		$.ajax(
			{
				url: HOST + '/api/red_social_negocio',
				type: 'POST',
				dataType: 'json',
				data: postData,
				processData: false,
				contentType: false,
				success: function(data) {
					swal('Registro exitoso', '', 'success');
					limpiarRedSocial();
					consultarRedesSocialesNegocio();

					
				},
				error: function(xhr) {
					if(xhr.responseJSON == undefined) {
						swal('Ocurrió un error', 'No se pudo conectar con el servidor', 'error');
						return;
					}
					swal('Ocurrió un error',
							xhr.status + ' ' + xhr.statusText + ': ' +
							xhr.responseJSON.data.message,
							'error');
				}
			}
		);
	});
	$('#btnActualizarRedes').click(function(){
		var form = document.getElementById("formEditarRedesSociales");
		var putData = new FormData(form);
		var id = $('#txtIdEditarRedSocial').val();
		$.ajax(
			{
				url: HOST + '/api/red_social_negocio/'+id,
				type: 'PUT',
				dataType: 'json',
				data: putData,
				processData: false,
				contentType: false,
				success: function(data) {
					swal('Actualización exitosa', '', 'success');

					limpiarRedSocialNegocio();
					consultarTablaRedesSocialesNegocio();
					
				},
				error: function(xhr) {
					if(xhr.responseJSON == undefined) {
						swal('Ocurrió un error', 'No se pudo conectar con el servidor', 'error');
						return;
					}
					swal('Ocurrió un error',
							xhr.status + ' ' + xhr.statusText + ': ' +
							xhr.responseJSON.data.message,
							'error');
				}
			}
		);
		
	});

});
//Funciones
function seleccionarFila(el) {
	$('#txtIdEditarRedSocial').val($(el).attr('data-id-red-social-negocio'));
	$('#cmbEditarRedes').selectpicker('val', $(el).children('td').eq(0).data('id-red-social'));
	$('#txtEditarUrl').val($(el).children('td').eq(1).text());
	
	$('#btnActualizarRedes').show();
	//$('#btnModificar').show();
}

function consultarRedesSocialesNegocio() {
	$.ajax(
		{
			url: HOST + '/api/red_social_negocio',
			type: 'GET',
			success: function(result) {
				
				$.each(result.data, function(index, data) {
					$('#listaRedesSociales').append(

						'<div>'+'<i class="material-icons">'+'star'+'</i>'+
						'<span data-id-red-social="'+ data.id_red_social +'">'+ data.red_social.descripcion +'</span>'+
						":   "+
						'<span data-id-red-social-negocio="' + data.id +'">' + data.url + '</span>'+
						'</div>'+'</br>'
						
					);
				});
				},
			error: function(xhr) {
				swal('Ocurrió un error',
						xhr.status + ' ' + xhr.statusText + ': ' +
						xhr.responseJSON.data.message,
						'error');
			}
		}
	);
}
function consultarTablaRedesSocialesNegocio() {
	$('#btnActualizarRedes').hide();
	$.ajax(
		{
			url: HOST + '/api/red_social_negocio',
			type: 'GET',
			success: function(result) {
				$('#tablaRedesSociales tbody').empty();
				$.each(result.data, function(index, data) {
					$('#tablaRedesSociales tbody').append(
						'<tr data-id-red-social-negocio="' + data.id + '" data-id-red-social="' + data.id_red_social + '"onclick="seleccionarFila(this)">' +						
						'<td data-id-red-social="'+ data.id_red_social +'">'+ data.red_social.descripcion +'</td>'+
						'<td>'+ data.url + '</td>'+
						'</tr>'	
					);
				});
				$('#tablaRedesSociales tbody tr').css('cursor', 'pointer');
				},
			error: function(xhr) {
				swal('Ocurrió un error',
						xhr.status + ' ' + xhr.statusText + ': ' +
						xhr.responseJSON.data.message,
						'error');
			}
		}
	);
}
function consultarNegocio(){
    $.ajax(
        {
            url: HOST + '/api/negocio/1',
            type: 'GET',
            success: function(result) {
               

                        $('#bRazonSocial').text( result.data.nombre);
                        $('#sRif').text( result.data.rif);
                        $('#sDireccion').text(result.data.direccion);
                        $('#sCorreo').text(result.data.correo);
                        $('#sTelefono1').text(result.data.telefono_1);
						$('#sTelefono2').text(result.data.telefono_2);
						$('#imgImagen').attr('src',result.data.logo);
                  

                

               
            },
            error: function(xhr) {
                swal('Ocurrió un error',
                        xhr.status + ' ' + xhr.statusText + ': ' +
                        xhr.responseJSON.data.message,
                        'error');
            }
        }
    );
}

function consultarFilosofia(){
    $.ajax(
        {
            url: HOST + '/api/filosofia/1',
            type: 'GET',
            success: function(result) {
               

                        $('#pMision').text( result.data.mision);
                        $('#pVision').text( result.data.vision);
                        $('#pObjetivo').text(result.data.objetivo_general);
                        $('#liObjEspecifico1').text(result.data.objetivo_especifico_1);
						$('#liObjEspecifico2').text(result.data.objetivo_especifico_2);
						$('#liObjEspecifico3').text(result.data.objetivo_especifico_3);
						$('#liObjEspecifico4').text(result.data.objetivo_especifico_4); 
            },
            error: function(xhr) {
                swal('Ocurrió un error',
                        xhr.status + ' ' + xhr.statusText + ': ' +
                        xhr.responseJSON.data.message,
                        'error');
            }
        }
    );
}

function consultarRedesSocialesEditar() {
	$.ajax(
		{
			url: HOST + '/api/red_social',
			type: 'GET',
			success: function(result) {
				$('#cmbEditarRedes').html('<option value="0" selected disabled hidden>Red Social</option>');
				$.each(result.data, function(index, data) {
					$('#cmbEditarRedes').append('<option value="' + data.id + '">' + data.descripcion  + '</option>');
				});
				$('#cmbEditarRedes').selectpicker('refresh');
			},
			error: function(xhr) {
				swal('Ocurrió un error',
						xhr.status + ' ' + xhr.statusText + ': ' +
						xhr.responseJSON.data.message,
						'error');
			}
		}
	);

}
function consultarRedesSociales() {
	$.ajax(
		{
			url: HOST + '/api/red_social',
			type: 'GET',
			success: function(result) {
				$('#cmbRedes').html('<option value="0" selected disabled hidden>Red Social</option>');
				$.each(result.data, function(index, data) {
					$('#cmbRedes').append('<option value="' + data.id + '">' + data.descripcion  + '</option>');
				});
				$('#cmbRedes').selectpicker('refresh');
			},
			error: function(xhr) {
				swal('Ocurrió un error',
						xhr.status + ' ' + xhr.statusText + ': ' +
						xhr.responseJSON.data.message,
						'error');
			}
		}
	);

}
function consultarPais() {
	$.ajax(
		{
			url: HOST + '/api/pais',
			type: 'GET',
			success: function(result) {
				$('#cmbPais').html('<option value="0" selected disabled hidden>País</option>');
				$.each(result.data, function(index, data) {
					$('#cmbPais').append('<option value="' + data.id + '">' + data.nombre  + '</option>');
				});
				$('#cmbPais').selectpicker('refresh');
			},
			error: function(xhr) {
				swal('Ocurrió un error',
						xhr.status + ' ' + xhr.statusText + ': ' +
						xhr.responseJSON.data.message,
						'error');
			}
		}
	);

}


function consultarEstadoPorPais(id) {
       
    $.ajax(
    {

        url: HOST + '/api/pais/'+id,
        type: 'GET',
        success: function(result) {
            $('#cmbEstado').html('<option value="" selected disabled hidden>Estado</option>');
            $.each(result.data.estado, function(index, data) {
                $('#cmbEstado').append('<option value="' + data.id + '">' + data.nombre + '</option>')
            });
            $('#cmbEstado').selectpicker('refresh');
        },
        error: function(xhr) {
            console.log('Ocurrió un error: ' + xhr.status + ' ' + xhr.statusText);
        }
      }
    );

}


function consultarCiudadPorEstado(id) {
       
    $.ajax(
    {

        url: HOST + '/api/estado/'+id,
        type: 'GET',
        success: function(result) {
            $('#cmbCiudad').html('<option value="" selected disabled hidden>Ciudad</option>');
            $.each(result.data.ciudad, function(index, data) {
                $('#cmbCiudad').append('<option value="' + data.id + '">' + data.nombre + '</option>')
            });
            $('#cmbCiudad').selectpicker('refresh');

        },
        error: function(xhr) {
            console.log('Ocurrió un error: ' + xhr.status + ' ' + xhr.statusText);
        }
      }

    );

}


function limpiarRedSocial() {
    $('#txtRedSocial').val("");
	$('#cmbRedes').selectpicker('val', 0);
	$('#ModalRedSocial').modal('toggle');
		  
    
}

function limpiarRedSocialNegocio() {
    $('#txtIdEditarRedSocial').val("");
	$('#cmbEditarRedes').selectpicker('val', 0);
	$('#txtEditarUrl').val("");
	//$('#modalEditarRedSocial').modal('toggle');
		  
    
}


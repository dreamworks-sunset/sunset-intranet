

let valuesDirigidoA = {}; //Diccionario donde se almacenarán los valores parámetro de "Dirigido A"
let valuesDirigidoAPorEliminar = {} //Diccionario donde se almacenarán los valores parámetro de "Dirigido A" que serán removidos de la base de datos



$(document).ready(function() {
  $('#valoresParametro').multiSelect({
		selectableOptgroup: true, //Permite seleccionar un grupo de valores parámetro completo
		afterSelect: function (values) {
			//Iteramos por cada elemento seleccionado (uno o más a la vez)
			values.forEach(element => {
				valuesDirigidoA[element] = element; //Si se selecciona valor parámetro "1", se setea el campo clave "1" con el valor en cuestión
				valuesDirigidoAPorEliminar[element] = false; //En caso de que se haya removido y se selecciona de nuevo, debe setearse en false
			});
			console.log(valuesDirigidoA);
		},
		afterDeselect: function (values) {
			//Iteramos por cada elemento seleccionado (uno o más a la vez)
			values.forEach(element => {
				valuesDirigidoA[element] = false; //Si se deselecciona valor parámetro "1", se setea el campo clave "1" con el valor false
				valuesDirigidoAPorEliminar[element] = element;
			});
			console.log(valuesDirigidoA);
			console.log('Se eliminarán:', valuesDirigidoAPorEliminar);
		}
	});

    limpiarCampos();
	consultarPromociones();
	consultarServicio();
	consultarValoresParametro();


$('#btnGuardar').click(function() {
		var form = document.getElementById("formPromocion");
		var postData = new FormData(form);
		var idServicio = $('#txtIdServicio').attr('data-id-servicio');
		postData.append('id_servicio', idServicio);
		var valoresParametroDirigidoA = Object.values(valuesDirigidoA);
		var valoresDirigidoA = [];
		valoresParametroDirigidoA.forEach(valor => {
			if(valor) {
				valoresDirigidoA.push(valor);
			}
		});
		console.log('valores =', valoresDirigidoA);
		postData.append('detalle_promocion', valoresDirigidoA);
		$.ajax(
			{
				url: HOST + '/api/promocion',
				type: 'POST',
				dataType: 'json',
				data: postData,
				processData: false,
				contentType: false,
				success: function(data) {
					swal('Registro exitoso', '', 'success');
					limpiarCampos();
					consultarPromociones();
				},
				error: function(xhr) {
					if(xhr.responseJSON == undefined) {
						swal('Ocurrió un error', 'No se pudo conectar con el servidor', 'error');
						return;
					}
					swal('Ocurrió un error',
							xhr.status + ' ' + xhr.statusText + ': ' +
							xhr.responseJSON.data.message,
							'error');
				}
			}
		);
	});

$('#btnModificar').click(function() {
		var form = document.getElementById("formPromocion");
		var putData = new FormData(form);
		var idServicio = $('#txtIdServicio').attr('data-id-servicio');
		putData.append('id_servicio', idServicio);
		var id = $('#txtId').val();
		var valoresParametroDirigidoA = Object.values(valuesDirigidoA);
		var valoresDirigidoA = [];
		valoresParametroDirigidoA.forEach(valor => {
			if(valor) {
				valoresDirigidoA.push(valor);
			}
		});
		console.log('valores =', valoresDirigidoA);
		var valoresParametroDirigidoAPorEliminar = Object.values(valuesDirigidoAPorEliminar);
		var valoresDirigidoAPorEliminar = [];
		valoresParametroDirigidoAPorEliminar.forEach(valor => {
			if(valor) {
				valoresDirigidoAPorEliminar.push(valor);
			}
		});
		console.log('valores por eliminar =', valoresDirigidoAPorEliminar);

		putData.append('detalle_promocion', valoresDirigidoA);
		putData.append('detalle_promocion_eliminar', valoresDirigidoAPorEliminar);
		putData.append('cambio_estatus','false');
		$.ajax(
			{
				url: HOST + '/api/promocion/' + id,
				type: 'PUT',
				dataType: 'json',
				data: putData,
				processData: false,
				contentType: false,
				success: function(data) {
					swal('Modificación exitosa', '', 'success');
					limpiarCampos();
					consultarPromociones();
				},
				error: function(xhr) {
					if(xhr.responseJSON == undefined) {
						swal('Ocurrió un error', 'No se pudo conectar con el servidor', 'error');
						return;
					}
					swal('Ocurrió un error',
							xhr.status + ' ' + xhr.statusText + ': ' +
							xhr.responseJSON.data.message,
							'error');
				}
			}
		);
	});


$('#btnHabilitar').click(function() {
		var putData = new FormData();
		putData.append('estatus', '1');
		putData.append('cambio_estatus','true');
		var id = $('#txtId').val();
		$.ajax(
			{
				url: HOST + '/api/promocion/' + id,
				type: 'PUT',
				dataType: 'json',
				data: putData,
				processData: false,
				contentType: false,
				success: function(data) {
					swal('Habilitación exitosa', '', 'success');
					limpiarCampos();
					consultarPromociones();
				},
				error: function(xhr) {
					swal('Ocurrió un error',
							xhr.status + ' ' + xhr.statusText + ': ' +
							xhr.responseJSON.data.message,
							'error');
				}
			}
		);
	});

	$('#btnDeshabilitar').click(function() {
		var putData = new FormData();
		putData.append('estatus', '0');
		putData.append('cambio_estatus','true');
		var id = $('#txtId').val();
		$.ajax(
			{
				url: HOST + '/api/promocion/' + id,
				type: 'PUT',
				dataType: 'json',
				data: putData,
				processData: false,
				contentType: false,
				success: function(data) {
					swal('Deshabilitación exitosa', '', 'success');
					limpiarCampos();
					consultarPromociones();
				},
				error: function(xhr) {
					swal('Ocurrió un error',
							xhr.status + ' ' + xhr.statusText + ': ' +
							xhr.responseJSON.data.message,
							'error');
				}
			}
		);
	});

	$('#btnCancelar').click(function() {
		limpiarCampos();
	});

});

function seleccionarFila(el) {
	var idPromocion = $(el).attr('data-id-promocion');
	$('#txtId').val(idPromocion);
	$('#imgImagen').attr('src',$(el).data('imagen'));
	$('#imgImagen').show();
	//$('#txtId').val($(el).attr('data-id-promocion'));
	$('#txtNombre').val($(el).children('td').eq(0).text());
	$('#txtDescripcion').val($(el).children('td').eq(1).text());
	$('#txtPorcentajeDescuento').val($(el).children('td').eq(2).text());
	$('#dtpFechaInicio').val($(el).children('td').eq(3).text());
	$('#dtpFechaFin').val($(el).children('td').eq(4).text());
	//$('#elegir').val($(el).children('td').eq(5).text());
	$('#txtIdServicio').val($(el).children('td').eq(6).text());
	$('#txtIdServicio').attr('data-id-servicio', $(el).attr('data-id-servicio'));
	var habilitado = $(el).children('td').eq(7).data('estatus');
	if(habilitado == 1) {
		$('#btnDeshabilitar').show();
		$('#btnHabilitar').hide();
	} else {
		$('#btnDeshabilitar').hide();
		$('#btnHabilitar').show();
	}
	$('#btnGuardar').hide();
	$('#btnModificar').show();
	$('.ms optgroup option:selected').removeAttr('selected'); //Remueve las selecciones de todos los select multiple
	$('.ms').multiSelect('refresh'); //Actualiza los select multiple

	//Setea todos los valores actuales de los diccionarios en false
	limpiarDiccionario(valuesDirigidoA);
	limpiarDiccionario(valuesDirigidoAPorEliminar);
	//FIN limpiar diccionarios

	consultarValoresParametroPorPromocion(idPromocion);

	
}

function seleccionarServicio(el) {
	$('#txtIdServicio').attr('data-id-servicio', $(el).attr('data-id-servicio'));
	$('#txtIdServicio').val($(el).children('td').eq(0).text());
	$('#ModalAsignarServicios').modal('toggle');

}

function consultarPromociones() {
	$.ajax(
		{
			url: HOST + '/api/promocion',
			type: 'GET',
			success: function(result) {
				//Destruir la tabla para luego reinicializarla más adelante
                if($.fn.DataTable.isDataTable("#tablaPromocion")) {
                    $('#tablaPromocion').DataTable().clear().destroy();
                }
				//$('#tablaPromocion tbody').empty();
				$.each(result.data, function(index, data) {
					$('#tablaPromocion tbody').append(
						'<tr data-imagen="'+ data.imagen +'" data-id-promocion="' + data.id + '" data-id-servicio="' + data.id_servicio + '"onclick="seleccionarFila(this)">' +
							'<td>' + data.nombre + '</td>' +
							'<td>' + data.descripcion + '</td>' +
							'<td>' + data.porcentaje_descuento + '</td>' +
							'<td>' + formatFecha(data.fecha_inicio) + '</td>' +
							'<td>' + formatFecha(data.fecha_fin) + '</td>' +
							'<td>' + data.imagen+ '</td>' +
							'<td>' + data.servicio.nombre+ '</td>' +
							'<td data-estatus="' + data.estatus + '">' + ((data.estatus == 1) ? 'Sí' : 'No') + '</td>' +



						'</tr>'
					);
				});
				$('.js-exportable').DataTable({
                    dom: 'Bfrtip',
                    responsive: true,
                    buttons: [
                        'copy', 'csv', 'excel', 'pdf', 'print'
                    ]
                });
				$('#tablaPromocion tbody tr').css('cursor', 'pointer');
				$('td[data-estatus="1"]').addClass('habilitado');
				$('td[data-estatus="0"]').addClass('deshabilitado');
			},
			error: function(xhr) {
				swal('Ocurrió un error',
						xhr.status + ' ' + xhr.statusText + ': ' +
						xhr.responseJSON.data.message,
						'error');
			}
		}
	);
}

function consultarServicio(){
	$.ajax(
		{
			url: HOST + '/api/servicio',
			type: 'GET',
			success: function(result) {
				$('#tablaServicio tbody').empty();
				$.each(result.data, function(index, data) {
					$('#tablaServicio tbody').append(
						'<tr data-id-servicio="' + data.id + '" onclick="seleccionarServicio(this)">' +
							'<td>' + data.nombre + '</td>' +
							'<td>' + data.cantidad_sesiones + '</td>' +
							'<td>' + data.precio_por_sesion + '</td>' +
			

						'</tr>'
					);
				});
				$('#tablaServicio tbody tr').css('cursor', 'pointer');
			},
			error: function(xhr) {
				swal('Ocurrió un error',
						xhr.status + ' ' + xhr.statusText + ': ' +
						xhr.responseJSON.data.message,
						'error');
			}
		}
	);
}

function limpiarCampos() {
	$('#txtId').val("");
	$('#imgImagen').attr('src', '');
	$('#imgImagen').hide();
	$('#txtNombre').val("");
	$('#txtDescripcion').val("");
	$('#txtPorcentajeDescuento').val("");
	$('#txtIdServicio').val("");
	$('#dtpFechaInicio').val("");
	$('#dtpFechaFin').val("");
	$('#elegir').val("");
	$('#btnGuardar').show();
	$('#btnModificar, #btnDeshabilitar, #btnHabilitar').hide();
	//Setea todos los valores actuales de los diccionarios en false
	limpiarDiccionario(valuesDirigidoA);
	limpiarDiccionario(valuesDirigidoAPorEliminar);
	resetFile();
}

function resetFile() {
	$('#elegir').wrap('<form></form>').closest('form').get(0).reset();
	$('#elegir').unwrap();
}

function formatFecha(date) {
    //Ejemplo: '2018-04-27T04:34:03.687Z'
    var fechaHoraStr = String(date).split('T'); //Separa la fecha de la hora
    var fecha = fechaHoraStr[0].split('-'); //Separa año, mes y día

    //Concatena cada valor de fecha en formato dd/mm/yyyy
    return fecha[2] + '/' + fecha[1] + '/' + fecha[0];
}

function consultarValoresParametro() {
	$.ajax(
	{
		url: HOST + '/api/parametro',
		type: 'GET',
		success: function(result) {
			$('#valoresParametro').empty();
			$.each(result.data, function(index, data) {
				$('#valoresParametro').append('<optgroup label="' + data.descripcion + '"></optgroup>');
				$.each(data.valor_parametro, function(index, valor) {
					$('#valoresParametro optgroup:last-child').append(
						((valor.estatus == 1) ? '<option value="' + valor.id + '">' + valor.descripcion + '</option>' : 'NO')
						
					);
				});
			});
			$('#valoresParametro').multiSelect('refresh');
		},
		error: function(xhr) {
			if(xhr.responseJSON == undefined) {
				swal('Ocurrió un error', 'No se pudo conectar con el servidor', 'error');
				return;
			}
			swal('Ocurrió un error',
					xhr.status + ' ' + xhr.statusText + ': ' +
					xhr.responseJSON.data.message,
					'error');
		}
	});
}

function consultarValoresParametroPorPromocion(idPromocion) {
	$.ajax(
	{
		url: HOST + '/api/promocion/' + idPromocion,
		type: 'GET',
		success: function(result) {
			$.each(result.data.detalle_promocion, function(index, data) {
				//Por cada valor parámetro asociado al servicio, toma el id y se selecciona
				//el correspondiente valor en el select multiple
				$('#valoresParametro optgroup option[value="' + data.id + '"]').prop('selected', true);
			});
			$('#valoresParametro').multiSelect('refresh');
		},
		error: function(xhr) {
			if(xhr.responseJSON == undefined) {
				swal('Ocurrió un error', 'No se pudo conectar con el servidor', 'error');
				return;
			}
			swal('Ocurrió un error',
					xhr.status + ' ' + xhr.statusText + ': ' +
					xhr.responseJSON.data.message,
					'error');
		}
	});
}


function limpiarDiccionario(objeto) {
	var keys = Object.keys(objeto);
	keys.forEach(element => {
		objeto[element] = false;
	});
}

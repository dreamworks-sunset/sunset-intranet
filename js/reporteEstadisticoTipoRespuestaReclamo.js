$(function () {
    consultarServiciosReclamadosAlgunaVez();

    $('#btnGenerarReporte').click(function() {
        $('#donut_chart').empty();
        consultarTiposRespuestaReclamos();
    });

});


function getMorris(type, element, datos, colores) {
    if (type === 'donut') {
        Morris.Donut({
            element: element,
            /*data: [
                {
                    label: 'Procesados',
                    value: 35
                }, {
                    label: 'No procesados por servicio sin garantía',
                    value: 12
                }, {
                    label: 'No procesados por no cumplir con las condicones de garantia',
                    value: 7
                }
            ],*/
            data: datos,
            //colors: ['rgb(233, 30, 99)', 'rgb(0, 188, 212)','rgb(0, 18, 12)'],
            colors: colores,
            formatter: function (y) {
                return y + '%'
            }
        });
    }
}

function consultarTiposRespuestaReclamos() {
    $.ajax(
    {
        url: HOST + '/api/vista_reporte_tipo_respuesta_reclamo_por_servicio',
        type: 'GET',
        success: function(result) {
            //Vaciamos la tabla
            //Destruir la tabla para luego reinicializarla más adelante
            if($.fn.DataTable.isDataTable("#tablaReporteTipoRespuestaReclamo")) {
                $('#tablaReporteTipoRespuestaReclamo').DataTable().clear().destroy();
            }
            //$('#tablaReporteTipoRespuestaReclamo tbody').empty();
            
            var tiposRespuesta = []; //arreglo que almacena la cantidad de respuestas por tipo
            var idsTiposRespuesta = {}; //diccionario para almacenar los id de tipo de respuesta con sus valores
            var colors = {}; //dicionario para almacenar los colores de cada tipo de respuesta
            var total = 0.0; //Necesario para sacar el porcentaje

            $.each(result.data, function(index, data) {
                var idServicio = $('#cmbServicio').val();
                var fecha_ini = formatFechaDMYToYMD($('#dtpFechaInicio').val());
                var fecha_fin = formatFechaDMYToYMD($('#dtpFechaFin').val());
                var fecha_creacion = new Date(data.fecha_creacion);

                //Filtrar por fecha
                if(fecha_creacion.getTime() < fecha_ini.getTime() || fecha_creacion.getTime() > fecha_fin.getTime()) {
                    return true; //continúa en la siguiente iteración
                }

                //Filtrar por servicio
                if(idServicio != 0 && idServicio != data.id_servicio) {
                    return true; //continúa en la siguiente iteración
                }

                $('#tablaReporteTipoRespuestaReclamo tbody').append(
                    '<tr>' +
                        '<td>' +
                            data.id_orden +
                        '</td>' +
                        '<td>' +
                            data.cliente +
                        '</td>' +
                        '<td>' +
                            data.servicio +
                        '</td>' +
                        '<td>' +
                            data.tipo_respuesta_reclamo +
                        '</td>' +
                    '</tr>'
                );

                idsTiposRespuesta[data.id_tipo_respuesta_reclamo] = data.tipo_respuesta_reclamo;
                colors[data.id_tipo_respuesta_reclamo] = getRandomColor();
                
                if(tiposRespuesta[data.id_tipo_respuesta_reclamo] == undefined) {
                    tiposRespuesta[data.id_tipo_respuesta_reclamo] = 1.0;
                } else {
                    tiposRespuesta[data.id_tipo_respuesta_reclamo]++;
                }
                total++;
            });
            //Aquí se reinicializa la tabla
            $(".js-basic-example").dataTable({
                responsive: true
            });

            var datos = []; //arreglo con los datos que se le pasarán al gráfico
            $.each(idsTiposRespuesta, function(key, value) {
                datos.push({
                    label: value,
                    value: parseFloat(tiposRespuesta[key] / total * 100).toFixed(2)
                });
            });
            getMorris('donut', 'donut_chart', datos, Object.values(colors));
        },
        error: function(xhr) {
            if(xhr.responseJSON == undefined) {
                swal('Ocurrió un error', 'No se pudo conectar con el servidor', 'error');
                return;
            }
            swal('Ocurrió un error',
                    xhr.status + ' ' + xhr.statusText + ': ' +
                    xhr.responseJSON.data.message,
                    'error');
        }
    });
}

function consultarServiciosReclamadosAlgunaVez() {
    $('#cmbServicio').html('<option value="0">Todos</option>');
    $.ajax(
        {
            url: HOST + '/api/respuesta_reclamo',
            type: 'GET',
            success: function(result) {
                $.each(result.data, function(index, data) {
                    var servicio = data.reclamo.orden_servicio.solicitud.servicio;
                    if($('#cmbServicio option[value="' + servicio.id + '"]').length == 0) {
                        $('#cmbServicio').append('<option value="' + servicio.id + '">' + servicio.nombre + '</option>');
                    }
                });
                $('#cmbServicio').selectpicker('refresh');
            },
            error: function(xhr) {
                if(xhr.responseJSON == undefined) {
                    swal('Ocurrió un error', 'No se pudo conectar con el servidor', 'error');
                    return;
                }
                swal('Ocurrió un error',
                        xhr.status + ' ' + xhr.statusText + ': ' +
                        xhr.responseJSON.data.message,
                        'error');
            }
        });
}

function formatFechaDMYToYMD(date) {
    var fecha = date.split('/');
    var fec = new Date(fecha[2], parseInt(fecha[1]) - 1, fecha[0]);
    return fec;
}

function getRandomColor() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 3; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}
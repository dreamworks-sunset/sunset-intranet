

$(document).ready(function() {

	limpiarCampos();
	consultarValorParametro();
	consultarParametro();
	consultarUnidad();
	consultarClasificacion();
	

	$('#btnGuardar').click(function() {
		var form = document.getElementById("formValorParametro");
		var postData = new FormData(form);
		var IdParametro = $('#txtId_Parametro').attr('data-id-parametro');
		postData.append('id_parametro', IdParametro);
		console.log(IdParametro);
		$.ajax(
			{
				url: HOST + '/api/valor_parametro',
				type: 'POST',
				dataType: 'json',
				data: postData,
				processData: false,
				contentType: false,
				success: function(data) {
					swal('Registro exitoso', '', 'success');
					limpiarCampos();
					consultarValorParametro();
				},
				error: function(xhr) {
					if(xhr.responseJSON == undefined) {
						swal('Ocurrió un error', 'No se pudo conectar con el servidor', 'error');
						return;
					}
					swal('Ocurrió un error',
							xhr.status + ' ' + xhr.statusText + ': ' +
							xhr.responseJSON.data.message,
							'error');
				}
			}
		);
	});

	$('#btnModificar').click(function() {
		var form = document.getElementById("formValorParametro");
		var putData = new FormData(form);
		var IdParametro = $('#txtId_Parametro').attr('data-id-parametro');
		putData.append('id_parametro', IdParametro);
		var id = $('#txtId').val();
		$.ajax(
			{
				url: HOST + '/api/valor_parametro/' + id,
				type: 'PUT',
				dataType: 'json',
				data: putData,
				processData: false,
				contentType: false,
				success: function(data) {
					swal('Modificación exitosa', '', 'success');
					limpiarCampos();
					consultarValorParametro();
				},
				error: function(xhr) {
					swal('Ocurrió un error',
							xhr.status + ' ' + xhr.statusText + ': ' +
							xhr.responseJSON.data.message,
							'error');
				}
			}
		);
	});

	$('#btnHabilitar').click(function() {
		var putData = new FormData();
		putData.append('estatus', '1');
		var id = $('#txtId').val();
		$.ajax(
			{
				url: HOST + '/api/valor_parametro/' + id,
				type: 'PUT',
				dataType: 'json',
				data: putData,
				processData: false,
				contentType: false,
				success: function(data) {
					swal('Habilitación exitosa', '', 'success');
					limpiarCampos();
					consultarValorParametro();
				},
				error: function(xhr) {
					swal('Ocurrió un error',
							xhr.status + ' ' + xhr.statusText + ': ' +
							xhr.responseJSON.data.message,
							'error');
				}
			}
		);
	});

	$('#btnDeshabilitar').click(function() {
		var putData = new FormData();
		putData.append('estatus', '0');
		var id = $('#txtId').val();
		$.ajax(
			{
				url: HOST + '/api/valor_parametro/' + id,
				type: 'PUT',
				dataType: 'json',
				data: putData,
				processData: false,
				contentType: false,
				success: function(data) {
					swal('Deshabilitación exitosa', '', 'success');
					limpiarCampos();
					consultarValorParametro();
				},
				error: function(xhr) {
					swal('Ocurrió un error',
							xhr.status + ' ' + xhr.statusText + ': ' +
							xhr.responseJSON.data.message,
							'error');
				}
			}
		);
	});

	$('#btnCancelar').click(function() {
		limpiarCampos();
	});

});

function seleccionarFila(el) {
	var idValorParametro = $(el).attr('data-id-valorparametro');
	$('#txtId').val(idValorParametro);
	$('#txtDescripcion').val($(el).children('td').eq(0).text());
	$('#cmbClasificación').selectpicker('val', $(el).children('td').eq(1).data('id-clasificacion-parametro'));
	$('#cmbUnidad').selectpicker('val', $(el).children('td').eq(2).data('id-unidad'));
	$('#txtId_Parametro').val($(el).children('td').eq(3).text());
	$('#txtId_Parametro').attr('data-id-parametro', $(el).attr('data-id-parametro'));
	

	var habilitado = $(el).children('td').eq(4).data('estatus');
	if(habilitado == 1) {
		$('#btnDeshabilitar').show();
		$('#btnHabilitar').hide();
	} else {
		$('#btnDeshabilitar').hide();
		$('#btnHabilitar').show();
	}
	$('#btnGuardar').hide();
	$('#btnModificar').show();
}

function seleccionarParametro(el) {
	$('#txtId_Parametro').attr('data-id-parametro', $(el).attr('data-id-parametro'));
	$('#txtId_Parametro').val($(el).children('td').eq(0).text());
	$('#ModalAsignarParametro').modal('toggle');

}

function consultarValorParametro() {
	$.ajax(
		{
			url: HOST + '/api/valor_parametro',
			type: 'GET',
			success: function(result) {
				//Destruir la tabla para luego reinicializarla más adelante
                if($.fn.DataTable.isDataTable("#tablaValorParametro")) {
                    $('#tablaValorParametro').DataTable().clear().destroy();
                }
				//$('#tablaValorParametro tbody').empty();
				$.each(result.data, function(index, data) {
					var abreviatura ="";
						if(data.unidad != undefined)
						{
							abreviatura= data.unidad.abreviatura;
						}
					$('#tablaValorParametro tbody').append(
						
						'<tr data-id-valorparametro="' + data.id + '" data-id-parametro="' + data.id_parametro + '"onclick="seleccionarFila(this)">' +
							'<td>' + data.descripcion + '</td>' +
							'<td data-id-clasificacion-parametro="' + data.id_clasificacion_parametro+ '">' + data.clasificacion_parametro.descripcion + '</td>' +
							'<td data-id-unidad="' + data.id_unidad + '">' + abreviatura + '</td>' +
							'<td>' + data.parametro.descripcion + '</td>' +
							'<td data-estatus="' + data.estatus + '">' + ((data.estatus == 1) ? 'Sí' : 'No') + '</td>' +
						'</tr>'
					);
				});
				$('.js-exportable').DataTable({
                    dom: 'Bfrtip',
                    responsive: true,
                    buttons: [
                        'copy', 'csv', 'excel', 'pdf', 'print'
                    ]
                });
				$('#tablaValorParametro tbody tr').css('cursor', 'pointer');
				$('td[data-estatus="1"]').addClass('habilitado');
				$('td[data-estatus="0"]').addClass('deshabilitado');
			},
			error: function(xhr) {
				swal('Ocurrió un error',
						xhr.status + ' ' + xhr.statusText + ': ' +
						xhr.responseJSON.data.message,
						'error');
			}
		}
	);
}

function consultarParametro(){
	$.ajax(
		{
			url: HOST + '/api/parametro',
			type: 'GET',
			success: function(result) {
				$('#tablaParametro tbody').empty();
				$.each(result.data, function(index, data) {
					$('#tablaParametro tbody').append(
						'<tr data-id-parametro ="' + data.id + '" onclick="seleccionarParametro(this)">' +
							'<td>' + data.descripcion + '</td>' +
						'</tr>'
					);
				});
				$('#tablaParametro tbody tr').css('cursor', 'pointer');
			},
			error: function(xhr) {
				swal('Ocurrió un error',
						xhr.status + ' ' + xhr.statusText + ': ' +
						xhr.responseJSON.data.message,
						'error');
			}
		}
	);
}

function consultarUnidad() {
	$.ajax(
	{
		url: HOST + '/api/unidad',
		type: 'GET',
		success: function(result) {
			$('#cmbUnidad').html('<option value="0" selected disabled hidden>Unidad</option>');

			$.each(result.data, function(index, data) {
				$('#cmbUnidad').append( ((data.estatus == 1) ? '<option value="' + data.id + '">' + data.abreviatura  + '</option>': 'NO TIENE'));
			});
			$('#cmbUnidad').selectpicker('refresh');
		},
		error: function(xhr) {
			if(xhr.responseJSON == undefined) {
				swal('Ocurrió un error', 'No se pudo conectar con el servidor', 'error');
				return;
			}
			swal('Ocurrió un error',
					xhr.status + ' ' + xhr.statusText + ': ' +
					xhr.responseJSON.data.message,
					'error');
		}

	});
}

function consultarClasificacion() {
	$.ajax(
	{
		url: HOST + '/api/clasificacion_parametro',
		type: 'GET',
		success: function(result) {
			$('#cmbClasificación').html('<option value="0" selected disabled hidden>Clasificacion</option>');

			$.each(result.data, function(index, data) {
				$('#cmbClasificación').append('<option data-id-clasificacion-parametro="'+ data.id_clasificacion_parametro +'" value="' + data.id + '">' + data.descripcion + '</option>');
				
			});
			$('#cmbClasificación').selectpicker('refresh');
		},
		error: function(xhr) {
			if(xhr.responseJSON == undefined) {
				swal('Ocurrió un error', 'No se pudo conectar con el servidor', 'error');
				return;
			}
			swal('Ocurrió un error',
					xhr.status + ' ' + xhr.statusText + ': ' +
					xhr.responseJSON.data.message,
					'error');
		}

	});
}



function limpiarCampos() {
	$('#txtId').val("")
	$('#txtDescripcion').val("");
	$('#txtId_Parametro').val("");
	$('#cmbClasificación').selectpicker('val', 0);
	$('#cmbUnidad').selectpicker('val', 0);
	$('#btnGuardar').show();
	$('#btnModificar, #btnHabilitar, #btnDeshabilitar').hide();
}

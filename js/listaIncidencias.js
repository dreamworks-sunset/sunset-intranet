

$(document).ready(function() {

	consultarIncidencias();
	
});




	

function consultarIncidencias() {
	$.ajax(
		{
			url: HOST + '/api/Incidencia',
			type: 'GET',
			success: function(result) {
				//Destruir la tabla para luego reinicializarla más adelante
                if($.fn.DataTable.isDataTable("#tablaIncidencia")) {
                    $('#tablaIncidencia').DataTable().clear().destroy();
                }
				//$('#tablaIncidencia tbody').empty();
				$.each(result.data, function(index, data) {
					if (data.estatus == 1){
						var porcentaje_descuento = 0;
						if(data.promocion != undefined) {
							porcentaje_descuento = data.promocion.porcentaje_descuento;
						}
					$('#tablaIncidencia tbody').append(
						'<tr id="' + data.id + '">' +
							'<td>' + formatFechaHora(data.fecha_creacion) + '</td>' +
							'<td>' + data.sesion.cita.orden_servicio.solicitud.cliente.nombres + ' ' + data.sesion.cita.orden_servicio.solicitud.cliente.apellidos + '</td>' +
							'<td>' + data.sesion.cita.orden_servicio.solicitud.servicio.nombre + 
							'<td>' + data.id_sesion + '</td>' +
							'<td>' + data.sesion.cita.orden_servicio.solicitud.empleado.nombres + ' ' + data.sesion.cita.orden_servicio.solicitud.empleado.apellidos + '</td>' +
							'<td>' + data.tipo_incidencia.descripcion + '</td>' +
							'<td>' + data.contenido + '</td>' +							


						'</tr>'


	
					);
				
				}			

				});
				//Aquí se reinicializa la tabla
                $(".js-basic-example").dataTable({
                    responsive: true
                });
			
			},
			error: function(xhr) {
				swal('Ocurrió un error',
						xhr.status + ' ' + xhr.statusText + ': ' +
						xhr.responseJSON.data.message,
						'error');
			}
		}
	);
}




function formatFecha(date) {
    //Ejemplo: '2018-04-27T04:34:03.687Z'
    var fechaHoraStr = String(date).split('T'); //Separa la fecha de la hora
    var fecha = fechaHoraStr[0].split('-'); //Separa año, mes y día

    //Concatena cada valor de fecha en formato dd/mm/yyyy
    return fecha[2] + '/' + fecha[1] + '/' + fecha[0];
}
function formatFechaHora(date) {
    //Ejemplo: '2018-04-27T04:34:03.687Z'
    var fechaHoraStr = String(date).split('T'); //Separa la fecha de la hora
    var fecha = fechaHoraStr[0].split('-'); //Separa año, mes y día

    //Concatena cada valor de fecha en formato dd/mm/yyyy
    var f = fecha[2] + '/' + fecha[1] + '/' + fecha[0];

    var hora = fechaHoraStr[1].split(':'); //Separa hora, minuto y segundo
    var h = parseInt(hora[0]); //Convierte hora a entero en formato 24h
    var min = parseInt(hora[1]);
    var a = (h < 12) ? 'AM' : 'PM'; //Si h es menor a 12 es AM, si no, PM
    if(h == 0) {
        h = 12;
    } else if(h > 12) {
        h = (h % 12 < 10) ? '0' + (h % 12) : (h % 12);
    }
    min = (min < 10) ? '0' + min : min;
    return f + ' ' + h + ':' + min + ' ' + a;
}







$(document).ready(function () {
   
    $("#tablaReporte").hide();
    
    $("#grafica").hide();


    $('#btnGenerar').click(function() {
        $("#tablaReporte").show();
        $("#grafica").show();
        $('#bar_chart').empty();
        consultarReporteTipoComentario();
        
    });
     $('#btnCancelar').click(function() {
        $("#tablaReporte").hide();
        $("#grafica").hide();
    });
});

/*

$(function () {
    getMorris('line', 'line_chart');
    getMorris('bar', 'bar_chart');
    getMorris('area', 'area_chart');
    getMorris('donut', 'donut_chart');
});

*/
function getMorris(type, element, datos,tipoComentario, colors) {

    if (type === 'bar') {
        Morris.Bar({

            element: element,
            data: datos,
            xkey: 'periodo',
            ykeys: tipoComentario,
            labels: tipoComentario,
            barColors: colors,
        });
    } 
}

function consultarReporteTipoComentario(){
    $.ajax(
        {
            url: HOST + '/api/vista_reporte_tipo_comentario_por_rango_edad_y_sexo',
            type: 'GET',
            destroy: true,
            async: false,
            success: function(result) {

                var datos = [];


                var colTipoComentario = [] //  arreglo que guarda las columna de los tipo de comentario


                var tiposComentarios = []; //arreglo que almacena la cantidad de tipo
                var idsTiposComentario = {}; //diccionario para almacenar los id de tipo de respuesta con sus valores
                var tipoComentario = {}; //diccionario para almacenar la descripcion del tipo de comentario
                var rangoEdad = {} // dicccionario que almacena la descripcion del rango de edad
                var genero = {} // diccionario que almacena el sexo
                var colors = {};
                //Destruir la tabla para luego reinicializarla más adelante
                if($.fn.DataTable.isDataTable("#tablaReporteTipoComentario2")) {
                    $('#tablaReporteTipoComentario2').DataTable().clear().destroy();
                }
                //$('#tablaReporteTipoComentario2 tbody').empty();
                $('#tablaReporteTipoComentario2 thead tr').html('<th class="col-md-5">Rango de edad</th>')
                
               


                $.each(result.data, function(index, data) {
                    tipoComentario[data.id_tipo_comentario] = data.tipo_comentario; // nombre tipo comentario
                });
                $.each(tipoComentario, function(index, data2) {
                    $('#tablaReporteTipoComentario2 thead tr:last-child').append(
                        '<th data-id-tipo-comentario="'+ index +'">' + data2 + '</th>' 
                    );
                        
                    colTipoComentario.push(index);
                })
                console.log('colTipoComentario = ', colTipoComentario);
                $.each(result.data, function(index, data) {

                    var fecha_ini = formatFechaDMYToYMD($('#dtpFechaInicio').val());
                    var fecha_fin = formatFechaDMYToYMD($('#dtpFechaFin').val());
                    var fecha_creacion = new Date(data.fecha);

                    //Filtrar por fecha
                    if(fecha_creacion.getTime() < fecha_ini.getTime() || fecha_creacion.getTime() > fecha_fin.getTime()) {
                        return true; //continúa en la siguiente iteración
                    }

                    var sexo = $('#cmbSexo').val();
                    if(sexo != data.sexo) {
                        return true;
                    }
         
                    /*if(genero[data.sexo] == undefined) {
                        genero[data.sexo] = {};
                    }*/
                    rangoEdad[data.rango] = data.rango;

                    //$.each(rangoEdad, function(index, dataRango) {
                        if(genero[data.rango] == undefined) {
                            genero[data.rango] = {};
                        }
                        //$.each(idsTiposComentario, function(indexTipo, dataTipo) {
                            if(genero[data.rango][data.id_tipo_comentario] == undefined) {
                                genero[data.rango][data.id_tipo_comentario] = 1.0;
                            } else {
                                genero[data.rango][data.id_tipo_comentario]++;
                            }
                        //})
                    //})

                    idsTiposComentario[data.id_tipo_comentario] = data.tipo_comentario;
                    colors[data.id_tipo_comentario] = getRandomColor();
                        
                        if(tiposComentarios[data.id_tipo_comentario] == undefined) {
                            tiposComentarios[data.id_tipo_comentario] = 1.0;
                        } else {
                            tiposComentarios[data.id_tipo_comentario]++;
                        }
                       
                });
                console.log("genero es el siguiente:");
                console.log(genero)
                
                $.each(genero, function(index, data3) {
                        $('#tablaReporteTipoComentario2 tbody').append(
                            '<tr id="tr' + index + '">' + 
                               '<td>' + index + '</td>' +
                            '</tr>'
                        );

                        $.each(colTipoComentario, function(index, dataCol) {
                            $('#tablaReporteTipoComentario2 tbody tr:last-child').append(
                                    '<td data-id-tipo-comentario="' + dataCol + '">0</td>'
                            );
                        })
                      //  console.log(data3)
                    $.each(data3, function(index2, data31) {
                        $('#tablaReporteTipoComentario2 tbody tr:last-child td[data-id-tipo-comentario="' + index2 + '"]')
                        .html(data31);
                    //  console.log(tiposComentarios[index2])
                    })
                });
                //Aquí se reinicializa la tabla
                $(".js-basic-example").dataTable({
                    responsive: true
                });

                //$.each(fecha, function(index, data) {
                    $.each(genero, function(index, datas) {
                        console.log('index',index)
                        console.log('datos',datas)
                        var obj = {};
                        
                        obj['periodo'] = index;
                        $.each(tipoComentario, function(indexTipo, dataTipo) {
                            $.each(datas, function(indexs2, datas2) {
                                if(indexTipo == indexs2){
                                    obj[dataTipo] = datas2;
                                }
                                
                            });
                           
                        });
                         datos.push(obj);
                    });
                //});
                getMorris('bar', 'bar_chart',datos, Object.values(tipoComentario), Object.values(colors));
                
            },
            error: function(xhr) {
                swal('Ocurrió un error',
                        xhr.status + ' ' + xhr.statusText + ': ' +
                        xhr.responseJSON.data.message,
                        'error');
            }
        }
    );

}

function getRandomColor() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 3; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  }

function formatFechaDMYToYMD(date) {
    var fecha = date.split('/');
    var fec = new Date(fecha[2], parseInt(fecha[1]) - 1, fecha[0]);
    return fec;
}

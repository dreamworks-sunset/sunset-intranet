$(document).ready(function () {
    consultarServiciosCalificados();
    $("#tablaReporte").hide();
    
    $("#grafica").hide();


    $('#btnGenerar').click(function() {
        $("#tablaReporte").show();
        $("#grafica").show();
        $('#line_chart').empty();
        consultarReporteNivelConfianza();
        
    });
     $('#btnCancelar').click(function() {
        $("#tablaReporte").hide();
        $("#grafica").hide();
    });
});


function getMorris(type, element, datos, criterios, colors) {

    if (type === 'line') {
        Morris.Line({

            element: element,
            data: datos,
            xkey: 'periodo',
            ykeys: criterios,
            labels: criterios,
            lineColors: colors,
            lineWidth: 3
        });
    } 
}

function consultarReporteNivelConfianza(){
    $.ajax(
        {
            url: HOST + '/api/vista_reporte_nivel_confianza',
            type: 'GET',
            destroy: true,
            async: false,
            success: function(result) {
                var datos = [];
                var criterios = {};
                var colors = {};
                //Destruir la tabla para luego reinicializarla más adelante
                if($.fn.DataTable.isDataTable("#tablaReporteNivelConfianza")) {
                    $('#tablaReporteNivelConfianza').DataTable().clear().destroy();
                }
                //$('#tablaReporteNivelConfianza tbody').empty();
                var fechas = {};
                var annoInicial = $('#annoInicial').val();
                var mesInicial = $('#mesInicial').val();
                var annoFinal = $('#annoFinal').val();
                var mesFinal = $('#mesFinal').val();
                var fechaIni = new Date(annoInicial, parseInt(mesInicial) - 1);
                var fechaFin = new Date(annoFinal, parseInt(mesFinal) - 1);
                $.each(result.data, function(index, data) {
                    var fecha = new Date(data.anno, parseInt(data.mes) - 1);
                    if(fecha.getTime() < fechaIni.getTime() || fecha.getTime() > fechaFin.getTime()) {
                        return true;
                    }

                    var idServicio = $('#cmbServicio').val();
                    if(idServicio != 0 && idServicio != data.id_servicio) {
                        return true;
                    }
                    $('#tablaReporteNivelConfianza tbody').append(
                        '<tr>' +
                            '<td>' + data.anno + '</td>' +                        
                            '<td>' + data.mes + '</td>' +
                            '<td>' + data.nombre + '</td>' +                            
                            '<td>' + data.descripcion + '</td>' +
                            '<td>' + data.promedio + '</td>' +
                        '</tr>'
                    );
                    criterios[data.descripcion] = true;
                    fechas[data.anno + '-' + ((data.mes < 10) ? '0' + data.mes : data.mes)] = data.anno + '-' + ((data.mes < 10) ? '0' + data.mes : data.mes);
                    colors[data.descripcion] = getRandomColor();                    
                });
                //Aquí se reinicializa la tabla
                $(".js-basic-example").dataTable({
                    responsive: true
                });
                $.each(fechas, function(index, data) {
                    var obj = {};
                    obj['periodo'] = data;
                    $.each(result.data, function(index, data2) {
                        if(data2.anno + '-' + ((data2.mes < 10) ? '0' + data2.mes : data2.mes) == data) {
                            obj[data2.descripcion] = data2.promedio;
                        }
                    });
                    datos.push(obj);
                });
                getMorris('line', 'line_chart', datos, Object.keys(criterios), Object.values(colors));
                
            },
            error: function(xhr) {
                swal('Ocurrió un error',
                        xhr.status + ' ' + xhr.statusText + ': ' +
                        xhr.responseJSON.data.message,
                        'error');
            }
        }
    );

}

function getRandomColor() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 3; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}

function consultarServiciosCalificados() {
    $('#cmbServicio').html('<option value="0">Todos</option>');
    $.ajax(
    {
        url: HOST + '/api/evaluacion_sesion',
        type: 'GET',
        success: function(result) {
            $.each(result.data, function(index, data) {
                var servicio = data.sesion.cita.orden_servicio.solicitud.servicio;
                if($('#cmbServicio option[value="' + servicio.id + '"]').length == 0) {
                    $('#cmbServicio').append(
                        '<option value="' + servicio.id + '">' + servicio.nombre + '</option>'
                    );
                }
            });
            $('#cmbServicio').selectpicker('refresh');
        },
        error: function(xhr) {
            swal('Ocurrió un error',
                    xhr.status + ' ' + xhr.statusText + ': ' +
                    xhr.responseJSON.data.message,
                    'error');
        }
    });
}


$(document).ready(function() {

	limpiarCampos();
	consultarSistema();
	
	$('#btnGuardar').click(function() {
		var form = document.getElementById("formSistema");
		var postData = new FormData(form);
	
		$.ajax(
			{
				url: HOST + '/api/sistema',
				type: 'POST',
				dataType: 'json',
				data: postData,
				processData: false,
				contentType: false,
				success: function(data) {
					swal('Registro exitoso', '', 'success');
					limpiarCampos();
					consultarSistema();
				},
				error: function(xhr) {
					if(xhr.responseJSON == undefined) {
						swal('Ocurrió un error', 'No se pudo conectar con el servidor', 'error');
						return;
					}
					swal('Ocurrió un error',
							xhr.status + ' ' + xhr.statusText + ': ' +
							xhr.responseJSON.data.message,
							'error');
				}
			}
		);
	});

	$('#btnModificar').click(function() {
		var form = document.getElementById("formSistema");
		var putData = new FormData(form);
		putData.append('cambio_estatus' , 'false');
		
		var id = $('#txtId').val();
		$.ajax(
			{
				url: HOST + '/api/sistema/' + id,
				type: 'PUT',
				dataType: 'json',
				data: putData,
				processData: false,
				contentType: false,
				success: function(data) {
					swal('Modificación exitosa', '', 'success');
					limpiarCampos();
					consultarSistema();
				},
				error: function(xhr) {
					swal('Ocurrió un error',
							xhr.status + ' ' + xhr.statusText + ': ' +
							xhr.responseJSON.data.message,
							'error');
				}
			}
		);
	});

	$('#btnHabilitar').click(function() {
		var putData = new FormData();
		putData.append('estatus', '1');
		putData.append('cambio_estatus' , 'true');

		var id = $('#txtId').val();
		$.ajax(
			{
				url: HOST + '/api/sistema/' + id,
				type: 'PUT',
				dataType: 'json',
				data: putData,
				processData: false,
				contentType: false,
				success: function(data) {
					swal('Habilitación exitosa', '', 'success');
					limpiarCampos();
					consultarSistema();
				},
				error: function(xhr) {
					swal('Ocurrió un error',
							xhr.status + ' ' + xhr.statusText + ': ' +
							xhr.responseJSON.data.message,
							'error');
				}
			}
		);
	});

	$('#btnDeshabilitar').click(function() {
		var putData = new FormData();
		putData.append('estatus', '0');
		putData.append('cambio_estatus' , 'true');

		var id = $('#txtId').val();
		$.ajax(
			{
				url: HOST + '/api/sistema/' + id,
				type: 'PUT',
				dataType: 'json',
				data: putData,
				processData: false,
				contentType: false,
				success: function(data) {
					swal('Deshabilitación exitosa', '', 'success');
					limpiarCampos();
					consultarSistema();
				},
				error: function(xhr) {
					swal('Ocurrió un error',
							xhr.status + ' ' + xhr.statusText + ': ' +
							xhr.responseJSON.data.message,
							'error');
				}
			}
		);
	});

	$('#btnCancelar').click(function() {
		limpiarCampos();
	});

});

function seleccionarFila(el) {
	$('#txtId').val($(el).attr('id'));
	$('#txtNombre').val($(el).children('td').eq(0).text());
	$('#imgImagen').attr('src' ,$(el).data('logo'));
	$('#imgImagen').show();
	var habilitado = $(el).children('td').eq(2).data('estatus');
	if(habilitado == 1) {
		$('#btnDeshabilitar').show();
		$('#btnHabilitar').hide();
	} else {
		$('#btnDeshabilitar').hide();
		$('#btnHabilitar').show();
	}
	$('#btnGuardar').hide();
	$('#btnModificar').show();
}



function consultarSistema() {
	$.ajax(
		{
			url: HOST + '/api/sistema',
			type: 'GET',
			success: function(result) {
				//Destruir la tabla para luego reinicializarla más adelante
                if($.fn.DataTable.isDataTable("#tablaSistema")) {
                    $('#tablaSistema').DataTable().clear().destroy();
                }
				//$('#tablaSistema tbody').empty();
				$.each(result.data, function(index, data) {
					$('#tablaSistema tbody').append(
						'<tr data-logo="'+ data.logo + '" id="' + data.id + '" onclick="seleccionarFila(this)">' +
							'<td>' + data.nombre + '</td>' +
							'<td>' + data.logo + '</td>' +
							'<td data-estatus="' + data.estatus + '">' + ((data.estatus == 1) ? 'Sí' : 'No') + '</td>' +
						'</tr>'
					);
				});
				$('.js-exportable').DataTable({
                    dom: 'Bfrtip',
                    responsive: true,
                    buttons: [
                        'copy', 'csv', 'excel', 'pdf', 'print'
                    ]
                });
				$('#tablaSistema tbody tr').css('cursor', 'pointer');
				$('td[data-estatus="1"]').addClass('habilitado');
				$('td[data-estatus="0"]').addClass('deshabilitado');
			},
			error: function(xhr) {
				swal('Ocurrió un error',
						xhr.status + ' ' + xhr.statusText + ': ' +
						xhr.responseJSON.data.message,
						'error');
			}
		}
	);
}


function limpiarCampos() {
	$('#txtId').val("")
	$('#txtNombre').val("");
	$('#imgImagen').hide();
	$('#btnGuardar').show();
	$('#btnModificar, #btnHabilitar, #btnDeshabilitar').hide();
}

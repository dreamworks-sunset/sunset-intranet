

let valuesDirigidoA = {}; //Diccionario donde se almacenarán los valores parámetro de "Dirigido A"
let valuesDirigidoAPorEliminar = {} //Diccionario donde se almacenarán los valores parámetro de "Dirigido A" que serán removidos de la base de datos
let valuesParametrosAvance = {}; //Diccionario donde se almacenarán los valores parámetro de "Parámetros de avance"
let valuesParametrosAvancePorEliminar = {} //Diccionario donde se almacenarán los valores parámetro de "Dirigido A" que serán removidos de la base de datos

let valuesTecnicaServicio = {}; //Diccionario donde se almacenarán las tecnicas del servicio
let valuesTecnicaServicioPorEliminar = {} //Diccionario donde se almacenarán la tecnicas del servicio que serán removidos de la base de datos

let valuesGarantiaServicio = {}; //Diccionario donde se almacenarán las tecnicas del servicio
let valuesGarantiaServicioPorEliminar = {} //Diccionario donde se almacenarán la tecnicas del servicio que serán removidos de la base de datos


let valuesInsumoServicio = {}; //Diccionario donde se almacenarán las tecnicas del servicio
let valuesInsumoServicioPorEliminar = {} //Diccionario donde se almacenarán la tecnicas del servicio que serán removidos de la base de datos





$(document).ready(function() {

	    $('#btnAnadirValor').click(function() {
        var insumo = $('#cmbInsumo option:selected');
        var cantida = $('#txtCantidad').val();
        var unidad = $('#cmbUnidad option:selected');


        // Voy a añadir un insumo, elimino la fila "No se han añadido insumos"
        $('#sinValor').remove();
        
        // Añade un insumo a la tabla
        $('#tablaInsumo tbody').append(
            '<tr data-id-insumo="' + insumo.val() + '" data-id-unidad="' + unidad.val() + '" data-cantidad="'+ cantida +'">' +
                '<td>' + insumo.text() + '</td>' +
                '<td>' + cantida + '</td>' +
                '<td>' + unidad.text() + '</td>' +
                '<td class="centrado"> <button type="button" rel="tooltip"  title="Eliminar" class="btn btn-danger btn-circle waves-effect waves-circle waves-float insumo-tabla" onclick="eliminarInsumo(this)"> <i class="material-icons">delete</i> </button> </td>' +
            '</tr>'

        );
        limpiarInsumo() ;
        	// 
			var idInsumoPorAnadir = $('#tablaInsumo tbody tr:last-child').data('id-insumo');
			var idUnidadPorAnadir = $('#tablaInsumo tbody tr:last-child').data('id-unidad');
			var cantidadPorAnadir = $('#tablaInsumo tbody tr:last-child').data('cantidad');

            valuesInsumoServicio[idInsumoPorAnadir] = {
            	id_insumo: idInsumoPorAnadir,
            	id_unidad: idUnidadPorAnadir,
            	cantidad: cantidadPorAnadir
            }
            valuesInsumoServicioPorEliminar[idInsumoPorAnadir] = false;
		

    });

    $('#valoresParametro').multiSelect({
		selectableOptgroup: true, //Permite seleccionar un grupo de valores parámetro completo
		afterSelect: function (values) {
			//Iteramos por cada elemento seleccionado (uno o más a la vez)
			values.forEach(element => {
				valuesDirigidoA[element] = element; //Si se selecciona valor parámetro "1", se setea el campo clave "1" con el valor en cuestión
				valuesDirigidoAPorEliminar[element] = false; //En caso de que se haya removido y se selecciona de nuevo, debe setearse en false
			});
			console.log(valuesDirigidoA);
		},
		afterDeselect: function (values) {
			//Iteramos por cada elemento seleccionado (uno o más a la vez)
			values.forEach(element => {
				valuesDirigidoA[element] = false; //Si se deselecciona valor parámetro "1", se setea el campo clave "1" con el valor false
				valuesDirigidoAPorEliminar[element] = element;
			});
			console.log(valuesDirigidoA);
			console.log('Se eliminarán:', valuesDirigidoAPorEliminar);
		}
	});

	$('#valoresParametroAvance').multiSelect({
		selectableOptgroup: true, //Permite seleccionar un grupo de valores parámetro completo
		afterSelect: function (values) {
			//Iteramos por cada elemento seleccionado (uno o más a la vez)
			values.forEach(element => {
				valuesParametrosAvance[element] = element; //Si se selecciona valor parámetro "1", se setea el campo clave "1" con el valor en cuestión
				valuesParametrosAvancePorEliminar[element] = false;
			});
			console.log(valuesParametrosAvance);
		},
		afterDeselect: function (values) {
			//Iteramos por cada elemento seleccionado (uno o más a la vez)
			values.forEach(element => {
				valuesParametrosAvance[element] = false; //Si se deselecciona valor parámetro "1", se setea el campo clave "1" con el valor false
				valuesParametrosAvancePorEliminar[element] = element;
			});
			console.log(valuesParametrosAvance);
			console.log('Se eliminarán:', valuesParametrosAvancePorEliminar);
		}
	});

	$('#tecnicaServicio').multiSelect({
		selectableOptgroup: true, //Permite seleccionar un grupo d tecnicas completo
		afterSelect: function (values) {
			//Iteramos por cada elemento seleccionado (uno o más a la vez)
			values.forEach(element => {
				valuesTecnicaServicio[element] = element; //Si se selecciona valor parámetro "1", se setea el campo clave "1" con el valor en cuestión
				valuesTecnicaServicioPorEliminar[element] = false;
			});
			console.log(valuesTecnicaServicio);
		},
		afterDeselect: function (values) {
			//Iteramos por cada elemento seleccionado (uno o más a la vez)
			values.forEach(element => {
				valuesTecnicaServicio[element] = false; //Si se deselecciona valor parámetro "1", se setea el campo clave "1" con el valor false
				valuesTecnicaServicioPorEliminar[element] = element;
			});
			console.log(valuesTecnicaServicio);
			console.log('Se eliminarán:', valuesTecnicaServicioPorEliminar);
		}
	});

	$('#garantiaServicio').multiSelect({
		selectableOptgroup: true, //Permite seleccionar un grupo d tecnicas completo
		afterSelect: function (values) {
			//Iteramos por cada elemento seleccionado (uno o más a la vez)
			values.forEach(element => {
				valuesGarantiaServicio[element] = element; //Si se selecciona valor parámetro "1", se setea el campo clave "1" con el valor en cuestión
				valuesGarantiaServicioPorEliminar[element] = false;
			});
			console.log(valuesGarantiaServicio);
		},
		afterDeselect: function (values) {
			//Iteramos por cada elemento seleccionado (uno o más a la vez)
			values.forEach(element => {
				valuesGarantiaServicio[element] = false; //Si se deselecciona valor parámetro "1", se setea el campo clave "1" con el valor false
				valuesGarantiaServicioPorEliminar[element] = element;
			});
			console.log(valuesGarantiaServicio);
			console.log('Se eliminarán:', valuesGarantiaServicioPorEliminar);
		}
	});




 	limpiarCampos();
	consultarServicio();
	consultarTipoServicio();
	consultarValoresParametro();
	consultarValoresParametroDeAvance();
	consultarTecnica();
	consultarGarantia();
	consultarInsumo();
	consultarUnidad();

	$('#btnGuardar').click(function() {
		var form = document.getElementById("formServicio");
		var postData = new FormData(form);

		

		var valoresParametroDirigidoA = Object.values(valuesDirigidoA);
		var valoresDirigidoA = [];
		valoresParametroDirigidoA.forEach(valor => {
			if(valor) {
				valoresDirigidoA.push(valor);
			}
		});
		console.log('valores =', valoresDirigidoA);
		var valoresParametroAvance = Object.values(valuesParametrosAvance);
		var valoresAvance = [];
		valoresParametroAvance.forEach(valor => {
			if(valor) {
				valoresAvance.push(valor);
			}
		});
		console.log('valores avance =', valoresAvance);
		
		// tecnica servicio

		var tecnicaServicio = Object.values(valuesTecnicaServicio);
		var valoresTecnica= [];
		tecnicaServicio.forEach(valor => {
			if(valor) {
				valoresTecnica.push(valor);
			}
		});
		console.log('tecnicas =', valoresTecnica);
		// fin tecnica

		// garantia servicio

		var garantiaServicio = Object.values(valuesGarantiaServicio);
		var valoresGarantia = [];
		garantiaServicio.forEach(valor => {
			if(valor) {
				valoresGarantia.push(valor);
			}
		});
		console.log('condicion garantia =', valoresGarantia);
		// fin garantia
		
		// insumo servicio
		var insumoServicio = Object.values(valuesInsumoServicio);
		var valoresInsumo = [];
		insumoServicio.forEach(valor => {
			if(valor) {
				valoresInsumo.push(valor);
			}
		});
		console.log('insumo =',valoresInsumo);
		// fin insumo
		

		postData.append('valor_parametro_servicio', valoresDirigidoA);
		postData.append('valor_parametro_avance', valoresAvance);
		postData.append('tecnica', valoresTecnica);
		postData.append('condicion_garantia', valoresGarantia);
		postData.append('insumo', JSON.stringify(valoresInsumo));
		$.ajax(
			{
				url: HOST + '/api/servicio',
				type: 'POST',
				dataType: 'json',
				data: postData,
				processData: false,
				contentType: false,
				success: function(data) {
					swal('Registro exitoso', '', 'success');
					limpiarCampos();
					consultarServicio();
				},
				error: function(xhr) {
					if(xhr.responseJSON == undefined) {
						swal('Ocurrió un error', 'No se pudo conectar con el servidor', 'error');
						return;
					}
					swal('Ocurrió un error',
							xhr.status + ' ' + xhr.statusText + ': ' +
							xhr.responseJSON.data.message,
							'error');
				}
			}
		);
	});

	$('#btnModificar').click(function() {
		var form = document.getElementById("formServicio");
		var putData = new FormData(form);
		console.log(form);
		var id = $('#txtId').val();
		var valoresParametroDirigidoA = Object.values(valuesDirigidoA);
		var valoresDirigidoA = [];
		valoresParametroDirigidoA.forEach(valor => {
			if(valor) {
				valoresDirigidoA.push(valor);
			}
		});
		console.log('valores =', valoresDirigidoA);
		var valoresParametroDirigidoAPorEliminar = Object.values(valuesDirigidoAPorEliminar);
		var valoresDirigidoAPorEliminar = [];
		valoresParametroDirigidoAPorEliminar.forEach(valor => {
			if(valor) {
				valoresDirigidoAPorEliminar.push(valor);
			}
		});
		console.log('valores por eliminar =', valoresDirigidoAPorEliminar);
		var valoresParametroAvance = Object.values(valuesParametrosAvance);
		var valoresAvance = [];
		valoresParametroAvance.forEach(valor => {
			if(valor) {
				valoresAvance.push(valor);
			}
		});
		console.log('valores avance =', valoresAvance);
		var valoresParametroAvancePorEliminar = Object.values(valuesParametrosAvancePorEliminar);
		var valoresAvancePorEliminar = [];
		valoresParametroAvancePorEliminar.forEach(valor => {
			if(valor) {
				valoresAvancePorEliminar.push(valor);

			}
		});
		console.log('valores avance por eliminar =', valoresAvancePorEliminar);
		
		// tecnica
		var tecnicaServicio = Object.values(valuesTecnicaServicio);
		var valoresTecnica= [];
		tecnicaServicio.forEach(valor => {
			if(valor) {
				valoresTecnica.push(valor);
			}
		});
		console.log('tecnicas =', valoresTecnica);
		
		var tecnicaServicioPorEliminar = Object.values(valuesTecnicaServicioPorEliminar);
		var valoresTecnicaPorEliminar= [];
		tecnicaServicioPorEliminar.forEach(valor => {
			if(valor) {
				valoresTecnicaPorEliminar.push(valor);
			}
		});
		console.log('tecnicas por eliminar =', valoresTecnicaPorEliminar);
		// fin tecnica

		// garantia
		var garantiaServicio = Object.values(valuesGarantiaServicio);
		var valoresGarantia = [];
		garantiaServicio.forEach(valor => {
			if(valor) {
				valoresGarantia.push(valor);
				
			}
		});
		console.log('garantia =', valoresGarantia);
		
		var garantiaServicioPorEliminar = Object.values(valuesGarantiaServicioPorEliminar);
		var valoresGarantiaPorEliminar= [];
		garantiaServicioPorEliminar.forEach(valor => {
			if(valor) {
				valoresGarantiaPorEliminar.push(valor);
			}
		});
		console.log('garantia por eliminar =', valoresGarantiaPorEliminar);
		// fin garantia

		// insumo servicio
		var insumoServicio = Object.values(valuesInsumoServicio);
		var valoresInsumo = [];
		insumoServicio.forEach(valor => {
			if(valor) {
				valoresInsumo.push(valor);
			}
		});
		console.log('insumo',valoresInsumo);

		var insumoServicioPorEliminar = Object.values(valuesInsumoServicioPorEliminar);
		var valoresInsumoPorEliminar = [];
		insumoServicioPorEliminar.forEach(valor => {
			if(valor) {
				valoresInsumoPorEliminar.push(valor);
			}
		});
		console.log('insumo por eliminar',valoresInsumoPorEliminar);
		// fin insumo

		putData.append('valor_parametro_servicio', valoresDirigidoA);
		putData.append('valor_parametro_servicio_eliminar', valoresDirigidoAPorEliminar);
		putData.append('valor_parametro_avance', valoresAvance);
		putData.append('valor_parametro_avance_eliminar', valoresAvancePorEliminar);
		putData.append('tecnica', valoresTecnica);
		putData.append('tecnica_eliminar', valoresTecnicaPorEliminar);
		putData.append('condicion_garantia', valoresGarantia);
		putData.append('condicion_garantia_eliminar', valoresGarantiaPorEliminar);
		putData.append('insumo', JSON.stringify(valoresInsumo));
		putData.append('insumo_eliminar', JSON.stringify(valoresInsumoPorEliminar));
		putData.append('cambio_estatus', 'false');
		
		$.ajax(
			{
				url: HOST + '/api/servicio/' + id,
				type: 'PUT',
				dataType: 'json',
				data: putData,
				processData: false,
				contentType: false,
				success: function(data) {
					swal('Modificación exitosa', '', 'success');
					limpiarCampos();
					consultarServicio();
				},
				error: function(xhr) {
					if(xhr.responseJSON == undefined) {
						swal('Ocurrió un error', 'No se pudo conectar con el servidor', 'error');
						return;
					}
					swal('Ocurrió un error',
							xhr.status + ' ' + xhr.statusText + ': ' +
							xhr.responseJSON.data.message,
							'error');
				}
			}
		);
	});

	$('#btnHabilitar').click(function() {
		var putData = new FormData();
		putData.append('estatus', '1');
		putData.append('cambio_estatus', 'true');
		var id = $('#txtId').val();
		$.ajax(
			{
				url: HOST + '/api/servicio/' + id,
				type: 'PUT',
				dataType: 'json',
				data: putData,
				processData: false,
				contentType: false,
				success: function(data) {
					swal('Habilitación exitosa', '', 'success');
					limpiarCampos();
					consultarServicio();
				},
				error: function(xhr) {
					if(xhr.responseJSON == undefined) {
						swal('Ocurrió un error', 'No se pudo conectar con el servidor', 'error');
						return;
					}
					swal('Ocurrió un error',
							xhr.status + ' ' + xhr.statusText + ': ' +
							xhr.responseJSON.data.message,
							'error');
				}
			}
		);
	});

	$('#btnDeshabilitar').click(function() {
		var putData = new FormData();
		putData.append('estatus', '0');
		putData.append('cambio_estatus', 'true');
		var id = $('#txtId').val();
		$.ajax(
			{
				url: HOST + '/api/servicio/' + id,
				type: 'PUT',
				dataType: 'json',
				data: putData,
				processData: false,
				contentType: false,
				success: function(data) {
					swal('Deshabilitación exitosa', '', 'success');
					limpiarCampos();
					consultarServicio();
				},
				error: function(xhr) {
					if(xhr.responseJSON == undefined) {
						swal('Ocurrió un error', 'No se pudo conectar con el servidor', 'error');
						return;
					}
					swal('Ocurrió un error',
							xhr.status + ' ' + xhr.statusText + ': ' +
							xhr.responseJSON.data.message,
							'error');
				}
			}
		);
	});

	$('#btnCancelar').click(function() {
		limpiarCampos();

	});

});

function seleccionarFila(el) {
	var idServicio = $(el).attr('data-id-servicio');

	$('#imgImagen').attr('src',$(el).data('imagen'));
	$('#imgImagen').show();
	$('#txtId').val(idServicio);
	$('#txtNombre').val($(el).children('td').eq(0).text());
	$('#txtDescripcion').val($(el).children('td').eq(1).text());
	$('#txtCantidadSesiones').val($(el).children('td').eq(3).text());
	$('#txtPrecioPorSesion').val($(el).children('td').eq(4).text());
	$('#txtTiempoPorSesion').val($(el).children('td').eq(5).text());
	$('#cmbTipoServicio').selectpicker('val', $(el).children('td').eq(2).data('id-tipo-servicio'));
	var habilitado = $(el).children('td').eq(6).data('estatus');
	if(habilitado == 1) {
		$('#btnDeshabilitar').show();
		$('#btnHabilitar').hide();
	} else {
		$('#btnDeshabilitar').hide();
		$('#btnHabilitar').show();
	}
	$('#btnGuardar').hide();
	$('#btnModificar').show();
	$('.ms optgroup option:selected').removeAttr('selected'); //Remueve las selecciones de todos los select multiple
	$('.ms').multiSelect('refresh'); //Actualiza los select multiple

	//Setea todos los valores actuales de los diccionarios en false
	limpiarDiccionario(valuesDirigidoA);
	limpiarDiccionario(valuesDirigidoAPorEliminar);
	limpiarDiccionario(valuesParametrosAvance);
	limpiarDiccionario(valuesParametrosAvancePorEliminar);
	limpiarDiccionario(valuesTecnicaServicio);
	limpiarDiccionario(valuesTecnicaServicioPorEliminar);
	limpiarDiccionario(valuesGarantiaServicio);
	limpiarDiccionario(valuesGarantiaServicioPorEliminar);
	limpiarDiccionario(valuesInsumoServicio);
	limpiarDiccionario(valuesInsumoServicioPorEliminar);

	//FIN limpiar diccionarios

	consultarValoresParametroPorServicio(idServicio);
	consultarValoresParametroDeAvancePorServicio(idServicio);
	consultarTecnicaPorServicio(idServicio);
	consultarGarantiaPorServicio(idServicio);
	consultarInsumoPorServicio(idServicio);
}

function consultarServicio() {
	$.ajax(
		{
			url: HOST + '/api/servicio',
			type: 'GET',
			success: function(result) {
				//Destruir la tabla para luego reinicializarla más adelante
                if($.fn.DataTable.isDataTable("#tablaServicio")) {
                    $('#tablaServicio').DataTable().clear().destroy();
                }
				//$('#tablaServicio tbody').empty();
				$.each(result.data, function(index, data) {
					$('#tablaServicio tbody').append(
						'<tr data-id-servicio="' + data.id + '" data-imagen="' + data.imagen + '" data-id-tipo-servicio="' + data.id_tipo_servicio + '"onclick="seleccionarFila(this)">' +
							'<td>' + data.nombre + '</td>' +
							'<td>' + data.descripcion + '</td>' +
							'<td data-id-tipo-servicio="' + data.id_tipo_servicio + '">' + data.tipo_servicio.descripcion + '</td>' +
							'<td>' + data.cantidad_sesiones + '</td>' +
							'<td>' + data.precio_por_sesion + '</td>' +
							'<td>' + data.tiempo_por_sesion + '</td>' +
							'<td data-estatus="' + data.estatus + '">' + ((data.estatus == 1) ? 'Sí' : 'No') + '</td>' +
						'</tr>'
					);
				});
				$('.js-exportable').DataTable({
                    dom: 'Bfrtip',
                    responsive: true,
                    buttons: [
                        'copy', 'csv', 'excel', 'pdf', 'print'
                    ]
                });
				$('#tablaPromocion tbody tr').css('cursor', 'pointer');
				$('td[data-estatus="1"]').addClass('habilitado');
				$('td[data-estatus="0"]').addClass('deshabilitado');
			},
			error: function(xhr) {
				if(xhr.responseJSON == undefined) {
					swal('Ocurrió un error', 'No se pudo conectar con el servidor', 'error');
					return;
				}
				swal('Ocurrió un error',
						xhr.status + ' ' + xhr.statusText + ': ' +
						xhr.responseJSON.data.message,
						'error');
			}
		}
	);
}

function limpiarInsumo() {
	$('#txtCantidad').val("");
	$('#cmbUnidad').selectpicker('val', 0);
	$('#cmbInsumo').selectpicker('val', 0);
	
}
function limpiarCampos() {
	$('#tablaInsumo tbody').empty();
	if($('#tablaInsumo tbody tr').length === 0) {
        $('#tablaInsumo tbody').html(
            '<tr id="sinValor">' +
                '<td class="centrado" colspan="5">' +
                    '<i>No se han añadido datos a la tabla</i>' +
                '</td>' +
            '</tr>'
        );
	}
	$('#imgImagen').attr('src', '');
    $('#imgImagen').hide();
	$('#txtId').val("");
	$('#txtNombre').val("");
	$('#txtDescripcion').val("");
	$('#txtPrecioPorSesion').val("");
	$('#txtCantidadSesiones').val("");
	$('#txtClausula').val("");
	$('#txtTiempoPorSesion').val("");
	$('#cmbTipoServicio').selectpicker('val', 0);
	$('#elegir').val("");
	$('#btnGuardar').show();
	$('#btnModificar, #btnDeshabilitar, #btnHabilitar').hide();
	$('.ms optgroup option:selected').removeAttr('selected'); //Remueve las selecciones de todos los select multiple
	$('.ms').multiSelect('refresh'); //Actualiza los select multiple
	 limpiarInsumo() ;
	//Setea todos los valores actuales de los diccionarios en false
	limpiarDiccionario(valuesDirigidoA);
	limpiarDiccionario(valuesDirigidoAPorEliminar);
	limpiarDiccionario(valuesParametrosAvance);
	limpiarDiccionario(valuesParametrosAvancePorEliminar);
	limpiarDiccionario(valuesTecnicaServicio);
	limpiarDiccionario(valuesTecnicaServicioPorEliminar);
	limpiarDiccionario(valuesGarantiaServicio);
	limpiarDiccionario(valuesGarantiaServicioPorEliminar);
	limpiarDiccionario(valuesInsumoServicio);
	limpiarDiccionario(valuesInsumoServicioPorEliminar);

		//FIN limpiar diccionarios
	resetFile(); //Limpiar input de imagen
}

function resetFile() {
	$('#elegir').wrap('<form></form>').closest('form').get(0).reset();
	$('#elegir').unwrap();
}

function formatFecha(date) {
    //Ejemplo: '2018-04-27T04:34:03.687Z'
    var fechaHoraStr = String(date).split('T'); //Separa la fecha de la hora
    var fecha = fechaHoraStr[0].split('-'); //Separa año, mes y día

    //Concatena cada valor de fecha en formato dd/mm/yyyy
    return fecha[2] + '/' + fecha[1] + '/' + fecha[0];
}

function consultarTipoServicio() {
	$.ajax(
	{
		url: HOST + '/api/tipo_servicio',
		type: 'GET',
		success: function(result) {
			$('#cmbTipoServicio').html('<option value="0" selected disabled hidden>Tipo servicio</option>');

			$.each(result.data, function(index, data) {
				$('#cmbTipoServicio').append( ((data.estatus == 1) ? '<option value="' + data.id + '">' + data.descripcion  + '</option>': 'NO TIENE'));
			});
			$('#cmbTipoServicio').selectpicker('refresh');
		},
		error: function(xhr) {
			if(xhr.responseJSON == undefined) {
				swal('Ocurrió un error', 'No se pudo conectar con el servidor', 'error');
				return;
			}
			swal('Ocurrió un error',
					xhr.status + ' ' + xhr.statusText + ': ' +
					xhr.responseJSON.data.message,
					'error');
		}

	});
}


function consultarUnidad() {
	$.ajax(
	{
		url: HOST + '/api/unidad',
		type: 'GET',
		success: function(result) {
			$('#cmbUnidad').html('<option value="0" selected disabled hidden> - </option>');

			$.each(result.data, function(index, data) {
				$('#cmbUnidad').append( ((data.estatus == 1) ? '<option value="' + data.id + '">' + data.abreviatura  + '</option>': 'NO TIENE'));
			});
			$('#cmbUnidad').selectpicker('refresh');
		},
		error: function(xhr) {
			if(xhr.responseJSON == undefined) {
				swal('Ocurrió un error', 'No se pudo conectar con el servidor', 'error');
				return;
			}
			swal('Ocurrió un error',
					xhr.status + ' ' + xhr.statusText + ': ' +
					xhr.responseJSON.data.message,
					'error');
		}

	});
}
function consultarValoresParametro() {
	$.ajax(
	{
		url: HOST + '/api/parametro',
		type: 'GET',
		success: function(result) {
			$('#valoresParametro').empty();
			$.each(result.data, function(index, data) {
				$('#valoresParametro').append('<optgroup label="' + data.descripcion + '"></optgroup>');
				$.each(data.valor_parametro, function(index, valor) {
					$('#valoresParametro optgroup:last-child').append(
						((valor.estatus == 1) ? '<option value="' + valor.id + '">' + valor.descripcion + '</option>' : 'NO')
					);
				});
			});
			$('#valoresParametro').multiSelect('refresh');
		},
		error: function(xhr) {
			if(xhr.responseJSON == undefined) {
				swal('Ocurrió un error', 'No se pudo conectar con el servidor', 'error');
				return;
			}
			swal('Ocurrió un error',
					xhr.status + ' ' + xhr.statusText + ': ' +
					xhr.responseJSON.data.message,
					'error');
		}
	});
}

function consultarValoresParametroDeAvance() {
	$.ajax(
	{
		url: HOST + '/api/tipo_parametro/4', //Solo los parámetros de historia médica
		type: 'GET',
		success: function(result) {
			$('#valoresParametroAvance').empty();
			$.each(result.data.parametro, function(index, data) {
				$('#valoresParametroAvance').append('<optgroup label="' + data.descripcion + '"></optgroup>' );
				$.each(data.valor_parametro, function(index, valor) {
					$('#valoresParametroAvance optgroup:last-child').append(
						((valor.estatus == 1) ? '<option value="' + valor.id + '">' + valor.descripcion + '</option>' : 'NO')
					);
				});
			});
			$('#valoresParametroAvance').multiSelect('refresh');
		},
		error: function(xhr) {
			if(xhr.responseJSON == undefined) {
				swal('Ocurrió un error', 'No se pudo conectar con el servidor', 'error');
				return;
			}
			swal('Ocurrió un error',
					xhr.status + ' ' + xhr.statusText + ': ' +
					xhr.responseJSON.data.message,
					'error');
		}
	});
}

function consultarTecnica() {
	$.ajax(
	{
		url: HOST + '/api/tipo_tecnica',
		type: 'GET',
		success: function(result) {
			$('#tecnicaServicio').empty();
			$.each(result.data, function(index, data) {
				$('#tecnicaServicio').append( '<optgroup label="' + data.descripcion + '"></optgroup>' );
				$.each(data.tecnica, function(index, valor) {
					$('#tecnicaServicio optgroup:last-child').append(
						((valor.estatus == 1) ? '<option value="' + valor.id + '">' + valor.nombre + '</option>' : 'NO')
					);
				});
			});
			$('#tecnicaServicio').multiSelect('refresh');
		},
		error: function(xhr) {
			if(xhr.responseJSON == undefined) {
				swal('Ocurrió un error', 'No se pudo conectar con el servidor', 'error');
				return;
			}
			swal('Ocurrió un error',
					xhr.status + ' ' + xhr.statusText + ': ' +
					xhr.responseJSON.data.message,
					'error');
		}
	});
}

function consultarGarantia() {
	$.ajax(
	{
		url: HOST + '/api/condicion_garantia',
		type: 'GET',
		success: function(result) {
			$('#cmbClausula').attr('onchange', 'cargarClausula()');
			$('#cmbClausula').html('<option value="0" selected disabled hidden>Cláusula</option>');
			$.each(result.data, function(index, data) {
				$('#cmbClausula').append('<option value="' + data.id + '"  data-clausula="' + data.clausula + '">Cláusula ' + data.id + '</option>');
			});
			$('#cmbClausula').selectpicker('refresh');



			$('#garantiaServicio').empty();
			$.each(result.data, function(index, data) {
				
				$('#garantiaServicio').append('<option data-clausula="' + data.clausula + '" value="' + data.id + '"> Cláusula ' + data.id + '</option>');
			});

			$('#garantiaServicio').multiSelect('refresh');
		},
		error: function(xhr) {
			if(xhr.responseJSON == undefined) {
				swal('Ocurrió un error', 'No se pudo conectar con el servidor', 'error');
				return;
			}
			swal('Ocurrió un error',
					xhr.status + ' ' + xhr.statusText + ': ' +
					xhr.responseJSON.data.message,
					'error');
		}
	});
}




function cargarClausula(){
	var descripcion = $('#cmbClausula option:selected').data('clausula');
	$('#txtClausula').val(descripcion);   
	console.log(descripcion);
}


function consultarInsumo() {
	$.ajax(
	{
		url: HOST + '/api/insumo',
		type: 'GET',
		success: function(result) {

			$('#cmbInsumo').html('<option value="0" selected disabled hidden> - </option>');

			$.each(result.data, function(index, data) {
				$('#cmbInsumo').append('<option value="' + data.id + '">' + data.nombre + '</option>');
			});
			$('#cmbInsumo').selectpicker('refresh');
		},
		error: function(xhr) {
			if(xhr.responseJSON == undefined) {
				swal('Ocurrió un error', 'No se pudo conectar con el servidor', 'error');
				return;
			}
			swal('Ocurrió un error',
					xhr.status + ' ' + xhr.statusText + ': ' +
					xhr.responseJSON.data.message,
					'error');
		}
	});
}

function consultarValoresParametroPorServicio(idServicio) {
	$.ajax(
	{
		url: HOST + '/api/servicio/' + idServicio,
		type: 'GET',
		success: function(result) {
			$.each(result.data.valor_parametro_servicio, function(index, data) {
				//Por cada valor parámetro asociado al servicio, toma el id y se selecciona
				//el correspondiente valor en el select multiple
				$('#valoresParametro optgroup option[value="' + data.id + '"]').prop('selected', true);
			});
			$('#valoresParametro').multiSelect('refresh');
		},
		error: function(xhr) {
			if(xhr.responseJSON == undefined) {
				swal('Ocurrió un error', 'No se pudo conectar con el servidor', 'error');
				return;
			}
			swal('Ocurrió un error',
					xhr.status + ' ' + xhr.statusText + ': ' +
					xhr.responseJSON.data.message,
					'error');
		}
	});
}

function consultarValoresParametroDeAvancePorServicio(idServicio) {
	$.ajax(
	{
		url: HOST + '/api/servicio/' + idServicio,
		type: 'GET',
		success: function(result) {
			$.each(result.data.valor_parametro_avance, function(index, data) {
				$('#valoresParametroAvance optgroup option[value="' + data.id + '"]').prop('selected', true);
			});
			$('#valoresParametroAvance').multiSelect('refresh');
		},
		error: function(xhr) {
			if(xhr.responseJSON == undefined) {
				swal('Ocurrió un error', 'No se pudo conectar con el servidor', 'error');
				return;
			}
			swal('Ocurrió un error',
					xhr.status + ' ' + xhr.statusText + ': ' +
					xhr.responseJSON.data.message,
					'error');
		}
	});
}

function consultarTecnicaPorServicio(idServicio) {
	$.ajax(
	{
		url: HOST + '/api/servicio/' + idServicio,
		type: 'GET',
		success: function(result) {
			$.each(result.data.tecnica, function(index, data) {
				//Por cada valor parámetro asociado al servicio, toma el id y se selecciona
				//el correspondiente valor en el select multiple
				$('#tecnicaServicio optgroup option[value="' + data.id + '"]').prop('selected', true);
			});
			$('#tecnicaServicio').multiSelect('refresh');
		},
		error: function(xhr) {
			if(xhr.responseJSON == undefined) {
				swal('Ocurrió un error', 'No se pudo conectar con el servidor', 'error');
				return;
			}
			swal('Ocurrió un error',
					xhr.status + ' ' + xhr.statusText + ': ' +
					xhr.responseJSON.data.message,
					'error');
		}
	});
}


function consultarGarantiaPorServicio(idServicio) {
	$.ajax(
	{
		url: HOST + '/api/servicio/' + idServicio,
		type: 'GET',
		success: function(result) {
			$.each(result.data.condicion_garantia, function(index, data) {
				//Por cada valor parámetro asociado al servicio, toma el id y se selecciona
				//el correspondiente valor en el select multiple
				$('#garantiaServicio optgroup option[value="' + data.id + '"]').prop('selected', true);
			});
			$('#garantiaServicio').multiSelect('refresh');
		},
		error: function(xhr) {
			if(xhr.responseJSON == undefined) {
				swal('Ocurrió un error', 'No se pudo conectar con el servidor', 'error');
				return;
			}
			swal('Ocurrió un error',
					xhr.status + ' ' + xhr.statusText + ': ' +
					xhr.responseJSON.data.message,
					'error');
		}
	});
}


function consultarInsumoPorServicio(idServicio) {
	$.ajax(
	{
		url: HOST + '/api/servicio/' + idServicio,
		type: 'GET',
		success: function(result) {
			$('#tablaInsumo tbody').empty();
			$.each(result.data.insumo_servicio, function(index, data) {
				$('#tablaInsumo tbody').append(
            '<tr data-id-insumo="' + data.id_insumo + '" data-id-unidad="' + data.id_unidad + '" data-cantidad="'+ data.cantidad +'">' +
                '<td>' + data.insumo.nombre + '</td>' +
                '<td>' + data.cantidad + '</td>' +
                '<td>' + data.unidad.abreviatura + '</td>' +
                '<td class="centrado"> <button type="button" rel="tooltip"  title="Eliminar" class="btn btn-danger btn-circle waves-effect waves-circle waves-float insumo-tabla" onclick="eliminarInsumo(this)"> <i class="material-icons">delete</i> </button> </td>' +
            '</tr>'

        );
			});
		},
		error: function(xhr) {
			if(xhr.responseJSON == undefined) {
				swal('Ocurrió un error', 'No se pudo conectar con el servidor', 'error');
				return;
			}
			swal('Ocurrió un error',
					xhr.status + ' ' + xhr.statusText + ': ' +
					xhr.responseJSON.data.message,
					'error');
		}
	});
}


function limpiarDiccionario(objeto) {
	var keys = Object.keys(objeto);
	keys.forEach(element => {
		objeto[element] = false;
	});
}

function eliminarInsumo(el){
    var idInsumoPorEliminar = $(el).parent().parent().data('id-insumo');
    valuesInsumoServicioPorEliminar[idInsumoPorEliminar] = idInsumoPorEliminar
    valuesInsumoServicio[idInsumoPorEliminar] = false;
     $(el).parent().parent().remove();

    // Este if evalúa si el tbody de la tabla se quedó sin filas
    if($('#tablaInsumo tbody tr').length === 0) {
        $('#tablaInsumo tbody').html(
            '<tr id="sinValor">' +
                '<td class="centrado" colspan="5">' +
                    '<i>No se han añadido datos a la tabla</i>' +
                '</td>' +
            '</tr>'
        );
    }
}
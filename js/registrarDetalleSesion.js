
let valuesInsumoDetalle = {}; //Diccionario donde se almacenarán las tecnicas del servicio
let valuesInsumoDetallePorEliminar = {} 

let valuesIncidenciaInsumo = {}; //Diccionario donde se almacenarán las tecnicas del servicio
let valuesIncidenciaInsumoPorEliminar = {} 


let valuesDetalleSesion = {}; //Diccionario donde se almacenarán las tecnicas del servicio
let valuesDetalleSesionPorEliminar = {} 

let itemLogros = []
let idTipoServicio = 0;
let idOrdenServicio = 0;

let hay_incidencia = false;
var idServicio;
var codigo;
var contFinalizada = 1;
let sonIguales = false;
var contSuspendida = 0;
var estatus_orden =2;

//Diccionario donde se almacenarán la tecnicas del servicio que serán removidos de la base de datos


$(document).ready(function() {
    console.log(localStorage.verSesion)
    console.log(localStorage.idCita)

    if(localStorage.verSesion == 1){
        console.log('ver sesion')
        consultarCitaSesion(localStorage.idCita);
        $('#insumos').hide();
        $('#divProximaSesion').hide();
        $('#btnGuardarDetalleSesion').hide();


    }else {
        console.log('registrar sesion')
        consultarCita(localStorage.idCita);
        consultarEmpleado()
        consultarUnidad();
        consultarBloqueHora();
        consultarTipoIncidencia(); 
    }
   
         


    $('input[name="sesionCulminada"]').change(function() {
        if($('#sesionCulminadaSi').is(':checked')) {
            $('#divDetalle').show();
            $('#divIncidenciaAgenda').hide();
            hay_incidencia = false;   

            if (sonIguales == true) {
                $('#divProximaSesion').hide();
                estatus_orden =3;
            }

            consultarItemDetalle();

        }
        if($('#sesionCulminadaNo').is(':checked')) {
            $('#divIncidenciaAgenda').show();  
            hay_incidencia = true;   
            estatus_orden =2;
            $('#divProximaSesion').show();
            $('#divDetalle').hide();

        }
    });

    $('#btnBuscarInsumo').click(function() {
        codigo = $('#txtCodigoInsumo').val();
        $('#txtCodigoInsumo').prop('disabled', true);
        consultarInsumoMarcaProveedor(codigo);
    });

    $('#btnCancelarInsumo').click(function() {
        limpiarInsumos();
    });

    $('#btnAgregarInsumo').click(function() {
        var insumo_marca_proveedor = $('#txtCodigoInsumo').val();
        var insumo = $('#txtNombreInsumo');
        var marca = $('#txtMarcaInsumo').val();
        var proveedor = $('#txtProveedorInsumo').val();
        var cantidad = $('#txtCantidad').val();
        var unidad = $('#cmbUnidad option:selected');


        // Voy a añadir un insumo, elimino la fila "No se han añadido insumos"
        $('#sinInsumos').remove();

        // Añade un insumo a la tabla

        $('#tablaInsumo tbody').append(
            '<tr data-id-insumo_marca_proveedor="' + insumo_marca_proveedor + '" data-id-unidad="'+ unidad.val() + '" data-cantidad="'+ cantidad +'" data-insumo-id="'+ insumo.attr('data-id-insumo')+ '">' +
                '<td>' + insumo.val() + '</td>' +
                '<td>' + marca + '</td>' +
                '<td>' + proveedor + '</td>' +
                '<td>' + cantidad + ' ' + unidad.text() +'</td>' +
                '<td class="acciones">' +
                    /*'<button type="button" rel="tooltip" data-toggle="modal" ' +
                            'data-target="#modalIncidenciaInsumo" ' +
                            'data-placement="top" ' +
                            'title="Detalle de incidencia de insumo"' +
                            'class="btn btn-warning btn-circle waves-effect ' +
                                    'waves-circle waves-float" onclick="seleccionarInsumo(this)""> ' +
                        '<i class="material-icons">warning</i>' +
                    '</button>' +
                    '&nbsp;' +*/
                    '<button type="button" rel="tooltip" data-placement="top" ' +
                            'title="Eliminar"' +
                            'class="btn btn-danger btn-circle waves-effect ' +
                                    'waves-circle waves-float" onclick="eliminarInsumo(this)""> ' +
                        '<i class="material-icons">delete</i>' +
                    '</button>' +
                '</td>' +
            '</tr>'
        );
         limpiarInsumos();
         // 
            var CodigoPorAnadir = $('#tablaInsumo tbody tr:last-child').data('id-insumo_marca_proveedor');
            var idUnidadPorAnadir = $('#tablaInsumo tbody tr:last-child').data('id-unidad');
            var cantidadPorAnadir = $('#tablaInsumo tbody tr:last-child').data('cantidad');

            valuesInsumoDetalle[CodigoPorAnadir] = {
                id_insumo_marca_proveedor: CodigoPorAnadir,
                id_unidad: idUnidadPorAnadir,
                cantidad: cantidadPorAnadir
            }
            valuesInsumoDetallePorEliminar[CodigoPorAnadir] = false;
    });

     $('#chxTecnica').focus({
        checked: true, //Permite seleccionar un grupo de valores parámetro completo
        afterSelect: function (values) {
            //Iteramos por cada elemento seleccionado (uno o más a la vez)
            values.forEach(element => {
                valuesTecnicaDetalle[element] = element; //Si se selecciona valor parámetro "1", se setea el campo clave "1" con el valor en cuestión
                valuesTecnicaDetallePorEliminar[element] = false; //En caso de que se haya removido y se selecciona de nuevo, debe setearse en false
            });
            console.log(valuesTecnicaDetalle);
        },
        afterDeselect: function (values) {
            //Iteramos por cada elemento seleccionado (uno o más a la vez)
            values.forEach(element => {
                valuesTecnicaDetalle[element] = false; //Si se deselecciona valor parámetro "1", se setea el campo clave "1" con el valor false
                valuesTecnicaDetalle[element] = element;
            });
            console.log(valuesTecnicaDetalle);
            console.log('Se eliminarán:', valuesTecnicaDetalle);
        }
    });
   


    // Evento click del botón para guardar el detalle de la sesión
    $('#btnGuardarDetalleSesion').click(function() {
        var form = document.getElementById("formDetalleSesion");
        var postData = new FormData(form);

        var tecnicaDetalle = [];
        var chxTecnicaDetalle = $('.tecnica:checked');
        $.each(chxTecnicaDetalle ,function(index,data){
            tecnicaDetalle.push(data.dataset.idTecnica)
        });
        console.log('tecnica: ',tecnicaDetalle);

       

        var radioIncidenciaDetalle = $('.incidencia:checked');
        var incidenciaDetalleRadio = radioIncidenciaDetalle[0];
        var incidenciaDetalle;
        if (incidenciaDetalleRadio != undefined) {
            incidenciaDetalle = { 
                id_clasificacion_incidencia:    1,
                id_tipo_incidencia:             radioIncidenciaDetalle[0].dataset.idIncidencia,
                contenido:                      $('#txtDescripcion').val()
            };
        }
        
        
        console.log('incidencia: ',incidenciaDetalle);


        var itemLogroDetalle = [];
        var itemDetalleSesion = [];
        var chxLogroDetalle = $('.item_logro:checked');
        $.each(chxLogroDetalle ,function(index,data){
            itemLogroDetalle.push({
                id_item_logro:   data.dataset.idItemLogro,
                id_item_detalle: data.dataset.idItemDetalle
            });
          
        });
        console.log('Logros: ',itemLogroDetalle);


        // insumo servicio
        var insumoServicio = Object.values(valuesInsumoDetalle);
        var valoresInsumo = [];
        insumoServicio.forEach(valor => {
            if(valor) {
                valoresInsumo.push(valor);
            }
        });
        console.log('insumo_marca_proveedor =',valoresInsumo);

         // incidencia insumo
        var insumoIncidencia = Object.values(valuesIncidenciaInsumo);
        var valoresInsumoIncidencia = [];
        insumoIncidencia.forEach(valor => {
            if(valor) {
                valoresInsumoIncidencia.push(valor);
            }
        });
        console.log('Incidencia insumo: ',insumoIncidencia);


        postData.append('insumo', JSON.stringify(valoresInsumo));
        postData.append('tecnica', JSON.stringify(tecnicaDetalle));
        postData.append('item_logro', JSON.stringify(itemLogroDetalle));
        postData.append('id_orden_servicio', idOrdenServicio);
        var fecha = $('#dtpFechaProximaSesion').val();
        fecha += ' ' + $('#cmbBloqueHora option:selected').data('fecha');
        postData.append('fecha', fecha);
        postData.append('estatus', 2);
        //postData.append('estatus_orden', estatus_orden);
        postData.append('incidencia',  JSON.stringify(incidenciaDetalle));
        postData.append('hay_incidencia', hay_incidencia);
       

        $.ajax(
                {

                url: HOST + '/api/sesion',
                type: 'POST',
                dataType: 'json',
                data: postData,
                processData: false,
                contentType: false,
                success: function(data) {
                    localStorage.idNewSesion = data.data.id;
                    console.log(localStorage.idNewSesion);
                    swal({
                    title: "¡Cambios guardados exitosamente!",
                    type: "success",
                    text: "En breve será redirigido a la página siguente ",
                    timer: 2000,
                    showConfirmButton: false
                }, function () {                
                   var redireccion;
                    if (hay_incidencia == true){
                        redireccion =  window.location.replace('DetalleSesion.html');
                    } else {
                        redireccion = window.location.replace('avanceCliente.html');
                    }
                });
                },
                error: function(xhr) {
                    if(xhr.responseJSON == undefined) {
                        swal('Ocurrió un error', 'No se pudo conectar con el servidor', 'error');
                        return;
                    }
                    swal('Ocurrió un error',
                            xhr.status + ' ' + xhr.statusText + ': ' +
                            xhr.responseJSON.data.message,
                            'error');
                }
            }
        );

     
    });
});

 
                    


function eliminarInsumo(el){
    var idInsumoPorEliminar = $(el).parent().parent().data('id-insumo-marca-proveedor');
    valuesInsumoDetallePorEliminar[idInsumoPorEliminar] = idInsumoPorEliminar
    valuesInsumoDetalle[idInsumoPorEliminar] = false;
     $(el).parent().parent().remove();

    // Este if evalúa si el tbody de la tabla se quedó sin filas
    if($('#tablaInsumo tbody tr').length === 0) {
        $('#tablaInsumo tbody').html(
            '<tr id="sinValor">' +
                '<td class="centrado" colspan="5">' +
                    '<i>No se han añadido datos a la tabla</i>' +
                '</td>' +
            '</tr>'
        );
    }
}

function seleccionarInsumo(el) {
    var id = $(el).parent().parent().attr('id-insumo-marca-proveedor');
    var fila = $(el).parent().parent().children();
    var insumo = $(fila).eq(0).text();
    var marca = $(fila).eq(1).text();
    var proveedor= $(fila).eq(2).text();
    $('#spanInsumoIncidencia').text(insumo);
    $('#spanMarcaIncidencia').text(marca);
    $('#spanProveedorIncidencia').text(proveedor);

    $('#btnGuardarIncidenciaInsumo').click(function() {
        console.log('jelous')
        var clasificacionIncidencia = 2;
        var idTipoIncidencia = $('#cmbTipoIncidencia').val()
        var contenido =  $('#txtDescripcionInsumo').val()

            valuesIncidenciaInsumo[idTipoIncidencia] = {
                id_incidencia_clasificacion: clasificacionIncidencia ,
                id_tipo_incidencia: idTipoIncidencia,
                contenido: contenido
            }
            valuesIncidenciaInsumoPorEliminar[idTipoIncidencia] = false;
            console.log(valuesIncidenciaInsumo);
       // $('#modalIncidenciaInsumo').modal('toggle');
        hay_incidencia = true

    });
}   

function consultarItemLogro(idDiv) {
    $.ajax(
    {
        url: HOST + '/api/item_logro',
        type: 'GET',
        success: function(result) {
            $('#divItemLogro').empty();
            $.each(result.data, function(index, data) {
                $('#divLogro' + idDiv).append(
                    
                       '<input type="radio" class="item_logro filled-in with-gap radio-col-deep-orange" data-id-item-detalle="' + idDiv + '" data-id-item-logro="'+ data.id +'" id="logro'+ data.id + idDiv +'" name="' + idDiv + '" />' +
                       '<label for="logro'+ data.id + idDiv + '">' + data.descripcion + '</label>'
                    
                );
            });
        },
        error: function(xhr) {
            if(xhr.responseJSON == undefined) {
                swal('Ocurrió un error', 'No se pudo conectar con el servidor', 'error');
                return;
            }
            swal('Ocurrió un error',
                    xhr.status + ' ' + xhr.statusText + ': ' +
                    xhr.responseJSON.data.message,
                    'error');
        }
    });
}



function consultarItemDetalle() {
    $.ajax(
    {
        url: HOST + '/api/item_detalle',
        type: 'GET',
        success: function(result) {
            $('#divItemDetalle').empty();
            $.each(result.data, function(index, data) {
                $('#divItemDetalle').append(
                    '<div class="col-sm-6">' +
                           '<label data-id-item-detalle="'+ data.id +'">' + data.descripcion + '</label>'+ 

                    '</div>' +
                    '<div class="col-sm-6" id="divLogro' + data.id + '">' +
                    '</div>'
                );
                consultarItemLogro(data.id);
                //$('#divLogro' + data.id + ' input').attr('name', 'logro' + data.id);
            });
        },
        error: function(xhr) {
            if(xhr.responseJSON == undefined) {
                swal('Ocurrió un error', 'No se pudo conectar con el servidor', 'error');
                return;
            }
            swal('Ocurrió un error',
                    xhr.status + ' ' + xhr.statusText + ': ' +
                    xhr.responseJSON.data.message,
                    'error');
        }
    });
}


function consultarTipoIncidencia() {
    $.ajax(
    {
        url: HOST + '/api/tipo_incidencia',
        type: 'GET',
        success: function(result) {
            $('#divTipoIncidencia').empty();
            
            $.each(result.data, function(index, data) {
                $('#divTipoIncidencia ').append(
                    '<div class="col-sm-3">' +
                        '<input type="radio" name="tipoIncidencia" data-id-incidencia="'+ data.id +'" id="tipoIncidencia'+ data.id +'" class="incidencia with-gap radio-col-deep-orange" required />' +
                        '<label for="tipoIncidencia'+ data.id +'">' + data.descripcion + '</label>' +
                    '</div>'
                );
            });

             $('#cmbTipoIncidencia').html('<option value="0" selected disabled hidden>Tipo incidencia</option>');

            $.each(result.data, function(index, data) {
                $('#cmbTipoIncidencia').append('<option value="' + data.id + '">' + data.descripcion  + '</option>');
            });
            $('#cmbTipoIncidencia').selectpicker('refresh');
        },
        error: function(xhr) {
            if(xhr.responseJSON == undefined) {
                swal('Ocurrió un error', 'No se pudo conectar con el servidor', 'error');
                return;
            }
            swal('Ocurrió un error',
                    xhr.status + ' ' + xhr.statusText + ': ' +
                    xhr.responseJSON.data.message,
                    'error');
        }
    });
}
function consultarCitas(idOrden) {
     $.ajax(
        {
            url: HOST + '/api/cita',
            async:false,
            type: 'GET',
            success: function(result) {
                var cont = 0;
                $.each(result.data, function(index, data) {
                    if (data.orden_servicio.id==idOrden) {
                        if(data.estatus == 2){
                            contFinalizada++; 

                        } else if(data.estatus == 3){
                            contSuspendida++;;
                        }
                        
                        if(contFinalizada==1) {
                            $('#txtNroSesion').val(1);
                        }else{
                             $('#txtNroSesion').val(contFinalizada+1);
                        }

                        $('#spanCantidadSesionesRealizadas').text(contFinalizada);
                        $('#spanCantidadSesionesCanceladas').text(contSuspendida);
                    }
                });
            },  
            error: function(xhr) {
                swal('Ocurrió un error',
                        xhr.status + ' ' + xhr.statusText + ': ' +
                        xhr.responseJSON.data.message,
                        'error');
            }
        }
    );
}

function consultarCita(idCita) {
    $.ajax(
        {
            url: HOST + '/api/cita/'+ idCita,
            type: 'GET',
            success: function(result) {
                   var cita = result.data;
                    $('#txtIdCita').val(cita.id);
                    
                    $('#spanCliente').text(cita.orden_servicio.solicitud.cliente.nombres+' '+cita.orden_servicio.solicitud.cliente.apellidos);
                    $('#spanServicio').text(cita.orden_servicio.solicitud.servicio.nombre);
                    idOrdenServicio = cita.orden_servicio.id;
                    idTipoServicio = cita.orden_servicio.solicitud.servicio.id_tipo_servicio;
                    ((cita.orden_servicio.solicitud.id_promocion == null) ?  $('#spanPromocion').text('Ninguna') :  $('#spanPromocion').text(cita.orden_servicio.solicitud.promocion.nombre));
                    $('#spanTipoOrden').text(cita.orden_servicio.solicitud.tipo_solicitud.descripcion);
                    $('#spanTiempoPorSesion').text(cita.orden_servicio.solicitud.servicio.tiempo_por_sesion + ' bloque(s) de 30 min' );
                    $('#spanFecha').text(formatFechaHora(cita.fecha));
                    $('#spanEmpleadoAsignado').text(cita.agenda.horario_empleado.empleado.nombres+' '+cita.agenda.horario_empleado.empleado.apellidos);
                    $('#spanCantidadSesiones').text(cita.orden_servicio.solicitud.servicio.cantidad_sesiones);
                   

                    idServicio = cita.orden_servicio.solicitud.servicio.id;
                    consultarTecnicaPorServicio(idServicio);
                    consultarCitas(cita.orden_servicio.id);

                    if (contFinalizada == cita.orden_servicio.solicitud.servicio.cantidad_sesiones)
                    {
                        $('#divProximaSesion').hide();
                        sonIguales = true;
                    }

            },  
            error: function(xhr) {
                swal('Ocurrió un error',
                        xhr.status + ' ' + xhr.statusText + ': ' +
                        xhr.responseJSON.data.message,
                        'error');
            }
        }
    );
}


function consultarCitaSesion(idCita) {
    $.ajax(
        {
            url: HOST + '/api/cita/'+ idCita,
            type: 'GET',
            success: function(result) {
                   var cita = result.data;
                    $('#txtIdCita').val(cita.id);
                    
                    $('#spanCliente').text(cita.orden_servicio.solicitud.cliente.nombres+' '+cita.orden_servicio.solicitud.cliente.apellidos);
                    $('#spanServicio').text(cita.orden_servicio.solicitud.servicio.nombre);
                    idOrdenServicio = cita.orden_servicio.id;
                    idTipoServicio = cita.orden_servicio.solicitud.servicio.id_tipo_servicio;
                    ((cita.orden_servicio.solicitud.id_promocion == null) ?  $('#spanPromocion').text('Ninguna') :  $('#spanPromocion').text(cita.orden_servicio.solicitud.promocion.nombre));
                    $('#spanTipoOrden').text(cita.orden_servicio.solicitud.tipo_solicitud.descripcion);
                    $('#spanTiempoPorSesion').text(cita.orden_servicio.solicitud.servicio.tiempo_por_sesion + ' bloque(s) de 30 min' );
                    $('#spanFecha').text(formatFechaHora(cita.fecha));
                    $('#spanEmpleadoAsignado').text(cita.agenda.horario_empleado.empleado.nombres+' '+cita.agenda.horario_empleado.empleado.apellidos);
                    $('#spanCantidadSesiones').text(cita.orden_servicio.solicitud.servicio.cantidad_sesiones);
                   
                    
                   // console.log(idCitaSesion);
                    idServicio = cita.orden_servicio.solicitud.servicio.id;
                    consultarTecnicaPorServicio(idServicio);
                    consultarCitas(cita.orden_servicio.id);
                    console.log('sesion', cita.sesion.id);
                    consultarSesion(cita.sesion.id); 
                    if (contFinalizada == cita.orden_servicio.solicitud.servicio.cantidad_sesiones)
                    {
                        $('#divProximaSesion').hide();
                        sonIguales = true;
                    }

            },  
            error: function(xhr) {
                swal('Ocurrió un error',
                        xhr.status + ' ' + xhr.statusText + ': ' +
                        xhr.responseJSON.data.message,
                        'error');
            }
        }
    );
}

function consultarSesion(idSesion) {
    $.ajax(
        {
            url: HOST + '/api/sesion/'+ idSesion,
            type: 'GET',
            success: function(result) {
                $('#spanCantidadSesionesRealizadas').text(result.data.numero_sesion);
                console.log(result.data.numero_sesion)
                $('#divTecnica').empty();
                $.each(result.data.tecnica, function(index, data) {
                    
                    $('#divTecnica').append(
                        ' <div class="col-sm-3" class="divTecnica">' + 
                            '<input disabled checked type="checkbox" class="tecnica filled-in chk-col-deep-orange" data-id-tecnica="'+ data.id +'" id="'+ data.id +'" />' +
                            '<label for="'+ data.id +'">' + data.nombre + '</label>'+
                        '</div>'
                    );
                });
            $('#tablaInsumo tbody').empty();
            $.each(result.data.insumo_utilizado, function(index, data1) {
                $('#tablaInsumo tbody').append(
                '<tr>' +
                    '<td>' + data1.insumo_marca_proveedor.insumo.nombre + '</td>' +
                    '<td>' + data1.insumo_marca_proveedor.marca.nombre + '</td>' +
                    '<td>' + data1.insumo_marca_proveedor.proveedor.nombre + '</td>' +
                    '<td>' + data1.cantidad +'</td>' +
                    '<td class="acciones">' +
                        
                    '</td>' +
                '</tr>'
                );
            });

            if(result.data.incidencia.length !=0){
                console.log('no hay incidencia')
                $('#sesionCulminadaSi').attr('checked',true)
                $('#sesionCulminadaSi').attr('disabled',true)
                $('#sesionCulminadaNo').attr('disabled',true)
            } else {
                console.log('hay incidencia')
            }
            },  
            error: function(xhr) {
                swal('Ocurrió un error',
                        xhr.status + ' ' + xhr.statusText + ': ' +
                        xhr.responseJSON.data.message,
                        'error');
            }
        }
    );
}


function consultarUnidad() {
    $.ajax(
    {
        url: HOST + '/api/unidad',
        type: 'GET',
        success: function(result) {
            $('#cmbUnidad').html('<option value="0" selected disabled hidden> - </option>');

            $.each(result.data, function(index, data) {
                $('#cmbUnidad').append( ((data.estatus == 1) ? '<option value="' + data.id + '">' + data.abreviatura  + '</option>': 'NO TIENE'));
            });
            $('#cmbUnidad').selectpicker('refresh');
        },
        error: function(xhr) {
            if(xhr.responseJSON == undefined) {
                swal('Ocurrió un error', 'No se pudo conectar con el servidor', 'error');
                return;
            }
            swal('Ocurrió un error',
                    xhr.status + ' ' + xhr.statusText + ': ' +
                    xhr.responseJSON.data.message,
                    'error');
        }

    });
}

function consultarTecnicaPorServicio(idServicio) {
    $.ajax(
    {
        url: HOST + '/api/servicio/' + idServicio,
        type: 'GET',
        success: function(result) {
            $('#divTecnica').empty();
            $.each(result.data.tecnica, function(index, data) {
                $('#divTecnica').append(
                    ' <div class="col-sm-3" class="divTecnica">' + 
                        '<input type="checkbox" class="tecnica filled-in chk-col-deep-orange" data-id-tecnica="'+ data.id +'" id="'+ data.id +'" />' +
                        '<label for="'+ data.id +'">' + data.nombre + '</label>'+
                    '</div>'
                );
            });
        },
        error: function(xhr) {
            if(xhr.responseJSON == undefined) {
                swal('Ocurrió un error', 'No se pudo conectar con el servidor', 'error');
                return;
            }
            swal('Ocurrió un error',
                    xhr.status + ' ' + xhr.statusText + ': ' +
                    xhr.responseJSON.data.message,
                    'error');
        }
    });
}




function consultarInsumoMarcaProveedor(codigo) {
    $.ajax(
    {
        url: HOST + '/api/insumo_marca_proveedor/' + codigo,
        type: 'GET',
        success: function(result) {
            $('#txtNombreInsumo').val(result.data.insumo.nombre);
            $('#txtNombreInsumo').attr('data-id-insumo',result.data.insumo.id);
            $('#txtMarcaInsumo').val(result.data.marca.nombre);
            $('#txtProveedorInsumo').val(result.data.proveedor.nombre);
        },
        error: function(xhr) {
            if(xhr.responseJSON == undefined) {
                swal('Ocurrió un error', 'No se pudo conectar con el servidor', 'error');
                return;
            }
            swal('El Insumo no existe', xhr.responseJSON.data.message,'error');
            limpiarInsumos()
        }
    });
}


function limpiarInsumos(){
    $('#txtNombreInsumo').val('');
    $('#txtMarcaInsumo').val('');
    $('#txtProveedorInsumo').val('');
    $('#txtCantidad').val('');
    $('#txtCodigoInsumo').val('');
    $('#txtCodigoInsumo').prop('disabled', false);
    $('#cmbUnidad').selectpicker('val', 0);
    $('#cmbUnidad').selectpicker('val', 0);
}

function formatHora24(time12) {

    var time = time12.split(' ');
    var hora = time[0].split(':'); //Separa hora, minuto y segundo
    var h = parseInt(hora[0]); //Convierte hora a entero en formato 24h
    var min = hora[1];

    var a = time[1];

    if(h == 12 && a == 'AM') {
        h = 0;
    } else if(a == 'PM' && h != 12) {
        h += 12;
    }
    
    return ((h < 10) ? '0' : '') + h + ':' + min;
}

function formatHora(time) {
    //Ejemplo: '04:34:00'
    var hora = time.split(':'); //Separa hora, minuto y segundo
    var h = parseInt(hora[0]); //Convierte hora a entero en formato 24h
    var min = parseInt(hora[1]);
    var a = (h < 12) ? 'AM' : 'PM'; //Si h es menor a 12 es AM, si no, PM
    if(h == 0) {
        h = 12;
    } else if(h > 12) {
        h = (h % 12 < 10) ? '0' + (h % 12) : (h % 12);
    }
    min = (min < 10) ? '0' + min : min;
    return h + ':' + min + ' ' + a;
}

function formatFecha(date) {
    //Ejemplo: '2018-04-27T04:34:03.687Z'
    var fechaHoraStr = String(date).split('T'); //Separa la fecha de la hora
    var fecha = fechaHoraStr[0].split('-'); //Separa año, mes y día

    //Concatena cada valor de fecha en formato dd/mm/yyyy
    return fecha[2] + '/' + fecha[1] + '/' + fecha[0];
}

function formatFechaHora(date) {
    //Ejemplo: '2018-04-27T04:34:03.687Z'
    var fechaHoraStr = String(date).split('T'); //Separa la fecha de la hora
    var fecha = fechaHoraStr[0].split('-'); //Separa año, mes y día

    //Concatena cada valor de fecha en formato dd/mm/yyyy
    var f = fecha[2] + '/' + fecha[1] + '/' + fecha[0];

    var hora = fechaHoraStr[1].split(':'); //Separa hora, minuto y segundo
    var h = parseInt(hora[0]); //Convierte hora a entero en formato 24h
    var min = parseInt(hora[1]);
    var a = (h < 12) ? 'AM' : 'PM'; //Si h es menor a 12 es AM, si no, PM
    if(h == 0) {
        h = 12;
    } else if(h > 12) {
        h = (h % 12 < 10) ? '0' + (h % 12) : (h % 12);
    }
    min = (min < 10) ? '0' + min : min;
    return f + ' ' + h + ':' + min + ' ' + a;
}




function consultarEmpleado() {
    $.ajax(
    {
        url: HOST + '/api/empleado',
        type: 'GET',
        success: function(result) {

            $('#cmbEmpleado').html('<option value="0" selected disabled hidden> Empleado </option>');

            $.each(result.data, function(index, data) {
                if(data.estatus == 1 && data.tipo_empleado == 2 && data.tipo_servicio.length > 0) {
                    $.each(data.tipo_servicio, function(index, data2) {
                        if(data2.id == idTipoServicio) {
                            $('#cmbEmpleado').append('<option value="' + data.id + '">' + data.nombres + ' ' + data.apellidos + '</option>');
                            return false;
                        }
                    });
                }
            });
            $('#cmbEmpleado').selectpicker('refresh');
        },
        error: function(xhr) {
            if(xhr.responseJSON == undefined) {
                swal('Ocurrió un error', 'No se pudo conectar con el servidor', 'error');
                return;
            }
            swal('Ocurrió un error',
                    xhr.status + ' ' + xhr.statusText + ': ' +
                    xhr.responseJSON.data.message,
                    'error');
        }
    });
}


function consultarBloqueHora() {
    $.ajax(
    {
        url: HOST + '/api/bloque_hora',
        type: 'GET',
        success: function(result) {

            $('#cmbBloqueHora').html('<option value="0" selected disabled hidden> Bloque hora </option>');

            $.each(result.data, function(index, data) {
                $('#cmbBloqueHora').append('<option value="' + data.id + '" data-fecha="' + data.hora_inicio + '">' + formatHora(data.hora_inicio) + '</option>');
            });
            $('#cmbBloqueHora').selectpicker('refresh');
        },
        error: function(xhr) {
            if(xhr.responseJSON == undefined) {
                swal('Ocurrió un error', 'No se pudo conectar con el servidor', 'error');
                return;
            }
            swal('Ocurrió un error',
                    xhr.status + ' ' + xhr.statusText + ': ' +
                    xhr.responseJSON.data.message,
                    'error');
        }
    });
}


let valuesPermisos = {}; //Diccionario donde se almacenarán los permisos de un rol
let valuesPermisosPorEliminar = {} //Diccionario donde se almacenarán los permisos de un rol que serán removidos de la base de datos

$(document).ready(function() {

    $('.sin-margen').css('margin-bottom', '0');
    $('#opcionesMenu').multiSelect({
		selectableOptgroup: true, //Permite seleccionar un grupo de permisos completo
		afterSelect: function (values) {
			//Iteramos por cada elemento seleccionado (uno o más a la vez)
			values.forEach(element => {
				valuesPermisos[element] = element; //Si se selecciona permiso "1", se setea el campo clave "1" con el valor en cuestión
				valuesPermisosPorEliminar[element] = false; //En caso de que se haya removido y se selecciona de nuevo, debe setearse en false
			});
			console.log(valuesPermisos);
		},
		afterDeselect: function (values) {
			//Iteramos por cada elemento seleccionado (uno o más a la vez)
			values.forEach(element => {
				valuesPermisos[element] = false; //Si se deselecciona permiso "1", se setea el campo clave "1" con el valor false
				valuesPermisosPorEliminar[element] = element;
			});
			console.log(valuesPermisos);
			console.log('Se eliminarán:', valuesPermisosPorEliminar);
		}
	});

    limpiarCampos();
    consultarRoles();

	$('#btnGuardar').click(function() {
		var form = document.getElementById("formPermisos");
        var putData = new FormData(form);
        var idRol = $('#txtIdRol').val();

        var valoresPermisos = Object.values(valuesPermisos);
		var permisos = [];
		valoresPermisos.forEach(valor => {
			if(valor) {
				permisos.push(valor);
			}
		});
		console.log('permisos =', permisos);
		var valoresPermisosPorEliminar = Object.values(valuesPermisosPorEliminar);
		var permisosPorEliminar = [];
		valoresPermisosPorEliminar.forEach(valor => {
			if(valor) {
				permisosPorEliminar.push(valor);
			}
		});
		console.log('permisos por eliminar =', permisosPorEliminar);

		putData.append('menu_opcion', permisos);
		putData.append('menu_opcion_eliminar', permisosPorEliminar);

		$.ajax(
			{
				url: HOST + '/api/rol/' + idRol,
				type: 'PUT',
				dataType: 'json',
				data: putData,
				processData: false,
				contentType: false,
				success: function(result) {
					limpiarCampos();
					var usuario = JSON.parse(localStorage.usuario);
					$.each(result.data.usuario, function(index, data) {
						if(data.id == usuario.id) {
							localStorage.usuario = JSON.stringify(data);
						}
					});
					swal('Asignación de permisos exitosa', '', 'success');
				},
				error: function(xhr) {
					swal('Ocurrió un error',
							xhr.status + ' ' + xhr.statusText + ': ' +
							xhr.responseJSON.data.message,
							'error');
				}
			}
		);
	});

	$('#btnCancelar').click(function() {
		limpiarCampos();
	});

});

function seleccionarFila(el) {
    var idRol = $(el).data('id-rol');
    $('#txtIdRol').val(idRol);
    $('#spanRol').text($(el).children().eq(0).text());
	$('.ms option:selected').removeAttr('selected'); //Remueve las selecciones de todos los select multiple
	$('.ms').multiSelect('refresh'); //Actualiza los select multiple

	//Setea todos los valores actuales de los diccionarios en false
	limpiarDiccionario(valuesPermisos);
	limpiarDiccionario(valuesPermisosPorEliminar);
    //FIN limpiar diccionarios
    
    consultarMenus();
	consultarPermisosPorRol(idRol);
	$('#btnGuardar, #btnCancelar').show();
}

function consultarRoles() {
	$.ajax(
		{
			url: HOST + '/api/rol',
			type: 'GET',
			success: function(result) {
				//Destruir la tabla para luego reinicializarla más adelante
                if($.fn.DataTable.isDataTable("#tablaRoles")) {
                    $('#tablaRoles').DataTable().clear().destroy();
                }
                //$('#tablaRoles tbody').empty();
				$.each(result.data, function(index, data) {
                    if(data.estatus == 1) {
                        $('#tablaRoles tbody').append(
                            '<tr data-id-rol="' + data.id + '" onclick="seleccionarFila(this)">' +
                                '<td>' + data.nombre + '</td>' +
                                '<td>' + data.descripcion + '</td>' +
                            '</tr>'
                        );
                    } 
				});
				//Aquí se reinicializa la tabla
                $(".js-basic-example").dataTable({
                    responsive: true
                });
                $('#tablaRoles tbody tr').css('cursor', 'pointer');
			},
			error: function(xhr) {
				swal('Ocurrió un error',
						xhr.status + ' ' + xhr.statusText + ': ' +
						xhr.responseJSON.data.message,
						'error');
			}
		}
	);
}

function consultarMenus() {
	$.ajax(
		{
			url: HOST + '/api/menu_opcion',
            type: 'GET',
            async: false,
			success: function(result) {
                $('#opcionesMenu').empty();
                var menus_padres = {};
				$.each(result.data, function(index, data) {
                    if(data.url != undefined) {
                        var id_padre = data.id_menu_opcion;
                        var nombre_padre = 'Raíz';
                        if(id_padre != undefined) {
                            nombre_padre = data.menu_opcion_padre.funcion;
						}
						if(!data.es_menu) {
							nombre_padre = 'Permisos fuera del menú';
						}
						menus_padres[nombre_padre] = nombre_padre;
                        $('#opcionesMenu').append(
                            '<option data-nombre-padre="' + nombre_padre + '" value="' + data.id + '">' +
                                data.funcion +
                            '</option>'
                        );
                    } 
                });
                var padres = Object.values(menus_padres);
                $.each(padres, function(index, data) {
                    $('option[data-nombre-padre="' + data + '"]').wrapAll('<optgroup label="' + data + '"></optgroup>');
                });
                $('#opcionesMenu').multiSelect('refresh');
			},
			error: function(xhr) {
				swal('Ocurrió un error',
						xhr.status + ' ' + xhr.statusText + ': ' +
						xhr.responseJSON.data.message,
						'error');
			}
		}
	);
}

function consultarPermisosPorRol(idRol) {
    $.ajax(
        {
            url: HOST + '/api/rol/' + idRol,
            type: 'GET',
            success: function(result) {
                $.each(result.data.menu_opcion, function(index, data) {
                    //Por cada valor parámetro asociado al servicio, toma el id y se selecciona
                    //el correspondiente valor en el select multiple
                    $('#opcionesMenu option[value="' + data.id + '"]').prop('selected', true);
                });
                $('#opcionesMenu').multiSelect('refresh');
            },
            error: function(xhr) {
                if(xhr.responseJSON == undefined) {
                    swal('Ocurrió un error', 'No se pudo conectar con el servidor', 'error');
                    return;
                }
                swal('Ocurrió un error',
                        xhr.status + ' ' + xhr.statusText + ': ' +
                        xhr.responseJSON.data.message,
                        'error');
            }
        }
    );
}

function limpiarCampos() {
	$('#txtIdRol').val("")
	$('#spanRol').text("");
    $('#opcionesMenu').empty();
    $('#opcionesMenu').multiSelect('refresh');
    $('#btnGuardar, #btnCancelar').hide();
    //Setea todos los valores actuales de los diccionarios en false
	limpiarDiccionario(valuesPermisos);
	limpiarDiccionario(valuesPermisosPorEliminar);
}

function limpiarDiccionario(objeto) {
	var keys = Object.keys(objeto);
	keys.forEach(element => {
		objeto[element] = false;
	});
}


$(document).ready(function() {

    $('#cmbParametros').change(function() {
        $('#txtDescripcion').prop('disabled', false);
        consultarTablaParametro($(this).val());
        limpiarCampos();
    });

	$('#btnRegistrar').click(function() {
		var form = document.getElementById("formParametros");
        var postData = new FormData(form);
        var tablaParametro = $('#cmbParametros').val();
		$.ajax(
			{
				url: HOST + '/api/' + tablaParametro,
				type: 'POST',
				dataType: 'json',
				data: postData,
				processData: false,
				contentType: false,
				success: function(data) {
					swal('Registro exitoso', '', 'success');
                    limpiarCampos();
                    consultarTablaParametro($('#cmbParametros').val());
				},
				error: function(xhr) {
					swal('Ocurrió un error',
							xhr.status + ' ' + xhr.statusText + ': ' +
							xhr.responseJSON.data.message,
							'error');
				}
			}
		);
	});

	$('#btnActualizar').click(function() {
		var form = document.getElementById("formParametros");
		var putData = new FormData(form);
        var id = $('#txtId').val();
        var tablaParametro = $('#cmbParametros').val();
		$.ajax(
			{
				url: HOST + '/api/' + tablaParametro + '/' + id,
				type: 'PUT',
				dataType: 'json',
				data: putData,
				processData: false,
				contentType: false,
				success: function(data) {
					swal('Modificación exitosa', '', 'success');
                    limpiarCampos();
                    consultarTablaParametro($('#cmbParametros').val());
				},
				error: function(xhr) {
					swal('Ocurrió un error',
							xhr.status + ' ' + xhr.statusText + ': ' +
							xhr.responseJSON.data.message,
							'error');
				}
			}
		);
	});

	$('#btnCancelar').click(function() {
		limpiarCampos();
	});

});

function seleccionarFila(el) {
	$('#txtId').val($(el).attr('id'));
	$('#txtDescripcion').val($(el).children('td').eq(1).text());
	$('#btnRegistrar').hide();
	$('#btnActualizar').show();
}

function consultarTablaParametro(tablaParametro) {
	$.ajax(
		{
			url: HOST + '/api/' + tablaParametro,
			type: 'GET',
			success: function(result) {
				$('#tituloTabla').html($('#cmbParametros option:selected').text());
				//Destruir la tabla para luego reinicializarla más adelante
                if($.fn.DataTable.isDataTable("#tablaParametros")) {
                    $('#tablaParametros').DataTable().clear().destroy();
                }
				//$('#tablaParametros tbody').empty();
				$.each(result.data, function(index, data) {
					$('#tablaParametros tbody').append(
                        '<tr id="' + data.id + '" onclick="seleccionarFila(this)">' +
                            '<td>' + data.id + '</td>' +
							'<td>' + data.descripcion + '</td>' +
							'<td>' + formatFechaHora(data.fecha_creacion) + '</td>' +
						'</tr>'
					);
				});
				$('.js-exportable').DataTable({
                    dom: 'Bfrtip',
                    responsive: true,
                    buttons: [
                        'copy', 'csv', 'excel', 'pdf', 'print'
                    ]
                });
				$('#tablaParametros tbody tr').css('cursor', 'pointer');
			},
			error: function(xhr) {
				swal('Ocurrió un error',
						xhr.status + ' ' + xhr.statusText + ': ' +
						xhr.responseJSON.data.message,
						'error');
			}
		}
	);
}

function limpiarCampos() {
    $('#txtId').val("");
	$('#txtDescripcion').val("");
	$('#btnRegistrar').show();
    $('#btnActualizar').hide();
    $('#btnCancelar').show();
}

function formatFechaHora(date) {
    //Ejemplo: '2018-04-27T04:34:03.687Z'
    var fechaHoraStr = String(date).split('T'); //Separa la fecha de la hora
    var fecha = fechaHoraStr[0].split('-'); //Separa año, mes y día

    //Concatena cada valor de fecha en formato dd/mm/yyyy
    var f = fecha[2] + '/' + fecha[1] + '/' + fecha[0];

    var hora = fechaHoraStr[1].split(':'); //Separa hora, minuto y segundo
    var h = parseInt(hora[0]); //Convierte hora a entero en formato 24h
    var min = parseInt(hora[1]);
    var a = (h < 12) ? 'AM' : 'PM'; //Si h es menor a 12 es AM, si no, PM
    if(h == 0) {
        h = 12;
    } else if(h > 12) {
        h = (h % 12 < 10) ? '0' + (h % 12) : (h % 12);
    }
    min = (min < 10) ? '0' + min : min;
    return f + ' ' + h + ':' + min + ' ' + a;
}
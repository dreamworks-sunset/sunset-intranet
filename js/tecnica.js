

$(document).ready(function() {

	limpiarCampos();
	consultarTecnicas();
	consultarTipo_tecnica();

	$('#btnGuardar').click(function() {
		var form = document.getElementById("formTecnica");
		var postData = new FormData(form);
		var IdTipo_tecnica = $('#txtIdTipo_tecnica').attr('data-id-tipo_tecnica');
		postData.append('id_tipo_tecnica', IdTipo_tecnica);
		console.log(IdTipo_tecnica);
		$.ajax(
			{
				url: HOST + '/api/tecnica',
				type: 'POST',
				dataType: 'json',
				data: postData,
				processData: false,
				contentType: false,
				success: function(data) {
					swal('Registro exitoso', '', 'success');
					limpiarCampos();
					consultarTecnicas();
				},
				error: function(xhr) {
					if(xhr.responseJSON == undefined) {
						swal('Ocurrió un error', 'No se pudo conectar con el servidor', 'error');
						return;
					}
					swal('Ocurrió un error',
							xhr.status + ' ' + xhr.statusText + ': ' +
							xhr.responseJSON.data.message,
							'error');
				}
			}
		);
	});

	$('#btnModificar').click(function() {
		var form = document.getElementById("formTecnica");
		var putData = new FormData(form);
		var IdTipo_tecnica = $('#txtIdTipo_tecnica').attr('data-id-tipo_tecnica');
		putData.append('id_tipo_tecnica', IdTipo_tecnica);
		var id = $('#txtId').val();
		$.ajax(
			{
				url: HOST + '/api/tecnica/' + id,
				type: 'PUT',
				dataType: 'json',
				data: putData,
				processData: false,
				contentType: false,
				success: function(data) {
					swal('Modificación exitosa', '', 'success');
					limpiarCampos();
					consultarTecnicas();
				},
				error: function(xhr) {
					swal('Ocurrió un error',
							xhr.status + ' ' + xhr.statusText + ': ' +
							xhr.responseJSON.data.message,
							'error');
				}
			}
		);
	});

	$('#btnHabilitar').click(function() {
		var putData = new FormData();
		putData.append('estatus', '1');
		var id = $('#txtId').val();
		$.ajax(
			{
				url: HOST + '/api/tecnica/' + id,
				type: 'PUT',
				dataType: 'json',
				data: putData,
				processData: false,
				contentType: false,
				success: function(data) {
					swal('Habilitación exitosa', '', 'success');
					limpiarCampos();
					consultarTecnicas();
				},
				error: function(xhr) {
					swal('Ocurrió un error',
							xhr.status + ' ' + xhr.statusText + ': ' +
							xhr.responseJSON.data.message,
							'error');
				}
			}
		);
	});

	$('#btnDeshabilitar').click(function() {
		var putData = new FormData();
		putData.append('estatus', '0');
		var id = $('#txtId').val();
		$.ajax(
			{
				url: HOST + '/api/tecnica/' + id,
				type: 'PUT',
				dataType: 'json',
				data: putData,
				processData: false,
				contentType: false,
				success: function(data) {
					swal('Deshabilitación exitosa', '', 'success');
					limpiarCampos();
					consultarTecnicas();
				},
				error: function(xhr) {
					swal('Ocurrió un error',
							xhr.status + ' ' + xhr.statusText + ': ' +
							xhr.responseJSON.data.message,
							'error');
				}
			}
		);
	});

	$('#btnCancelar').click(function() {
		limpiarCampos();
	});

});

function seleccionarFila(el) {
	var idTecnica = $(el).attr('data-id-tecnica');
	$('#txtId').val(idTecnica);
	$('#txtNombre').val($(el).children('td').eq(0).text());
	$('#txtDescripcion').val($(el).children('td').eq(1).text());
	$('#txtIdTipo_tecnica').val($(el).children('td').eq(2).text());
	$('#txtIdTipo_tecnica').attr('data-id-tipo_tecnica', $(el).attr('data-id-tipo_tecnica'));
	var habilitado = $(el).children('td').eq(3).data('estatus');
	if(habilitado == 1) {
		$('#btnDeshabilitar').show();
		$('#btnHabilitar').hide();
	} else {
		$('#btnDeshabilitar').hide();
		$('#btnHabilitar').show();
	}
	$('#btnGuardar').hide();
	$('#btnModificar').show();
}

function seleccionarTipo_tecnica(el) {
	$('#txtIdTipo_tecnica').attr('data-id-tipo_tecnica', $(el).attr('data-id-tipo_tecnica'));
	$('#txtIdTipo_tecnica').val($(el).children('td').eq(0).text());
	$('#ModalAsignarProcedimiento').modal('toggle');

}

function consultarTecnicas() {
	$.ajax(
		{
			url: HOST + '/api/tecnica',
			type: 'GET',
			success: function(result) {
				//Destruir la tabla para luego reinicializarla más adelante
                if($.fn.DataTable.isDataTable("#tablaTecnica")) {
                    $('#tablaTecnica').DataTable().clear().destroy();
                }
				//$('#tablaTecnica tbody').empty();
				$.each(result.data, function(index, data) {
					$('#tablaTecnica tbody').append(
						'<tr data-id-tecnica="' + data.id + '" data-id-tipo_tecnica="' + data.id_tipo_tecnica + '"onclick="seleccionarFila(this)">' +
							'<td>' + data.nombre + '</td>' +
							'<td>' + data.descripcion + '</td>' +
							'<td>' + data.tipo_tecnica.descripcion + '</td>' +
							'<td data-estatus="' + data.estatus + '">' + ((data.estatus == 1) ? 'Sí' : 'No') + '</td>' +
						'</tr>'
					);
				});
				$('.js-exportable').DataTable({
                    dom: 'Bfrtip',
                    responsive: true,
                    buttons: [
                        'copy', 'csv', 'excel', 'pdf', 'print'
                    ]
                });
				$('#tablaTecnica tbody tr').css('cursor', 'pointer');
				$('td[data-estatus="1"]').addClass('habilitado');
				$('td[data-estatus="0"]').addClass('deshabilitado');
			},
			error: function(xhr) {
				swal('Ocurrió un error',
						xhr.status + ' ' + xhr.statusText + ': ' +
						xhr.responseJSON.data.message,
						'error');
			}
		}
	);
}

function consultarTipo_tecnica(){
	$.ajax(
		{
			url: HOST + '/api/tipo_tecnica',
			type: 'GET',
			success: function(result) {
				$('#tablaTipo_tecnica tbody').empty();
				$.each(result.data, function(index, data) {
					$('#tablaTipo_tecnica tbody').append(
						'<tr data-id-tipo_tecnica="' + data.id + '" onclick="seleccionarTipo_tecnica(this)">' +
							'<td>' + data.descripcion + '</td>' +
						
			

						'</tr>'
					);
				});
				$('#tablaTipo_tecnica tbody tr').css('cursor', 'pointer');
			},
			error: function(xhr) {
				swal('Ocurrió un error',
						xhr.status + ' ' + xhr.statusText + ': ' +
						xhr.responseJSON.data.message,
						'error');
			}
		}
	);
}

function limpiarCampos() {
	$('#txtId').val("")
	$('#txtNombre').val("");
	$('#txtDescripcion').val("");
	$('#txtIdTipo_tecnica').val("");
	$('#btnGuardar').show();
	$('#btnModificar, #btnHabilitar, #btnDeshabilitar').hide();
}

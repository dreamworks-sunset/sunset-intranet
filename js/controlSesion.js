//Activa (true) o desactiva (false) redirección si una sesión está o no activa
var activarControlSesion = true;

//Dirección del host de la API sunset-services
const HOST = 'http://localhost:3000';

if(activarControlSesion) {
    //Página actual donde estoy ubicado
    var pagina_actual = window.location.pathname.split('/').pop();
    
    //Si la sesión no está activa, redirige a la página "Iniciar sesión"
    if(localStorage.sesion != 'true' && pagina_actual != 'IniciarSesion.html' && pagina_actual != 'RecuperarContrasena.html') {
        window.location.replace("IniciarSesion.html");
    }

    //Si estoy en "Iniciar sesión" o "Recuperar contraseña" y la sesión está activa,
    //redirige a la primera página a la que el usuario tenga acceso
    if(pagina_actual == 'IniciarSesion.html' || pagina_actual == 'RecuperarContrasena.html') {
        if(localStorage.sesion == 'true') {
            window.location.replace(localStorage.paginaInicio);
        }
    }

}

$(document).ready(function() {

        consultarTipoServicios();
        consultarReporteEstadistico();        
   
        //getMorris('donut', 'donut_chart');


         $('#cmbTipoServicio').change(function() {

         var idTipoServicio = $(this).val();
         consultarServiciosPorTipoServicio(idTipoServicio);
                 });


        $("#tablaReporte").hide();
        
        $("#grafica").hide();


        $('#btnGenerar').click(function() {
            $("#tablaReporte").show();
            $("#grafica").show();
            $('#donut_chart').empty();
            consultarReporteEstadistico();
            
        });
         $('#btnCancelar').click(function() {
            $("#tablaReporte").hide();
            $("#grafica").hide();
        });

        /* $("#dtpFechaInicial").datepicker({
                onSelect: function() { 
                    var fehcaInicial = $(this).datepicker('getDate'); 
                }
            });
         console.log(fehcaInicial);*/



});


function getMorris(type, element, datos, colores) {
    if (type === 'donut') {
        Morris.Donut({
            element: element,
            /*data: [
                {
                    label: 'Procesados',
                    value: 35
                }, {
                    label: 'No procesados por servicio sin garantía',
                    value: 12
                }, {
                    label: 'No procesados por no cumplir con las condicones de garantia',
                    value: 7
                }
            ],*/
            data: datos,
            //colors: ['rgb(233, 30, 99)', 'rgb(0, 188, 212)','rgb(0, 18, 12)'],
            colors: colores,
            formatter: function (y) {
                return y + '%'
            }
        });
    }
}

function consultarReporteEstadistico() {
    $.ajax(
        {
            url: HOST + '/api/vista_reporte_tipo_respuesta_solicitud_por_servicio',
            type: 'GET',
            success: function(result) {
                 //var colors = {};

                //Destruir la tabla para luego reinicializarla más adelante
                if($.fn.DataTable.isDataTable("#tablaReporteSolicitud")) {
                    $('#tablaReporteSolicitud').DataTable().clear().destroy();
                }
                //$('#tablaReporteSolicitud tbody').empty();
                    
                var tiposRespuesta = []; //arreglo que almacena la cantidad de respuestas por tipo
                var idsTiposRespuesta = {}; //diccionario para almacenar los id de tipo de respuesta con sus valores
                var colors = {}; //dicionario para almacenar los colores de cada tipo de respuesta
                var total = 0.0; //Necesario para sacar el porcentaje


                $.each(result.data, function(index, data) {
               
                var idServicio = $('#cmbServicio').val();
                var fecha_ini = formatFechaDMYToYMD($('#dtpFechaInicial').val());
                var fecha_fin = formatFechaDMYToYMD($('#dtpFechaFinal').val());
                var fecha_creacion = new Date(data.fecha_creacion);


                //Filtrar por fecha
                if(fecha_creacion.getTime() < fecha_ini.getTime() || fecha_creacion.getTime() > fecha_fin.getTime()) {
                    return true; //continúa en la siguiente iteración
                }

                //Filtrar por servicio
                if(idServicio != 0 && idServicio != data.id_servicio) {
                    return true; //continúa en la siguiente iteración
                }


                    $('#tablaReporteSolicitud tbody').append(
                        '<tr >' +
                            '<td>' + data.cliente + '</td>' +
                            '<td>' + data.servicio + '</td>' +
                            '<td>' + data.tipo_respuesta_solicitud + '</td>' +
                            '<td>' + formatFecha(data.fecha_creacion) + '</td>' +
                        '</tr>'
                    );
                    
                   

                 idsTiposRespuesta[data.id_tipo_respuesta_solicitud] = data.tipo_respuesta_solicitud;
                colors[data.id_tipo_respuesta_solicitud] = getRandomColor();
                
                if(tiposRespuesta[data.id_tipo_respuesta_solicitud] == undefined) {
                    tiposRespuesta[data.id_tipo_respuesta_solicitud] = 1.0;
                } else {
                    tiposRespuesta[data.id_tipo_respuesta_solicitud]++;
                }
                total++;
            });
            //Aquí se reinicializa la tabla
            $(".js-basic-example").dataTable({
                responsive: true
            });

            var datos = []; //arreglo con los datos que se le pasarán al gráfico
            $.each(idsTiposRespuesta, function(key, value) {
                datos.push({
                    label: value,
                    value: parseFloat(tiposRespuesta[key] / total * 100).toFixed(2)
                });


                });
                getMorris('donut', 'donut_chart', datos, Object.values(colors));
        
             
            },
            error: function(xhr) {
                swal('Ocurrió un error',
                        xhr.status + ' ' + xhr.statusText + ': ' +
                        xhr.responseJSON.data.message,
                        'error');
            }
        }
    );
}

function consultarTipoServicios() {
     
    $.ajax(
    {
        url: HOST + '/api/tipo_servicio',
        type: 'GET',
        success: function(result) {
            $('#cmbTipoServicio').html('<option value="" selected disabled hidden>Tipo Servicio</option>');
            $.each(result.data, function(index, data) {
                $('#cmbTipoServicio').append('<option data-id-tipo-servicio="' + data.id_tipo_servicio + '" value="' + data.id + '">' + data.descripcion + '</option>')
            });
            $('#cmbTipoServicio').selectpicker('refresh');
        },
        error: function(xhr) {
            console.log('Ocurrió un error: ' + xhr.status + ' ' + xhr.statusText);
        }
      }
    );

}

function consultarServiciosPorTipoServicio(id) {
        $('#cmbServicio').html('<option value="0">Todos</option>');
       
    $.ajax(
    {

        url: HOST + '/api/tipo_servicio/'+id,
        type: 'GET',
        success: function(result) {
            $('#cmbServicio').html('<option value="" selected disabled hidden>Servicio</option>');
            $.each(result.data.servicio, function(index, data) {
                $('#cmbServicio').append('<option value="' + data.id + '">' + data.nombre + '</option>')
            });
            $('#cmbServicio').selectpicker('refresh');
        },
        error: function(xhr) {
            console.log('Ocurrió un error: ' + xhr.status + ' ' + xhr.statusText);
        }
      }
    );

}

function formatFecha(date) {
    //Ejemplo: '2018-04-27T04:34:03.687Z'
    var fechaHoraStr = String(date).split('T'); //Separa la fecha de la hora
    var fecha = fechaHoraStr[0].split('-'); //Separa año, mes y día

    //Concatena cada valor de fecha en formato dd/mm/yyyy
    return fecha[2] + '/' + fecha[1] + '/' + fecha[0];
}
function getRandomColor() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 3; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  }
  function formatFechaDMYToYMD(date) {
    var fecha = date.split('/');
    var fec = new Date(fecha[2], parseInt(fecha[1]) - 1, fecha[0]);
    return fec;
}
$(function () {
    $("#RecuperarContrasena").validate({
  rules: {
    contrasena: "required",
    email: {
      required: true,
      email: true
    }
  },
  messages: {
    email: {
      required: "Este campo es obligatorio",
      email: "Tu correo electrónico debe ser en formato nombre@dominio.com"
    },
    contrasena: {
      required: "Este campo es obligatorio",
    }
  }
});

});


$(document).ready(function() {
    var usuario = JSON.parse(localStorage.usuario);
    var vistas_no_menu = JSON.parse(localStorage.vistasNoMenu);
    var funcion = 'RegistrarDetalleSesion.html';
    consultarCitas();
    var accesoFuncion = false;
    $.each(vistas_no_menu, function(index, data) {
        if(data == funcion) {
            accesoFuncion = true;
            return false;
        }
    });
    //Solo permite acceder al 
    if(!accesoFuncion) {
        $('#btnRegistrarDetalleSesion').remove();
    }

    $('#btnRegistrarDetalleSesion').click(function() {
        window.location.href = funcion;
    });

});

function consultarCitas() {
    $.ajax(
		{
			url: HOST + '/api/vista_citas_no_atendidas',
			type: 'GET',
			success: function(result) {
                var eventos = [];
                var esMasajista = localStorage.usuarioEsMasajista == 'true';
                var esCliente = localStorage.usuarioEsCliente == 'true';
                var usuario = JSON.parse(localStorage.usuario);
				$.each(result.data, function(index, data) {
                    
                    if(esMasajista) {
                        if(data.id_empleado != usuario.empleado.id) {
                            return true; //Continúa a la siguiente iteración
                        }
                    }
                    if(esCliente) {
                        if(data.id_cliente != usuario.cliente.id) {
                            return true; //Continúa a la siguiente iteración
                        }
                    }
                    var time = data.hora_inicio.split(':');
                    var fecha = new Date(data.fecha);
                    fecha.setHours(time[0], time[1]);
					eventos.push({
                        id: data.id_cita,
                        title: '\n' + data.nombres_cliente + ' ' + data.apellidos_cliente,
                        start: fecha,
                        allDay: false,
                        fecha: data.fecha.split('T')[0] + 'T' + data.hora_inicio,
                        id_cliente: data.id_cliente,
                        cliente: data.nombres_cliente + ' ' + data.apellidos_cliente,
                        id_empleado: data.id_empleado,
                        empleado: data.nombres_empleado + ' ' + data.apellidos_empleado,
                        servicio: data.nombre,
                        tiempo_sesion: data.tiempo_por_sesion
                    });
				});
				inicializarCalendario(eventos);
			},
			error: function(xhr) {
				if(xhr.responseJSON == undefined) {
					swal('Ocurrió un error', 'No se pudo conectar con el servidor', 'error');
					return;
				}
				swal('Ocurrió un error',
						xhr.status + ' ' + xhr.statusText + ': ' +
						xhr.responseJSON.data.message,
						'error');
			}
		}
	);
}

function inicializarCalendario(eventos) {

    /** Estructura de un evento
     * 
     *  evento = {
     *      id: "1",
     *      title: "Cumpleaños mamá",
     *      start: fecha_inicio_evento, // tipo Date
     *      [end : fecha_fin_evento,] // tipo Date, opcional
     *      allDay: false, // evento de todo el día, verdadero o falso
     *      [url : url_evento,] // evento puede incluir un link cliqueable, opcional
     *  }
     */
    $('#calendario').fullCalendar({
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,basicWeek,basicDay'
        },
        editable: false,
        droppable: false, // this allows things to be dropped onto the calendar !!!
        events: eventos, // aquí se agrega el array de todos los eventos al calendario

        // para cada evento, eventRender ejecuta una determinada secuencia de instrucciones
        eventRender: function(event, element) {
            // event: evento en el calendario
            // element: objeto HTML que representa el evento
            element.attr('id', event.id);
            element.css('cursor', 'pointer');
            element.attr('onclick', 'verDetalleCita(' +
                '"' + event.id + '",' +
                '"' + event.fecha + '",' +
                '"' + event.id_cliente + '",' +
                '"' + event.cliente + '",' +
                '"' + event.servicio + '",' +
                '"' + event.id_empleado + '",' +
                '"' + event.empleado + '",' +
                '"' + event.tiempo_sesion + '")'
            );
        },
        timeFormat: 'h:mm tt', // mostrar hora en formato 12 horas (Ejemplo: 10:00 am)
        //eventColor: '#FF5722' // para colorear los cuadros de los eventos
    });
}

function verDetalleCita(idCita, fecha, idCliente, cliente, servicio, idEmpleado, empleado, tiempo_sesion) {
    $('#txtIdCita').val(idCita);
    $('#txtIdEmpleado').val(idEmpleado);
    $('#spanFechaCita').text(formatFechaHora(fecha));
    $('#linkNombreCliente').text(cliente);
    //$('#linkNombreCliente').attr('href', 'CrearPerfil.html?id=' + idCliente);
    localStorage.idCita = idCita;
    localStorage.idCliente = idCliente;
    $('#spanServicio').text(servicio);
    $('#spanEmpleado').text(empleado);
    $('#spanTiempoSesion').text(tiempo_sesion + ' bloque(s) de 30 minutos');
    $('#modalCita').modal('show');
}

function formatFechaHora(date) {
    //Ejemplo: '2018-04-27T04:34:03.687Z'
    var fechaHoraStr = String(date).split('T'); //Separa la fecha de la hora
    var fecha = fechaHoraStr[0].split('-'); //Separa año, mes y día

    //Concatena cada valor de fecha en formato dd/mm/yyyy
    var f = fecha[2] + '/' + fecha[1] + '/' + fecha[0];

    var hora = fechaHoraStr[1].split(':'); //Separa hora, minuto y segundo
    var h = parseInt(hora[0]); //Convierte hora a entero en formato 24h
    var min = parseInt(hora[1]);
    var a = (h < 12) ? 'AM' : 'PM'; //Si h es menor a 12 es AM, si no, PM
    if(h == 0) {
        h = 12;
    } else if(h > 12) {
        h = (h % 12 < 10) ? '0' + (h % 12) : (h % 12);
    }
    min = (min < 10) ? '0' + min : min;
    return f + ' ' + h + ':' + min + ' ' + a;
}
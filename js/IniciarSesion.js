$(function () {

  $('#IniciarSesion').submit(function(e) {
      e.preventDefault();
  });

  var valido = $("#IniciarSesion").validate({
    rules: {
      contrasenia: "required",
      correo: {
        required: true,
        email: true
      }
    },
    messages: {
      correo: {
        required: "Este campo es obligatorio",
        email: "Tu correo electrónico debe ser en formato nombre@dominio.com"
      },
      contrasenia: {
        required: "Este campo es obligatorio",
      }
    },
    submitHandler: function(form) {
      var postData = new FormData(form);
      $.ajax(
        {
          url: HOST + '/api/login_wd',
          type: 'POST',
          async: false,
          dataType: 'json',
          data: postData,
          processData: false,
          contentType: false,
          success: function(result) {
            //Si el usuario que inició sesión no tiene ningún permiso asignado, redirigir a página de error
            if(result.data.rol.menu_opcion.length == 0) {
              localStorage.errorEstatus = 403;
              localStorage.errorMensaje = 'No tienes permiso para acceder a Sunset-intranet.';
              
              //Como el usuario no tiene ningún permiso, no hay necesidad de almacenar su información.
              //Se limpia el localStorage cuando se cargue la página de error
              localStorage.limpiar = true;
              window.location.href = "error.html";
              return false;
            }

            //Almacenamos los datos del usuario y sus relaciones en el localStorage
            localStorage.usuario = JSON.stringify(result.data);
            localStorage.idUsuario = result.data.id;
            localStorage.sesion = true;

            //Determinamos el tipo de usuario que está iniciando la sesión
            if(result.data.empleado.id != undefined) {
              if(result.data.rol.id_tipo_rol == 2) {
                  localStorage.usuarioEsMasajista = true;
              } else if(result.data.rol.id_tipo_rol == 1) {
                localStorage.usuarioEsAdministrativoGerencial = true;
              }
            } else if(result.data.cliente.id != undefined) {
              localStorage.usuarioEsCliente = true;
            }
            
            //Determinamos la primera página a la cual el usuario tiene acceso
            var visitar_pagina = result.data.rol.menu_opcion[0].url;
            localStorage.paginaInicio = visitar_pagina;

            //Finalmente se dirige a la página objetivo
            //window.location.href = 'url'   : Redirección normal
            //window.location.replace('url') : Redirección eliminando del historial la página anterior
            window.location.replace(visitar_pagina);
          },
          error: function(xhr) {
            if(xhr.responseJSON == undefined) {
              swal('Ocurrió un error', 'No se pudo conectar con el servidor', 'error');
              return false;
            }
            swal('Ocurrió un error',
                xhr.status + ' ' + xhr.statusText + ': ' +
                xhr.responseJSON.data.message,
                'error');
            return false;
          }
        }
      );
    }
  });

});

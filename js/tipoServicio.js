

$(document).ready(function() {
	
	limpiarCampos();
	consultarTipoServicio();
		
	 
		$('#btnGuardar').click(function() {
			var form = document.getElementById("formTipoServicio");
			var postData = new FormData(form);
			$.ajax(
				{
					url: HOST + '/api/tipo_servicio',
					type: 'POST',
					dataType: 'json',
					data: postData,
					processData: false,
					contentType: false,
					success: function(data) {
					
						swal('Registro exitoso', '', 'success');
						limpiarCampos();
						consultarTipoServicio();
					},
					error: function(xhr) {
						swal('Ocurrió un error',
								xhr.status + ' ' + xhr.statusText + ': ' +
								xhr.responseJSON.data.message,
								'error');
					}
				}
			);
		});
		$('#btnModificar').click(function() {
			var form = document.getElementById("formTipoServicio");
			var putData = new FormData(form);
			console.log(form);
			putData.append('cambio_estatus' , 'false');
			var id = $('#txtId').val();
			$.ajax(
				{
					url: HOST + '/api/tipo_servicio/' + id,
					type: 'PUT',
					dataType: 'json',
					data: putData,
					processData: false,
					contentType: false,
					success: function(data) {
						swal('Modificación exitosa', '', 'success');
						limpiarCampos();
						consultarTipoServicio();
					},
					error: function(xhr) {
						swal('Ocurrió un error',
								xhr.status + ' ' + xhr.statusText + ': ' +
								xhr.responseJSON.data.message,
								'error');
					}
				}
			);
		});
		
	
		

		
		
		$('#btnHabilitar').click(function() {
			var putData = new FormData();
			putData.append('estatus', '1');
			putData.append('cambio_estatus' , 'true');

			var id = $('#txtId').val();
			$.ajax(
				{
					url: HOST + '/api/tipo_servicio/' + id,
					type: 'PUT',
					dataType: 'json',
					data: putData,
					processData: false,
					contentType: false,
					success: function(data) {
						swal('Habilitación exitosa', '', 'success');
						limpiarCampos();
						consultarTipoServicio();
					},
					error: function(xhr) {
						swal('Ocurrió un error',
								xhr.status + ' ' + xhr.statusText + ': ' +
								xhr.responseJSON.data.message,
								'error');
					}
				}
			);
		});

		$('#btnDeshabilitar').click(function() {
			var putData = new FormData();
			putData.append('estatus', '0');
			putData.append('cambio_estatus' , 'true');

			var id = $('#txtId').val();
			$.ajax(
				{
					url: HOST + '/api/tipo_servicio/' + id,
					type: 'PUT',
					dataType: 'json',
					data: putData,
					processData: false,
					contentType: false,
					success: function(data) {
						swal('Deshabilitación exitosa', '', 'success');
						limpiarCampos();
						consultarTipoServicio();
					},
					error: function(xhr) {
						swal('Ocurrió un error',
								xhr.status + ' ' + xhr.statusText + ': ' +
								xhr.responseJSON.data.message,
								'error');
					}
				}
			);
		});

		$('#btnCancelar').click(function() {
			limpiarCampos();
		});

});
//		FUNCIONES

function seleccionarFila(el) {
	$('#txtId').val($(el).attr('id'));
	$('#txtDescripcion').val($(el).children('td').eq(0).text());
	$('#imgImagen').attr('src',$(el).data('imagen'));
	$('#imgImagen').show();
	var habilitado = $(el).children('td').eq(1).data('estatus');
	if(habilitado == 1) {
		$('#btnDeshabilitar').show();
		$('#btnHabilitar').hide();
	} else {
		$('#btnDeshabilitar').hide();
		$('#btnHabilitar').show();
	}
	$('#btnGuardar').hide();
	$('#btnModificar').show();
}


function consultarTipoServicio() {
	$.ajax(
		{
			url: HOST + '/api/tipo_servicio',
			type: 'GET',
			success: function(result) {
				//Destruir la tabla para luego reinicializarla más adelante
                if($.fn.DataTable.isDataTable("#tiposServicios")) {
                    $('#tiposServicios').DataTable().clear().destroy();
                }
				//$('#tiposServicios tbody').empty();
				$.each(result.data, function(index, data) {
					$('#tiposServicios tbody').append(
							'<tr id="' + data.id + '" data-imagen="' + data.imagen +'" onclick="seleccionarFila(this)">' +
							'<td>' + data.descripcion + '</td>' +
							'<td data-estatus="' + data.estatus + '">' + ((data.estatus == 1) ? 'Sí' : 'No') + '</td>' +
							'</tr>'
					);
				});
				$('.js-exportable').DataTable({
                    dom: 'Bfrtip',
                    responsive: true,
                    buttons: [
                        'copy', 'csv', 'excel', 'pdf', 'print'
                    ]
                });
				$('#tiposServicios tbody tr').css('cursor', 'pointer');
				$('td[data-estatus="1"]').addClass('habilitado');
				$('td[data-estatus="0"]').addClass('deshabilitado');
			},
			error: function(xhr) {
				swal('Ocurrió un error',
						xhr.status + ' ' + xhr.statusText + ': ' +
						xhr.responseJSON.data.message,
						'error');
			}
		}
    );
    
}

function limpiarCampos() {
	$('#imgImagen').attr('src', '');
	$('#imgImagen').hide();
	$('#txtId').val("")
	$('#txtDescripcion').val("");
	$('#elegir').val("");
	$('#btnGuardar').show();
	$('#btnModificar, #btnHabilitar, #btnDeshabilitar').hide();
	resetFile();
}

function resetFile() {
	$('#elegir').wrap('<form></form>').closest('form').get(0).reset();
	$('#elegir').unwrap();
}
$(document).ready(function() {
    var estatus = localStorage.errorEstatus;
    var mensaje = localStorage.errorMensaje;
    $('.error-code').text(estatus);
    $('.error-message').text(mensaje);
    if(localStorage.limpiar == 'true') {
        localStorage.clear();
    }

    $('#btnAtras').click(function() {
        window.history.back();
    });

});
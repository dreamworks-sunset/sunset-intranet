

$(document).ready(function() {

	limpiarCampos();
	consultarPreguntasFrecuentes();
    consultarNegocio();

	$('#btnGuardar').click(function() {
		var form = document.getElementById("formPreguntasFrecuentes");
		var postData = new FormData(form);
		$.ajax(
			{
				url: HOST + '/api/pregunta_frecuente',
				type: 'POST',
				dataType: 'json',
				data: postData,
				processData: false,
				contentType: false,
				success: function(data) {
					swal('Registro exitoso', '', 'success');
					limpiarCampos();
					consultarPreguntasFrecuentes();
				},
				error: function(xhr) {
					swal('Ocurrió un error',
							xhr.status + ' ' + xhr.statusText + ': ' +
							xhr.responseJSON.data.message,
							'error');
				}
			}
		);
	});

	$('#btnModificar').click(function() {
		var form = document.getElementById("formPreguntasFrecuentes");
		var putData = new FormData(form);
		var id = $('#txtId').val();
		$.ajax(
			{
				url: HOST + '/api/pregunta_frecuente/' + id,
				type: 'PUT',
				dataType: 'json',
				data: putData,
				processData: false,
				contentType: false,
				success: function(data) {
					swal('Modificación exitosa', '', 'success');
					limpiarCampos();
					consultarPreguntasFrecuentes();
				},
				error: function(xhr) {
					swal('Ocurrió un error',
							xhr.status + ' ' + xhr.statusText + ': ' +
							xhr.responseJSON.data.message,
							'error');
				}
			}
		);
	});

	$('#btnHabilitar').click(function() {
		var putData = new FormData();
		putData.append('estatus', '1');
		var id = $('#txtId').val();
		$.ajax(
			{
				url: HOST + '/api/pregunta_frecuente/' + id,
				type: 'PUT',
				dataType: 'json',
				data: putData,
				processData: false,
				contentType: false,
				success: function(data) {
					swal('Habilitación exitosa', '', 'success');
					limpiarCampos();
					consultarPreguntasFrecuentes();
				},
				error: function(xhr) {
					swal('Ocurrió un error',
							xhr.status + ' ' + xhr.statusText + ': ' +
							xhr.responseJSON.data.message,
							'error');
				}
			}
		);
	});

		$('#btnDeshabilitar').click(function() {
		var putData = new FormData();
		putData.append('estatus', '0');
		var id = $('#txtId').val();
		$.ajax(
			{
				url: HOST + '/api/pregunta_frecuente/' + id,
				type: 'PUT',
				dataType: 'json',
				data: putData,
				processData: false,
				contentType: false,
				success: function(data) {
					swal('Deshabilitación exitosa', '', 'success');
					limpiarCampos();
					consultarPreguntasFrecuentes();
				},
				error: function(xhr) {
					swal('Ocurrió un error',
							xhr.status + ' ' + xhr.statusText + ': ' +
							xhr.responseJSON.data.message,
							'error');
				}
			}
		);
	});

	$('#btnCancelar').click(function() {
		limpiarCampos();
	});

});

function seleccionarFila(el) {
	$('#txtId').val($(el).attr('id'));
	$('#txtPregunta').val($(el).children('td').eq(0).text());
	$('#txtRespuesta').val($(el).children('td').eq(1).text());
	$('#cmbNegocio').selectpicker('val', $(el).children('td').eq(2).data('id-negocio'));
	var habilitado = $(el).children('td').eq(3).data('estatus');
	if(habilitado == 1) {
		$('#btnDeshabilitar').show();
		$('#btnHabilitar').hide();
	} else {
		$('#btnDeshabilitar').hide();
		$('#btnHabilitar').show();
	}
	$('#btnGuardar').hide();
	$('#btnModificar').show();
}

function consultarPreguntasFrecuentes() {
	$.ajax(
		{
			url: HOST + '/api/pregunta_frecuente',
			type: 'GET',
			success: function(result) {
				//Destruir la tabla para luego reinicializarla más adelante
                if($.fn.DataTable.isDataTable("#tablaPreguntasFrecuentes")) {
                    $('#tablaPreguntasFrecuentes').DataTable().clear().destroy();
                }
				//$('#tablaPreguntasFrecuentes tbody').empty();
				$.each(result.data, function(index, data) 
                 
				{
					
					$('#tablaPreguntasFrecuentes tbody').append(
						'<tr id="' + data.id + '" onclick="seleccionarFila(this)">' +
							'<td>' + data.pregunta + '</td>' +
							'<td>' + data.respuesta + '</td>' +
							'<td data-id-negocio="' + data.id_negocio+ '">' + data.negocio.nombre + '</td>' +
							'<td data-estatus="' + data.estatus + '">' + ((data.estatus == 1) ? 'Sí' : 'No') + '</td>' +
						'</tr>'
					);
				});
				$('.js-exportable').DataTable({
                    dom: 'Bfrtip',
                    responsive: true,
                    buttons: [
                        'copy', 'csv', 'excel', 'pdf', 'print'
                    ]
                });
				$('#tablaPreguntasFrecuentes tbody tr').css('cursor', 'pointer');
				$('td[data-estatus="1"]').addClass('habilitado');
				$('td[data-estatus="0"]').addClass('deshabilitado');
			},
			error: function(xhr) {
				swal('Ocurrió un error',
						xhr.status + ' ' + xhr.statusText + ': ' +
						xhr.responseJSON.data.message,
						'error');
			}
		}
	);
}
	function consultarNegocio() {
	$.ajax(
{
		url: HOST + '/api/negocio',
		type: 'GET',
		success: function(result) {
			$('#cmbNegocio').html('<option value="0" selected disabled hidden>Ej: xxxxxxxx</option>');

			$.each(result.data, function(index, data) {
				$('#cmbNegocio').append('<option data-id-negocio="'+ data.id_negocio +'" value="' + data.id + '">' + data.nombre + '</option>');
				
			});
			$('#cmbNegocio').selectpicker('refresh');
		},
		error: function(xhr) {
			if(xhr.responseJSON == undefined) {
				swal('Ocurrió un error', 'No se pudo conectar con el servidor', 'error');
				return;
			}
			swal('Ocurrió un error',
					xhr.status + ' ' + xhr.statusText + ': ' +
					xhr.responseJSON.data.message,
					'error');
		}

	});
}

function limpiarCampos() {
	$('#txtId').val("")
	$('#txtPregunta').val("");
	$('#txtRespuesta').val("");
	$('#cmbNegocio').selectpicker('val', 0);
	$('#btnGuardar').show();
	$('#btnModificar, #btnHabilitar, #btnDeshabilitar').hide();
}



$(document).ready(function() {
    limpiarCampos();
    consultarRol();
	consultarUsuarios();


	$('#btnGuardar').click(function() {
		var form = document.getElementById("formUsuario");
		var postData = new FormData(form);
		var idcLIENTE = $('#txtIdCliente').attr('data-id-cliente');
		postData.append('id_cliente', idCliente);
		console.log(idCliente);
		$.ajax(
			{
				url: HOST + '/api/usuario',
				type: 'POST',
				dataType: 'json',
				data: postData,
				processData: false,
				contentType: false,
				success: function(data) {
					swal('Registro exitoso', '', 'success');
					limpiarCampos();
					consultarUsuarios();
					
				},
				error: function(xhr) {
					swal('Ocurrió un error',
							xhr.status + ' ' + xhr.statusText + ': ' +
							xhr.responseJSON.data.message,
							'error');
				}
			}
		);
	});

	$('#btnModificar').click(function() {
		var form = document.getElementById("formUsuario");
		var putData = new FormData(form);
		var id = $('#txtId').val();
		$.ajax(
			{
				url: HOST + '/api/usuario/' + id,
				type: 'PUT',
				dataType: 'json',
				data: putData,
				processData: false,
				contentType: false,
				success: function(data) {
					swal('Modificación exitosa', '', 'success');
					limpiarCampos();
					consultarUsuarios();
				},
				error: function(xhr) {
					swal('Ocurrió un error',
							xhr.status + ' ' + xhr.statusText + ': ' +
							xhr.responseJSON.data.message,
							'error');
				}
			}
		);
	});
$('#btnHabilitar').click(function() {
		var putData = new FormData();
		putData.append('estatus', '1');
		var id = $('#txtId').val();
		$.ajax(
			{
				url: HOST + '/api/usuario/' + id,
				type: 'PUT',
				dataType: 'json',
				data: putData,
				processData: false,
				contentType: false,
				success: function(data) {
					swal('Habilitación exitosa', '', 'success');
					limpiarCampos();
					consultarUsuarios();
				},
				error: function(xhr) {
					swal('Ocurrió un error',
							xhr.status + ' ' + xhr.statusText + ': ' +
							xhr.responseJSON.data.message,
							'error');
				}
			}
		);
	});

	$('#btnDeshabilitar').click(function() {
		var putData = new FormData();
		putData.append('estatus', '0');
		var id = $('#txtId').val();
		$.ajax(
			{
				url: HOST + '/api/usuario/' + id,
				type: 'PUT',
				dataType: 'json',
				data: putData,
				processData: false,
				contentType: false,
				success: function(data) {
					swal('Deshabilitación exitosa', '', 'success');
					limpiarCampos();
					consultarUsuarios();
				},
				error: function(xhr) {
					swal('Ocurrió un error',
							xhr.status + ' ' + xhr.statusText + ': ' +
							xhr.responseJSON.data.message,
							'error');
				}
			}
		);
	});

	$('#btnCancelar').click(function() {
		limpiarCampos();
	});

});

function seleccionarFila(el) {

	$('#txtId').val($(el).attr('data-id-usuario'));
	$('#txtCorreo').val($(el).children('td').eq(0).text());
	$('#txtNombreCliente').val($(el).children('td').eq(1).text());
	$('#txtApellidoCliente').val($(el).children('td').eq(2).text());
	$('#cmbRol').selectpicker('val', $(el).children('td').eq(3).data('id-rol'));


	var habilitado = $(el).children('td').eq(4).data('estatus');
	if(habilitado == 1) {
		$('#btnDeshabilitar').show();
		$('#btnHabilitar').hide();
	} else {
		$('#btnDeshabilitar').hide();
		$('#btnHabilitar').show();
	}
	$('#btnGuardar').hide();
	$('#btnModificar').show();

	
}




function consultarUsuarios() {
	$.ajax(
		{
			url: HOST + '/api/usuario',
			type: 'GET',
			success: function(result) {
				//Destruir la tabla para luego reinicializarla más adelante
                if($.fn.DataTable.isDataTable("#tablaUsuario")) {
                    $('#tablaUsuario').DataTable().clear().destroy();
                }
				//$('#tablaUsuario tbody').empty();
				$.each(result.data, function(index, data) {
					if(data.cliente.id!=undefined)
					{


						$('#tablaUsuario tbody').append(
							'<tr data-id-usuario="' + data.id + '" data-contrasenia="' + data.contrasenia + '"onclick="seleccionarFila(this)">' +
								//'<td>' + formatFechaHora(data.fecha_creacion) + '</td>' +
								'<td>' + data.correo + '</td>' +
								'<td data-nombres-cliente ="' + data.cliente.nombres + '">' + ((data.cliente.nombres == undefined ) ? 'No asignado' : data.cliente.nombres) + '</td>' +
								'<td data-apellidos-cliente ="' + data.cliente.apellidos + '">' + ((data.cliente.apellidos == undefined ) ? 'No asignado' : data.cliente.apellidos) + '</td>' +
								'<td data-id-rol="' + data.id_rol + '">' +  ((data.id_rol == undefined ) ? 'No asignado' : data.rol.nombre) + '</td>' +
								'<td data-estatus="' + data.estatus + '">' + ((data.estatus == 1) ? 'Sí' : 'No') + '</td>' +



							'</tr>'

					     );
				    }
				});
				$('.js-exportable').DataTable({
                    dom: 'Bfrtip',
                    responsive: true,
                    buttons: [
                        'copy', 'csv', 'excel', 'pdf', 'print'
                    ]
                });
				$('#tablaUsuario tbody tr').css('cursor', 'pointer');
				$('td[data-estatus="1"]').addClass('habilitado');
				$('td[data-estatus="0"]').addClass('deshabilitado');
			},
			error: function(xhr) {
				swal('Ocurrió un error',
						xhr.status + ' ' + xhr.statusText + ': ' +
						xhr.responseJSON.data.message,
						'error');
			}
		}
	);
}



function limpiarCampos() {
	$('#txtId').val("")
	$('#txtApellidoCliente').val("");
	$('#txtNombreCliente').val("");
	$('#txtCorreo').val("");
	$('#txtIdCliente').val("");
	$('#cmbRol').selectpicker('val', 0);
	$('#btnModificar, #btnDeshabilitar, #btnHabilitar').hide();
}

function formatFecha(date) {
    //Ejemplo: '2018-04-27T04:34:03.687Z'
    var fechaHoraStr = String(date).split('T'); //Separa la fecha de la hora
    var fecha = fechaHoraStr[0].split('-'); //Separa año, mes y día

    //Concatena cada valor de fecha en formato dd/mm/yyyy
    return fecha[2] + '/' + fecha[1] + '/' + fecha[0];
}

function formatFechaHora(date) {
    //Ejemplo: '2018-04-27T04:34:03.687Z'
    var fechaHoraStr = String(date).split('T'); //Separa la fecha de la hora
    var fecha = fechaHoraStr[0].split('-'); //Separa año, mes y día

    //Concatena cada valor de fecha en formato dd/mm/yyyy
    var f = fecha[2] + '/' + fecha[1] + '/' + fecha[0];

    var hora = fechaHoraStr[1].split(':'); //Separa hora, minuto y segundo
    var h = parseInt(hora[0]); //Convierte hora a entero en formato 24h
    var min = parseInt(hora[1]);
    var a = (h < 12) ? 'AM' : 'PM'; //Si h es menor a 12 es AM, si no, PM
    if(h == 0) {
        h = 12;
    } else if(h > 12) {
        h = (h % 12 < 10) ? '0' + (h % 12) : (h % 12);
    }
    min = (min < 10) ? '0' + min : min;
    return f + ' ' + h + ':' + min + ' ' + a;
}
 

function consultarRol() {
	$.ajax(
	{
		url: HOST + '/api/rol',
		type: 'GET',
		success: function(result) {
			$('#cmbRol').html('<option value="0" selected disabled hidden>Rol</option>');
			$.each(result.data, function(index, data) {
				if (data.id_tipo_rol==3){
				$('#cmbRol').append('<option data-id-rol="' + data.id_rol + '" value="' + data.id + '">' + data.nombre + '</option>')
			   }
			});
			$('#cmbRol').selectpicker('refresh');
		},
		error: function(xhr) {
			console.log('Ocurrió un error: ' + xhr.status + ' ' + xhr.statusText);
		}

	});
}
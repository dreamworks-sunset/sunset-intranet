

$(document).ready(function() {

	consultarSolicitudes();
	consultarTipoRespuestas();
	cargarSolicitud();
	//cargarPresupuesto();

	$('#btnRespuesta1').click(function(){
		enviarRespuesta();

	});

	$('#btnEnviarPresupuesto').click(function(){
		enviarPresupuesto();

	});
});




	

function consultarSolicitudes() {
	$.ajax(
		{
			url: HOST + '/api/solicitud',
			type: 'GET',
			success: function(result) {
				//Destruir la tabla para luego reinicializarla más adelante
                if($.fn.DataTable.isDataTable("#tablaSolicitud")) {
                    $('#tablaSolicitud').DataTable().clear().destroy();
                }
				//$('#tablaSolicitud tbody').empty();
				$.each(result.data, function(index, data) {
					if (data.estatus == 1){
						var porcentaje_descuento = 0;
						if(data.promocion != undefined) {
							porcentaje_descuento = data.promocion.porcentaje_descuento;
						}
					$('#tablaSolicitud tbody').append(
						'<tr id="' + data.id + '" data-id-servicio="' + data.servicio.id + '" data-cantidad="' + data.servicio.cantidad_sesiones + '" data-precio="' + data.servicio.precio_por_sesion + '" data-porcentaje="' + porcentaje_descuento + '">' +
							'<td>' + formatFechaHora(data.fecha_creacion) + '</td>' +
							'<td>' + data.cliente.nombres + ' ' + data.cliente.apellidos + '</td>' +
							'<td>' + data.servicio.nombre+ '</td>' +
							//'<td>' + data.tipo_motivo.descripcion+ '</td>' +
						    '<td data-nombre-promocion="' + data.id_promocion + '">' + ((data.id_promocion != null) ? data.promocion.nombre : 'NO TIENE') + '</td>' +
							
							'<td data-estatus="' + data.servicio.estatus + '">' + ((data.servicio.estatus == 1) ? 'Disponible' : 'No Disponible') + '</td>' +
							'<td>' +
							    '<button type="button" rel="tooltip" data-toggle="modal" ' +
										'data-target="#modalRespuestaSolicitud" data-placement="top" ' +
										'data-container="body" title="Responder" ' +
										'onclick="cargarSolicitud(this)" ' +
										'class="btn btn-success btn-circle waves-effect waves-circle waves-float">' +
                                    '<i class="material-icons">reply </i>' +
                                '</button>' +
								'<button type="button" rel="tooltip" data-toggle="modal" ' +
										'data-target="#modalPresupuestoSolicitud" data-placement="top" ' +
										'data-container="body" title="Ver Presupuesto" ' +
										'onclick="consultarServicio(this); cargarPresupuesto(this)" ' +
										'class="btn btn-warning btn-circle waves-effect waves-circle waves-float">' +
                                    '<i class="material-icons">assignment </i>' +
                                '</button>' +
                                
                            '</td>'+


						'</tr>'


	
					);
				
				}

				$('#tablaSolicitud tbody tr').css('cursor', 'pointer');
				$('td[data-estatus="1"]').addClass('habilitado');
				$('td[data-estatus="0"]').addClass('deshabilitado');

				$('#tablaSolicitud tbody tr').css('cursor', 'pointer');
				$('td[data-nombre-promocion=null]').addClass('deshabilitado');				

				});
				//Aquí se reinicializa la tabla
                $(".js-basic-example").dataTable({
                    responsive: true
                });
			
			},
			error: function(xhr) {
				swal('Ocurrió un error',
						xhr.status + ' ' + xhr.statusText + ': ' +
						xhr.responseJSON.data.message,
						'error');
			}
		}
	);
}

function consultarServicio(el) {
		var id = $(el).parent().parent().data("id-servicio");
		console.log(id);
	$.ajax(
		{
			url: HOST + '/api/servicio/'+ id,
			type: 'GET',
			async: false,
			success: function(result) {
				var nombreInsumos="";
				var acumMonto=0.0;
				$.each(result.data.insumo, function(index, data) {
					
						nombreInsumos += data.nombre + ", " ;
						acumMonto += data.precio_por_sesion; 	

			});
				console.log(nombreInsumos);
				console.log(acumMonto);
				$('#spanInsumo').text(nombreInsumos);
				$('#spanMontoInsumo').text(acumMonto);
		},
			error: function(xhr) {
				swal('Ocurrió un error',
						xhr.status + ' ' + xhr.statusText + ': ' +
						xhr.responseJSON.data.message,
						'error');
			}
		}
	);
}



function formatFecha(date) {
    //Ejemplo: '2018-04-27T04:34:03.687Z'
    var fechaHoraStr = String(date).split('T'); //Separa la fecha de la hora
    var fecha = fechaHoraStr[0].split('-'); //Separa año, mes y día

    //Concatena cada valor de fecha en formato dd/mm/yyyy
    return fecha[2] + '/' + fecha[1] + '/' + fecha[0];
}
function formatFechaHora(date) {
    //Ejemplo: '2018-04-27T04:34:03.687Z'
    var fechaHoraStr = String(date).split('T'); //Separa la fecha de la hora
    var fecha = fechaHoraStr[0].split('-'); //Separa año, mes y día

    //Concatena cada valor de fecha en formato dd/mm/yyyy
    var f = fecha[2] + '/' + fecha[1] + '/' + fecha[0];

    var hora = fechaHoraStr[1].split(':'); //Separa hora, minuto y segundo
    var h = parseInt(hora[0]); //Convierte hora a entero en formato 24h
    var min = parseInt(hora[1]);
    var a = (h < 12) ? 'AM' : 'PM'; //Si h es menor a 12 es AM, si no, PM
    if(h == 0) {
        h = 12;
    } else if(h > 12) {
        h = (h % 12 < 10) ? '0' + (h % 12) : (h % 12);
    }
    min = (min < 10) ? '0' + min : min;
    return f + ' ' + h + ':' + min + ' ' + a;
}

function consultarTipoRespuestas() {
	$.ajax(
	{
		url: HOST + '/api/tipo_respuesta_solicitud',
		type: 'GET',
		success: function(result) {
			$('#cmbTipoRespuesta').html('<option value="" selected disabled hidden>Tipo Respuesta</option>');
			$.each(result.data, function(index, data) {
				$('#cmbTipoRespuesta').append('<option data-id-tipo-respuesta-solicitud="' + data.id_tipo_respuesta_solicitud + '" value="' + data.id + '">' + data.descripcion + '</option>')
			});
			$('#cmbTipoRespuesta').selectpicker('refresh');
		},
		error: function(xhr) {
			console.log('Ocurrió un error: ' + xhr.status + ' ' + xhr.statusText);
		}

	});
}

function cargarSolicitud(el) {
	var idSolicitud = $(el).parent().parent().attr('id');

	var fila = $(el).parent().parent().children();
	var cliente = $(fila).eq(1).text();
	var servicio = $(fila).eq(2).text();
	var promocion = $(fila).eq(3).text();
	$('#spanCliente').text(cliente);
	$('#spanServicio').text(servicio);
	$('#txtIdSolicitud').val(idSolicitud);
}   

function cargarPresupuesto(el) {
	var idSolicitud = $(el).parent().parent().attr('id');
	var cantidadSesiones = $(el).parent().parent().attr('data-cantidad');
	var porcentaje = $(el).parent().parent().attr('data-porcentaje');
	var precio = $(el).parent().parent().attr('data-precio');
	var fila = $(el).parent().parent().children();
	var cliente = $(fila).eq(1).text();
	var servicio = $(fila).eq(2).text();
	var promocion = $(fila).eq(4).text();
	$('#spanCliente1').text(cliente);
	$('#spanServicio1').text(servicio);
	$('#spanPromocion1').text(promocion);
	$('#spanCantidadSesiones').text(cantidadSesiones);
	$('#spanPrecioPorSesion').text(precio);
	$('#spanPorcentaje').text(porcentaje);
	
	
	
	var totalSesiones =  parseFloat($('#spanPrecioPorSesion').text())* parseFloat($('#spanCantidadSesiones').text());
	$('#spanMontoSesiones').text(totalSesiones)
	


	var montoInsumo = parseFloat($('#spanMontoInsumo').text());
	console.log(montoInsumo);

	var totalSinDescuento = totalSesiones + montoInsumo ;
	
	var totalDescuento = totalSinDescuento * parseFloat($('#spanPorcentaje').text())/100;
    $('#spanMontoDescuento').text(totalDescuento);

    var total = totalSinDescuento - totalDescuento;
    $('#spanTotal').text(total);
	//$('#spanTotal').text(total);
	
	$('#txtIdSolicitudPresupuesto').val(idSolicitud);
}

function enviarRespuesta(){

		var form = document.getElementById("formRespuestaSolicitud");
		var postData = new FormData(form);
		$.ajax(
			{
				url: HOST + '/api/respuesta_solicitud',
				type: 'POST',
				dataType: 'json',
				data: postData,
				processData: false,
				contentType: false,
				success: function(data) {
					consultarSolicitudes();
					swal('Respuesta enviada exitosamente', '', 'success');
					$('#modalRespuestaSolicitud').modal('toggle');
					
				},
				error: function(xhr) {
					swal('Ocurrió un error',
							xhr.status + ' ' + xhr.statusText + ': ' +
							xhr.responseJSON.data.message,
							'error');
				}
			}
		);

}

function enviarPresupuesto(){

		var form = document.getElementById("formPresupuestoSolicitud");

		var postData = new FormData(form);
		postData.append('monto', $('#spanTotal').text() );
		$.ajax(
			{
				url: HOST + '/api/presupuesto',
				type: 'POST',
				dataType: 'json',
				data: postData,
				processData: false,
				contentType: false,
				success: function(data) {
					consultarSolicitudes();
					swal('Presupuesto enviado exitosamente', '', 'success');
					$('#modalPresupuestoSolicitud').modal('toggle');
					
				},
				error: function(xhr) {
					swal('Ocurrió un error',
							xhr.status + ' ' + xhr.statusText + ': ' +
							xhr.responseJSON.data.message,
							'error');
				}
			}
		);

}

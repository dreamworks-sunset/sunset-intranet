


$(document).ready(function() {

	limpiarCampos();
	consultarRoles();
	consultarTipoRol();

	$('#btnGuardar').click(function() {
		var form = document.getElementById("formRol");
		var postData = new FormData(form);
		$.ajax(
			{
				url: HOST + '/api/rol',
				type: 'POST',
				dataType: 'json',
				data: postData,
				processData: false,
				contentType: false,
				success: function(data) {
					swal('Registro exitoso', '', 'success');
					limpiarCampos();
					consultarRoles();
				},
				error: function(xhr) {
					swal('Ocurrió un error',
							xhr.status + ' ' + xhr.statusText + ': ' +
							xhr.responseJSON.data.message,
							'error');
				}
			}
		);
	});

	$('#btnModificar').click(function() {
		var form = document.getElementById("formRol");
		var putData = new FormData(form);
		putData.append('cambio_estatus', 'false');
		var id = $('#txtId').val();
		$.ajax(
			{
				url: HOST + '/api/rol/' + id,
				type: 'PUT',
				dataType: 'json',
				data: putData,
				processData: false,
				contentType: false,
				success: function(data) {
					swal('Modificación exitosa', '', 'success');
					limpiarCampos();
					consultarRoles();
				},
				error: function(xhr) {
					swal('Ocurrió un error',
							xhr.status + ' ' + xhr.statusText + ': ' +
							xhr.responseJSON.data.message,
							'error');
				}
			}
		);
	});

	$('#btnHabilitar').click(function() {
		var putData = new FormData();
		putData.append('estatus', '1');
		var id = $('#txtId').val();
		$.ajax(
			{
				url: HOST + '/api/rol/' + id,
				type: 'PUT',
				dataType: 'json',
				data: putData,
				processData: false,
				contentType: false,
				success: function(data) {
					swal('Habilitación exitosa', '', 'success');
					limpiarCampos();
					consultarRoles();
				},
				error: function(xhr) {
					swal('Ocurrió un error',
							xhr.status + ' ' + xhr.statusText + ': ' +
							xhr.responseJSON.data.message,
							'error');
				}
			}
		);
	});

	$('#btnDeshabilitar').click(function() {
		var putData = new FormData();
		putData.append('estatus', '0');
		var id = $('#txtId').val();
		$.ajax(
			{
				url: HOST + '/api/rol/' + id,
				type: 'PUT',
				dataType: 'json',
				data: putData,
				processData: false,
				contentType: false,
				success: function(data) {
					swal('Deshabilitación exitosa', '', 'success');
					limpiarCampos();
					consultarRoles();
				},
				error: function(xhr) {
					swal('Ocurrió un error',
							xhr.status + ' ' + xhr.statusText + ': ' +
							xhr.responseJSON.data.message,
							'error');
				}
			}
		);
	});

	$('#btnCancelar').click(function() {
		limpiarCampos();
	});

});

function seleccionarFila(el) {
	$('#txtId').val($(el).attr('id'));
	$('#txtNombre').val($(el).children('td').eq(0).text());
	$('#txtDescripcion').val($(el).children('td').eq(1).text());
	$('#cmbTipoRol').selectpicker('val', $(el).children('td').eq(2).data('id-tipo-rol'));
	var habilitado = $(el).children('td').eq(3).data('estatus');

	if(habilitado == 1) {
		$('#btnDeshabilitar').show();
		$('#btnHabilitar').hide();		
	    $('#btnModificar').show();
	} else {
		$('#btnDeshabilitar').hide();
		$('#btnHabilitar').show();
		$('#btnModificar').hide();
	}
	$('#btnGuardar').hide();
}

function consultarRoles() {
	$.ajax(
		{
			url: HOST + '/api/rol',
			type: 'GET',
			success: function(result) {
				//Destruir la tabla para luego reinicializarla más adelante
                if($.fn.DataTable.isDataTable("#tablaRol")) {
                    $('#tablaRol').DataTable().clear().destroy();
                }
				//$('#tablaRol tbody').empty();
				$.each(result.data, function(index, data) {
					$('#tablaRol tbody').append(
						'<tr id="' + data.id + '" onclick="seleccionarFila(this)">' +
							'<td>' + data.nombre + '</td>' +
							'<td>' + data.descripcion + '</td>' +
							'<td data-id-tipo-rol="' + data.id_tipo_rol + '">' + data.tipo_rol.descripcion + '</td>' +
							'<td data-estatus="' + data.estatus + '">' + ((data.estatus == 1) ? 'Sí' : 'No') + '</td>' +
						'</tr>'
					);
				});
				$('.js-exportable').DataTable({
                    dom: 'Bfrtip',
                    responsive: true,
                    buttons: [
                        'copy', 'csv', 'excel', 'pdf', 'print'
                    ]
                });
				$('#tablaRol tbody tr').css('cursor', 'pointer');
				$('td[data-estatus="1"]').addClass('habilitado');
				$('td[data-estatus="0"]').addClass('deshabilitado');
			},
			error: function(xhr) {
				swal('Ocurrió un error',
						xhr.status + ' ' + xhr.statusText + ': ' +
						xhr.responseJSON.data.message,
						'error');
			}
		}
	);
}
function consultarTipoRol() {
	$.ajax(
	{
		url: HOST + '/api/tipo_rol',
		type: 'GET',
		success: function(result) {
			$('#cmbTipoRol').html('<option value="0" selected disabled hidden>Tipo Rol</option>');
			$.each(result.data, function(index, data) {
				$('#cmbTipoRol').append('<option data-id-tipo-rol="' + data.id_tipo_rol + '" value="' + data.id + '">' + data.descripcion + '</option>')
			});
			$('#cmbTipoRol').selectpicker('refresh');
		},
		error: function(xhr) {
			console.log('Ocurrió un error: ' + xhr.status + ' ' + xhr.statusText);
		}

	});
}

function limpiarCampos() {
	$('#txtId').val("")
	$('#txtNombre').val("");
	$('#txtDescripcion').val("");
	$('#btnGuardar').show();
	$('#cmbTipoRol').selectpicker('val', 0);
	$('#btnModificar, #btnHabilitar, #btnDeshabilitar').hide();
}


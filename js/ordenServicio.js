

$(document).ready(function() {

	consultarOrdenesServicio();

	$('#btnAsignar').click(function(){
		asignarEmpleado();

	});
	

	



});




	

function consultarOrdenesServicio() {
	$.ajax(
		{
			url: HOST + '/api/orden_servicio',
			type: 'GET',
			success: function(result) {
				//Destruir la tabla para luego reinicializarla más adelante
                if($.fn.DataTable.isDataTable("#tablaOrdenServicio")) {
                    $('#tablaOrdenServicio').DataTable().clear().destroy();
                }
				//$('#tablaOrdenServicio tbody').empty();
				$.each(result.data, function(index, data) {
					if (data.estatus == 1){
						
					$('#tablaOrdenServicio tbody').append(
						'<tr data-id-orden-servicio="' + data.id +  '"data-id-tipo-servicio="' + data.solicitud.servicio.id_tipo_servicio + '" data-id-bloque-hora="' + data.solicitud.id_bloque_hora + '" + " data-id-cliente="' + data.solicitud.id_cliente + '" data-fecha="' + formatFechaAMDHora24(data.solicitud.fecha) + '" data-id-servicio="' + data.solicitud.id_servicio + '" data-bloques-servicio="' + data.solicitud.servicio.tiempo_por_sesion + '">' +
							'<td>' + data.id + '</td>' +
							'<td>' + formatFechaHora(data.fecha_creacion) + '</td>' +
							'<td>' + data.solicitud.cliente.nombres + ' ' + data.solicitud.cliente.apellidos + '</td>' +
							'<td>' + data.solicitud.servicio.nombre+ '</td>' +
							'<td>' + formatFechaHora(data.solicitud.fecha) + '</td>' +
							
							//'<td>' + data.tipo_motivo.descripcion+ '</td>' +
						  
							'<td>' + 
							
							    '<button type="button" rel="tooltip" data-toggle="modal" ' +
										'data-target="#modalAsignarEmpleado" data-placement="top" ' +
										'data-container="body" title="Asignar Empleado" ' +
										'onclick="cargarEmpleado(this)" ' +
										'class="btn btn-success btn-circle waves-effect waves-circle waves-float">' +
                                    '<i class="material-icons">event </i>' +
                                '</button>' +
								'<button type="button" rel="tooltip" "data-toggle="modal" ' +
										'data-target="CrearPerfil.html" data-placement="top" ' +
										'data-container="body" title="Completar Perfil" ' +
										'onclick="crearPerfil(this)" ' +
										'class="btn btn-danger btn-circle waves-effect waves-circle waves-float">' +
                                    '<i class="material-icons">assignment </i>' +
                                '</button>' +
                                
                            '</td>'+


						'</tr>'


	
					);
				
				}

				$('#tablaOrdenServicio tbody tr').css('cursor', 'pointer');
				$('td[data-estatus="1"]').addClass('habilitado');
				$('td[data-estatus="0"]').addClass('deshabilitado');

						

				});

				//Aquí se reinicializa la tabla
                $(".js-basic-example").dataTable({
                    responsive: true
                });
                	
			
			},
			error: function(xhr) {
				swal('Ocurrió un error',
						xhr.status + ' ' + xhr.statusText + ': ' +
						xhr.responseJSON.data.message,
						'error');
			}
		}
	);
}

function cargarEmpleado(el) {
	var idEmpleado = $(el).parent().parent().attr('id');

	var fila = $(el).parent().parent().children();
	var id = $(fila).eq(0).text();
	$('#txtId').val(id);
	$('#txtFecha').val($(el).parent().parent().data('fecha'));
	var cliente = $(fila).eq(2).text();
	var servicio = $(fila).eq(3).text();
	$('#spanFecha').text($(el).parent().parent().data('fecha'));
	
	$('#spanNumeroOrden').text(id);
	$('#spanCliente').text(cliente);
	$('#spanServicio').text(servicio);
	var idTipoServicio= $(el).parent().parent().attr('data-id-tipo-servicio');
	var idBloqueHora = $(el).parent().parent().attr('data-id-bloque-hora');
	var bloquesServicio = $(el).parent().parent().attr('data-bloques-servicio');

	var timestamp = $('#txtFecha').val().split(' ');
	var fecha = timestamp[0];
	console.log('fecha =', fecha);
	var hora = timestamp[1];
	console.log('hora =', hora);

	consultarEmpleados(idTipoServicio, hora, fecha, bloquesServicio);
	
	//$('#txtIdEmpleado').val(idEmpleado);
}   


function crearPerfil(el) {
		localStorage.idCliente = $(el).parent().parent().data('id-cliente');
		window.location.href = "CrearPerfil.html";
	}


function formatFecha(myDate) {
    var date = new Date(myDate).toLocaleDateString('es-ve', {
		year: 'numeric',
		month: '2-digit',
		day: '2-digit',
		hour: '2-digit',
		minute: '2-digit',
		hour12: true

	});

	return date;
}

function formatHora(time) {
    //Ejemplo: '04:34:00'
    var hora = time.split(':'); //Separa hora, minuto y segundo
    var h = parseInt(hora[0]); //Convierte hora a entero en formato 24h
    var min = parseInt(hora[1]);
    var a = (h < 12) ? 'AM' : 'PM'; //Si h es menor a 12 es AM, si no, PM
    if(h == 0) {
        h = 12;
    } else if(h > 12) {
        h = (h % 12 < 10) ? '0' + (h % 12) : (h % 12);
    }
    min = (min < 10) ? '0' + min : min;
    return h + ':' + min + ' ' + a;
}

function formatFechaHora(myDate) {
	//Ejemplo: '2018-04-27T04:34:03.687Z'
	var date = new Date(myDate).toLocaleDateString('es-ve', {
		year: 'numeric',
		month: '2-digit',
		day: '2-digit',
		hour: '2-digit',
		minute: '2-digit',
		hour12: true

	});

	return date;
}

function formatFechaAMD(date) {
	//Ejemplo: '2018-04-27T04:34:03.687Z'
	var myDate = new Date(date);

    //var fechaHoraStr = String(date).split('T'); //Separa la fecha de la hora
    //var fecha = fechaHoraStr[0].split('-'); //Separa año, mes y día

	//Concatena cada valor de fecha en formato dd/mm/yyyy
	var mes = myDate.getMonth() + 1;
	var dia = myDate.getDate();
    var f = myDate.getFullYear() + '-' + ((mes < 10) ? '0' + mes : mes) + '-' + ((dia < 10) ? '0' + dia : dia);
    
    return f;
}

function formatFechaAMDHora24(date) {
	//Ejemplo: '2018-04-27T04:34:03.687Z'
	var myDate = new Date(date);

    //var fechaHoraStr = String(date).split('T'); //Separa la fecha de la hora
    //var fecha = fechaHoraStr[0].split('-'); //Separa año, mes y día

	//Concatena cada valor de fecha en formato dd/mm/yyyy
	var mes = myDate.getMonth() + 1;
	var dia = myDate.getDate();
    var f = myDate.getFullYear() + '-' + ((mes < 10) ? '0' + mes : mes) + '-' + ((dia < 10) ? '0' + dia : dia);

    var hora = myDate.getHours();
    var min = myDate.getMinutes();
    
    return f + ' ' + ((hora < 10) ? '0' + hora : hora) + ':' + ((min < 10) ? '0' + min : min);
}

function seleccionarEmpleado(el) {
	$('#txtIdEmpleado').val($(el).attr('data-id-empleado'));
	$('#txtNombreEmpleado').val($(el).children('td').eq(0).text());
	$('#spanEmpleado').text($(el).children('td').eq(0).text() + ' ' +$(el).children('td').eq(1).text());
 	
	//$('#modalAsignarEmpleado').modal('toggle');

}

function consultarEmpleados(idTipoServicio, hora, fechaSolicitud, bloquesServicio){
	 
	$.ajax(
		{
			url: HOST + '/api/tipo_servicio/' + idTipoServicio,
			type: 'GET',
			success: function(result) {


				$('#tablaEmpleado tbody').empty();

				
				/*var fecha = fechaSolicitud.split('/');
				var dia = fecha[0];
				var mes = parseInt(fecha[1]) - 1;
				var anno = fecha[2];

				var date = new Date(anno, mes, dia);
				
				var diaSemana = date.getDay(); //Día de la semana 0-6 (comenzando en domingo)
				if(diaSemana == 0) {
					diaSemana = 7;
				}*/

				$.each(result.data.empleado, function(index, data) {

					var disponible = false;
					$.each(data.horario_empleado, function(index1, data1) {
						if(data1.horario.bloque_hora.hora_inicio == hora + ':00'
							&& formatFechaAMD(data1.horario.dia_laborable.dia) == fechaSolicitud) {
							console.log('Disponible = true!');
							disponible = true;
							
							$.each(data1.agenda, function(index2, data2) {
								var fechaAgenda = formatFechaAMDHora24(data2.cita.fecha);
								console.log(fechaAgenda);
								var cantBloques = data2.cita.orden_servicio.solicitud.servicio.tiempo_por_sesion;
								console.log(cantBloques);
								var i = cantBloques;
								if(fechaAgenda == formatFechaAMDHora24(fechaSolicitud + 'T' + hora + ':00')) {
									console.log('Ahora no estoy disponible');
									disponible = false;
									return false;
								}
								while(i > 0) {
									console.log('Verificando ocupacion ' + (i * 30) + ' minutos antes');
									var fechaMenos = new Date(data2.cita.fecha);
									fechaMenos.setMinutes(fechaMenos.getMinutes() - 30 * i);
									
									i--;
									var fechaAMenos = formatFechaAMDHora24(fechaMenos);
									
									if(fechaAMenos == formatFechaAMDHora24(fechaSolicitud + 'T' + hora + ':00')) {
										console.log('Ahora no estoy disponible');
										disponible = false;
										break;
									}
								}
								if(disponible) {
									var j = bloquesServicio;
								
									while(j > 0) {
										console.log('Verificando ocupacion ' + (j * 30) + ' minutos despues');
										var fechaMas = new Date(fechaSolicitud + 'T' + hora + ':00');
										fechaMas.setMinutes(fechaMas.getMinutes() + 30 * j);
										var fechaAMas = formatFechaAMDHora24(fechaMas);
										if(fechaAgenda == fechaAMas) {
											console.log('Ahora no estoy disponible');
											disponible = false;
											break;
										}
										j--;
									}
								}
								
								if(!disponible) {
									return false;
								}

							});

							if(disponible) {
								return false;
							}
						}
					});

					if(disponible) {
						$('#tablaEmpleado tbody').append(
							'<tr data-id-empleado ="' + data.id + '" onclick="seleccionarEmpleado(this)">' +
								'<td>' + data.nombres + '</td>' +
								'<td>' + data.apellidos + '</td>' +
							'</tr>'
						);
					}
					
				});
				$('#tablaEmpleado tbody tr').css('cursor', 'pointer');
			},
			error: function(xhr) {
				swal('Ocurrió un error',
						xhr.status + ' ' + xhr.statusText + ': ' +
						xhr.responseJSON.data.message,
						'error');
			}
		}
	);
}





function asignarEmpleado(){

		var form = document.getElementById("formAsignarE");
		var putData = new FormData(form);
		putData.append('estatus', 2);
		var id = $('#txtId').val();

		console.log(id);
		$.ajax(
			{
				url: HOST + '/api/orden_servicio/' + id,
				type: 'PUT',
				dataType: 'json',
				data: putData,
				processData: false,
				contentType: false,
				success: function(data) {
					consultarOrdenesServicio();
					swal('Empleado asignado exitosamente', '', 'success');
					//$('#modalAsignarEmpleado').modal('toggle');
					
				},
				error: function(xhr) {
					swal('Ocurrió un error',
							xhr.status + ' ' + xhr.statusText + ': ' +
							xhr.responseJSON.data.message,
							'error');
				}
			}
		);

}



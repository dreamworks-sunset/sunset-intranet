if (typeof jQuery === "undefined") {
    throw new Error("jQuery plugins need to be before this file");
}

$.AdminBSB = {};
$.AdminBSB.options = {
    colors: {
        red: '#F44336',
        pink: '#E91E63',
        purple: '#9C27B0',
        deepPurple: '#673AB7',
        indigo: '#3F51B5',
        blue: '#2196F3',
        lightBlue: '#03A9F4',
        cyan: '#00BCD4',
        teal: '#009688',
        green: '#4CAF50',
        lightGreen: '#8BC34A',
        lime: '#CDDC39',
        yellow: '#ffe821',
        amber: '#FFC107',
        orange: '#FF9800',
        deepOrange: '#FF5722',
        brown: '#795548',
        grey: '#9E9E9E',
        blueGrey: '#607D8B',
        black: '#000000',
        white: '#ffffff'
    },
    leftSideBar: {
        scrollColor: 'rgba(0,0,0,0.5)',
        scrollWidth: '4px',
        scrollAlwaysVisible: false,
        scrollBorderRadius: '0',
        scrollRailBorderRadius: '0',
        scrollActiveItemWhenPageLoad: true,
        breakpointWidth: 1170
    },
    dropdownMenu: {
        effectIn: 'fadeIn',
        effectOut: 'fadeOut'
    }
}

/* Left Sidebar - Function =================================================================================================
*  You can manage the left sidebar menu options
*  
*/
$.AdminBSB.leftSideBar = {
    activate: function () {
        var _this = this;
        var $body = $('body');
        var $overlay = $('.overlay');

        //Close sidebar
        $(window).click(function (e) {
            var $target = $(e.target);
            if (e.target.nodeName.toLowerCase() === 'i') { $target = $(e.target).parent(); }

            if (!$target.hasClass('bars') && _this.isOpen() && $target.parents('#leftsidebar').length === 0) {
                if (!$target.hasClass('js-right-sidebar')) $overlay.fadeOut();
                $body.removeClass('overlay-open');
            }
        });

        $.each($('.menu-toggle.toggled'), function (i, val) {
            $(val).next().slideToggle(0);
        });

        //When page load
        $.each($('.menu .list li.active'), function (i, val) {
            var $activeAnchors = $(val).find('a:eq(0)');

            $activeAnchors.addClass('toggled');
            $activeAnchors.next().show();
        });

        //Collapse or Expand Menu
        $('.menu-toggle').on('click', function (e) {
            var $this = $(this);
            var $content = $this.next();

            if ($($this.parents('ul')[0]).hasClass('list')) {
                var $not = $(e.target).hasClass('menu-toggle') ? e.target : $(e.target).parents('.menu-toggle');

                $.each($('.menu-toggle.toggled').not($not).next(), function (i, val) {
                    if ($(val).is(':visible')) {
                        $(val).prev().toggleClass('toggled');
                        $(val).slideUp();
                    }
                });
            }

            $this.toggleClass('toggled');
            $content.slideToggle(320);
        });

        //Set menu height
        _this.setMenuHeight();
        _this.checkStatuForResize(true);
        $(window).resize(function () {
            _this.setMenuHeight();
            _this.checkStatuForResize(false);
        });

        //Set Waves
        Waves.attach('.menu .list a', ['waves-block']);
        Waves.init();
    },
    setMenuHeight: function (isFirstTime) {
        if (typeof $.fn.slimScroll != 'undefined') {
            var configs = $.AdminBSB.options.leftSideBar;
            var height = ($(window).height() - ($('.legal').outerHeight() + $('.user-info').outerHeight() + $('.navbar').innerHeight()));
            var $el = $('.list');

            $el.slimscroll({
                height: height + "px",
                color: configs.scrollColor,
                size: configs.scrollWidth,
                alwaysVisible: configs.scrollAlwaysVisible,
                borderRadius: configs.scrollBorderRadius,
                railBorderRadius: configs.scrollRailBorderRadius
            });

            //Scroll active menu item when page load, if option set = true
            if ($.AdminBSB.options.leftSideBar.scrollActiveItemWhenPageLoad) {
                var activeItem  = $('.menu .list li.active')[0];
                if(activeItem != undefined) {
                    var activeItemOffsetTop = activeItem.offsetTop
                    if (activeItemOffsetTop > 150) $el.slimscroll({ scrollTo: activeItemOffsetTop + 'px' });
                }
            }
        }
    },
    checkStatuForResize: function (firstTime) {
        var $body = $('body');
        var $openCloseBar = $('.navbar .navbar-header .bars');
        var width = $body.width();

        if (firstTime) {
            $body.find('.content, .sidebar').addClass('no-animate').delay(1000).queue(function () {
                $(this).removeClass('no-animate').dequeue();
            });
        }

        if (width < $.AdminBSB.options.leftSideBar.breakpointWidth) {
            $body.addClass('ls-closed');
            $openCloseBar.fadeIn();
        }
        else {
            $body.removeClass('ls-closed');
            $openCloseBar.fadeOut();
        }
    },
    isOpen: function () {
        return $('body').hasClass('overlay-open');
    }
};
//==========================================================================================================================

/* Right Sidebar - Function ================================================================================================
*  You can manage the right sidebar menu options
*  
*/
$.AdminBSB.rightSideBar = {
    activate: function () {
        var _this = this;
        var $sidebar = $('#rightsidebar');
        var $overlay = $('.overlay');

        //Close sidebar
        $(window).click(function (e) {
            var $target = $(e.target);
            if (e.target.nodeName.toLowerCase() === 'i') { $target = $(e.target).parent(); }

            if (!$target.hasClass('js-right-sidebar') && _this.isOpen() && $target.parents('#rightsidebar').length === 0) {
                if (!$target.hasClass('bars')) $overlay.fadeOut();
                $sidebar.removeClass('open');
            }
        });

        $('.js-right-sidebar').on('click', function () {
            $sidebar.toggleClass('open');
            if (_this.isOpen()) { $overlay.fadeIn(); } else { $overlay.fadeOut(); }
        });
    },
    isOpen: function () {
        return $('.right-sidebar').hasClass('open');
    }
}
//==========================================================================================================================

/* Searchbar - Function ================================================================================================
*  You can manage the search bar
*  
*/
var $searchBar = $('.search-bar');
$.AdminBSB.search = {
    activate: function () {
        var _this = this;

        //Search button click event
        $('.js-search').on('click', function () {
            _this.showSearchBar();
        });

        //Close search click event
        $searchBar.find('.close-search').on('click', function () {
            _this.hideSearchBar();
        });

        //ESC key on pressed
        $searchBar.find('input[type="text"]').on('keyup', function (e) {
            if (e.keyCode == 27) {
                _this.hideSearchBar();
            }
        });
    },
    showSearchBar: function () {
        $searchBar.addClass('open');
        $searchBar.find('input[type="text"]').focus();
    },
    hideSearchBar: function () {
        $searchBar.removeClass('open');
        $searchBar.find('input[type="text"]').val('');
    }
}
//==========================================================================================================================

/* Navbar - Function =======================================================================================================
*  You can manage the navbar
*  
*/
$.AdminBSB.navbar = {
    activate: function () {
        var $body = $('body');
        var $overlay = $('.overlay');

        //Open left sidebar panel
        $('.bars').on('click', function () {
            $body.toggleClass('overlay-open');
            if ($body.hasClass('overlay-open')) { $overlay.fadeIn(); } else { $overlay.fadeOut(); }
        });

        //Close collapse bar on click event
        $('.nav [data-close="true"]').on('click', function () {
            var isVisible = $('.navbar-toggle').is(':visible');
            var $navbarCollapse = $('.navbar-collapse');

            if (isVisible) {
                $navbarCollapse.slideUp(function () {
                    $navbarCollapse.removeClass('in').removeAttr('style');
                });
            }
        });
    }
}
//==========================================================================================================================

/* Input - Function ========================================================================================================
*  You can manage the inputs(also textareas) with name of class 'form-control'
*  
*/
$.AdminBSB.input = {
    activate: function () {
        //On focus event
        $('.form-control').focus(function () {
            $(this).parent().addClass('focused');
        });

        //On focusout event
        $('.form-control').focusout(function () {
            var $this = $(this);
            if ($this.parents('.form-group').hasClass('form-float')) {
                if ($this.val() == '') { $this.parents('.form-line').removeClass('focused'); }
            }
            else {
                $this.parents('.form-line').removeClass('focused');
            }
        });

        //On label click
        $('body').on('click', '.form-float .form-line .form-label', function () {
            $(this).parent().find('input').focus();
        });

        //Not blank form
        $('.form-control').each(function () {
            if ($(this).val() !== '') {
                $(this).parents('.form-line').addClass('focused');
            }
        });
    }
}
//==========================================================================================================================

/* Form - Select - Function ================================================================================================
*  You can manage the 'select' of form elements
*  
*/
$.AdminBSB.select = {
    activate: function () {
        if ($.fn.selectpicker) { $('select:not(.ms)').selectpicker(); }
    }
}
//==========================================================================================================================

/* DropdownMenu - Function =================================================================================================
*  You can manage the dropdown menu
*  
*/

$.AdminBSB.dropdownMenu = {
    activate: function () {
        var _this = this;

        $('.dropdown, .dropup, .btn-group').on({
            "show.bs.dropdown": function () {
                var dropdown = _this.dropdownEffect(this);
                _this.dropdownEffectStart(dropdown, dropdown.effectIn);
            },
            "shown.bs.dropdown": function () {
                var dropdown = _this.dropdownEffect(this);
                if (dropdown.effectIn && dropdown.effectOut) {
                    _this.dropdownEffectEnd(dropdown, function () { });
                }
            },
            "hide.bs.dropdown": function (e) {
                var dropdown = _this.dropdownEffect(this);
                if (dropdown.effectOut) {
                    e.preventDefault();
                    _this.dropdownEffectStart(dropdown, dropdown.effectOut);
                    _this.dropdownEffectEnd(dropdown, function () {
                        dropdown.dropdown.removeClass('open');
                    });
                }
            }
        });

        //Set Waves
        Waves.attach('.dropdown-menu li a', ['waves-block']);
        Waves.init();
    },
    dropdownEffect: function (target) {
        var effectIn = $.AdminBSB.options.dropdownMenu.effectIn, effectOut = $.AdminBSB.options.dropdownMenu.effectOut;
        var dropdown = $(target), dropdownMenu = $('.dropdown-menu', target);

        if (dropdown.length > 0) {
            var udEffectIn = dropdown.data('effect-in');
            var udEffectOut = dropdown.data('effect-out');
            if (udEffectIn !== undefined) { effectIn = udEffectIn; }
            if (udEffectOut !== undefined) { effectOut = udEffectOut; }
        }

        return {
            target: target,
            dropdown: dropdown,
            dropdownMenu: dropdownMenu,
            effectIn: effectIn,
            effectOut: effectOut
        };
    },
    dropdownEffectStart: function (data, effectToStart) {
        if (effectToStart) {
            data.dropdown.addClass('dropdown-animating');
            data.dropdownMenu.addClass('animated dropdown-animated');
            data.dropdownMenu.addClass(effectToStart);
        }
    },
    dropdownEffectEnd: function (data, callback) {
        var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
        data.dropdown.one(animationEnd, function () {
            data.dropdown.removeClass('dropdown-animating');
            data.dropdownMenu.removeClass('animated dropdown-animated');
            data.dropdownMenu.removeClass(data.effectIn);
            data.dropdownMenu.removeClass(data.effectOut);

            if (typeof callback == 'function') {
                callback();
            }
        });
    }
}
//==========================================================================================================================

/* Browser - Function ======================================================================================================
*  You can manage browser
*  
*/
var edge = 'Microsoft Edge';
var ie10 = 'Internet Explorer 10';
var ie11 = 'Internet Explorer 11';
var opera = 'Opera';
var firefox = 'Mozilla Firefox';
var chrome = 'Google Chrome';
var safari = 'Safari';

$.AdminBSB.browser = {
    activate: function () {
        var _this = this;
        var className = _this.getClassName();

        if (className !== '') $('html').addClass(_this.getClassName());
    },
    getBrowser: function () {
        var userAgent = navigator.userAgent.toLowerCase();

        if (/edge/i.test(userAgent)) {
            return edge;
        } else if (/rv:11/i.test(userAgent)) {
            return ie11;
        } else if (/msie 10/i.test(userAgent)) {
            return ie10;
        } else if (/opr/i.test(userAgent)) {
            return opera;
        } else if (/chrome/i.test(userAgent)) {
            return chrome;
        } else if (/firefox/i.test(userAgent)) {
            return firefox;
        } else if (!!navigator.userAgent.match(/Version\/[\d\.]+.*Safari/)) {
            return safari;
        }

        return undefined;
    },
    getClassName: function () {
        var browser = this.getBrowser();

        if (browser === edge) {
            return 'edge';
        } else if (browser === ie11) {
            return 'ie11';
        } else if (browser === ie10) {
            return 'ie10';
        } else if (browser === opera) {
            return 'opera';
        } else if (browser === chrome) {
            return 'chrome';
        } else if (browser === firefox) {
            return 'firefox';
        } else if (browser === safari) {
            return 'safari';
        } else {
            return '';
        }
    }
}
//==========================================================================================================================

$(function () {
    $.AdminBSB.browser.activate();
    $.AdminBSB.leftSideBar.activate();
    $.AdminBSB.rightSideBar.activate();
    $.AdminBSB.navbar.activate();
    $.AdminBSB.dropdownMenu.activate();
    $.AdminBSB.input.activate();
    $.AdminBSB.select.activate();
    $.AdminBSB.search.activate();

    setTimeout(function () { $('.page-loader-wrapper').fadeOut(); }, 50);
});


//De aquí en adelante está el código que implementa la seguridad funcional
//en todos los HTML

//Página actual donde estoy ubicado
var pagina_actual = window.location.pathname.split('/').pop();

if(pagina_actual != "IniciarSesion.html" && localStorage.sesion == 'true') {
    $.ajax(
        {
            url: HOST + '/api/usuario/' + localStorage.idUsuario,
            type: 'GET',
            async: false,
            success: function(result) {
                var usuarioStr = JSON.stringify(result.data);

                //En caso de que el usuario o sus registros relacionados hayan cambiado
                //mientras su sesión estaba activa, actualizar localStorage.usuario
                if(localStorage.usuario != usuarioStr) {
                    localStorage.usuario = usuarioStr
                }
            },
            error: function(xhr) {
                if(xhr.responseJSON == undefined) {
                    alert('Ocurrió un error: No se pudo conectar con el servidor');
                    return false;
                } else {
                    alert('Ocurrió un error: ' +
                    xhr.status + ' ' + xhr.statusText + ': ' +
                    xhr.responseJSON.data.message);
                }
            }
        }
    );
    usuario = JSON.parse(localStorage.usuario);

    //Si el usuario que inició sesión no tiene ningún permiso asignado, redirigir a página de error
    if(usuario.rol.menu_opcion.length == 0) {
        localStorage.errorEstatus = 403;
        localStorage.errorMensaje = 'No tienes permiso para acceder a Sunset-intranet.';
        
        //Como el usuario no tiene ningún permiso, no hay necesidad de almacenar su información.
        //Se limpia el localStorage cuando se cargue la página de error
        localStorage.limpiar = true;
        window.location.href = "error.html";
    }

    var menus = usuario.rol.menu_opcion;
    var acceso = false;

    //Si la página es "Manual.html", conceder acceso por defecto
    if(pagina_actual == 'Manual.html') {
        acceso = true;
    } else {
        //Busco si la página actual está entre los permisos asignados al usuario
        $.each(menus, function(index, data) {
            if(data.url == pagina_actual) {
                acceso = true;
                return false; //Instrucción equivalente a 'break' para salir del ciclo 'each'
            }
        });
    }

    //Si el usuario no tiene permiso para acceder a la página actual, redirigir a página de error
    if(!acceso) {
        localStorage.errorEstatus = 403;
        localStorage.errorMensaje = 'No tienes permiso para acceder a esta página.';
        window.location.href = "error.html";
    }

    //Cargar datos de usuario en el menú lateral
    $('.email').text(usuario.correo);
    if(usuario.empleado.id != undefined) {
        $('.name').text(usuario.empleado.nombres + ' ' + usuario.empleado.apellidos);
        if(usuario.empleado.imagen != undefined) {
            $('.image img').attr('src', usuario.empleado.imagen);
        }
    } else if(usuario.cliente.id != undefined) {
        $('.name').text(usuario.cliente.nombres + ' ' + usuario.cliente.apellidos)
        if(usuario.cliente.imagen != undefined) {
            $('.image img').attr('src', usuario.cliente.imagen);
        }
    } else {
        $('.name').text(usuario.correo);
        $('.email').text('');
    }

    //Diccionario de menús padres
    var padres = {};

    //Limpiar menú antes de cargarlo
    $('.menu ul.list').html('<li class="header">MENÚ PRINCIPAL</li>');

    //Variable que permitirá controlar cuándo mostrar la opción de configuración del negocio
    var negocio = false;
    var vistas_no_menu = [];
    //Itero por cada menú al que tiene acceso el usuario
    $.each(usuario.rol.menu_opcion, function(index, data) {

        //Si el permiso no corresponde a un menú del panel, pasar a la siguiente iteracón
        if(!data.es_menu) {
            vistas_no_menu.push(data.url);
            return true; //Instrucción equivalente al 'continue' del ciclo 'each'
        }

        //Si el usuario tiene permiso a la configuración del negocio, pasar a la siguiente iteración
        if(data.url == 'Negocio.html') {
            negocio = true;
            return true; //Instrucción equivalente al 'continue' del ciclo 'each'
        }

        //Si la URL del menú es la página actual, marcar menú como activo
        var active = (pagina_actual == data.url) ? ' class="active"' : '';

        //Obtengo el menú padre del menú actual (si existe)
        var idPadre = (data.menu_opcion_padre != undefined) ? data.menu_opcion_padre.id : '';
        var nombrePadre = (idPadre != '') ? data.menu_opcion_padre.funcion : '';
        var iconPadre = (idPadre != '') ? data.menu_opcion_padre.material_icon : '';

        if(idPadre != '') {
            //Guardo en el diccionario 'padres' los datos del menú padre
            padres[idPadre] = {
                funcion: nombrePadre,
                material_icon: iconPadre
            }

            //Obtengo el menú abuelo (padre del padre) del menú actual (si existe)
            /*var idAbuelo = (data.menu_opcion_padre.menu_opcion_padre != undefined) ? data.menu_opcion_padre.menu_opcion_padre.id : '';
            var nombreAbuelo = (idAbuelo != '') ? data.menu_opcion_padre.menu_opcion_padre.funcion : '';
            var iconAbuelo = (idAbuelo != '') ? data.menu_opcion_padre.menu_opcion_padre.material_icon : '';

            if(idAbuelo != '') {
                //Guardo en el diccionario 'padres' los datos del menú abuelo
                padres[idAbuelo] = {
                    funcion: nombreAbuelo,
                    material_icon: iconAbuelo
                }
            }*/
        }

        //Añado el menú al panel lateral
        $('.menu ul.list').append(
            '<li' + active + ' data-id-padre="' + idPadre + /*'" data-id-abuelo="' + idAbuelo +*/'">' +
                '<a href="' + data.url + '">' +
                    '<i class="material-icons">' + data.material_icon + '</i>' +
                    '<span>' + data.funcion + '</span>' +
                '</a>' +
            '</li>'
        );
    });

    localStorage.vistasNoMenu = JSON.stringify(vistas_no_menu);

    //Colocar a los menús padres por encima de sus hijos
    $.each(padres, function(key, value) {
        $('li[data-id-padre="' + key + '"]').wrapAll(
            '<li data-id-padre-externo="' + key + '">' +
                '<ul class="ml-menu"></ul>' +
            '</li>'
        );
        $('li[data-id-padre-externo="' + key + '"]').prepend(
            '<a href="javascript:void(0);" class="menu-toggle">' +
                '<i class="material-icons">' + value.material_icon + '</i>' +
                '<span>' + value.funcion + '</span>' +
            '</a>'
        );

        /*$('li[data-id-abuelo="' + key + '"]').parent().parent().wrapAll(
            '<li data-id-padre-externo="' + key + '">' +
                '<ul class="ml-menu"></ul>' +
            '</li>'
        );
        $('li[data-id-padre-externo="' + key + '"]').parent().prepend(
            '<a href="javascript:void(0);" class="menu-toggle">' +
                '<i class="material-icons">' + value.material_icon + '</i>' +
                '<span>' + value.funcion + '</span>' +
            '</a>'
        );*/
    });

    //$.AdminBSB.leftSideBar.activate();

    //Si el usuario no tiene acceso a la configuración del negocio, remover la opción del navbar
    if(!negocio) {
        $('.navbar .container-fluid .collapse.navbar-collapse ul li:first-child').remove();
    }

    //Preparar menú contextual de usuario en el panel lateral
    $('.user-info .info-container .btn-group.user-helper-dropdown .dropdown-menu.pull-right').html(
        '<li><a href="javascript:void(0);"><i class="material-icons">person</i>Perfil</a></li>' +
        '<li role="seperator" class="divider"></li>' +
        '<li onclick="cerrarSesion()"><a href="javascript:void(0);"><i class="material-icons">input</i>Cerrar sesión</a></li>'
    );

    function cerrarSesion() {
        localStorage.clear();
        window.location.href = "IniciarSesion.html";
    }
}


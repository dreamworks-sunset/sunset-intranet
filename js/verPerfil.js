//const HOST = 'http://localhost:3000';

$(document).ready(function() {

       // consultarPerfil();

        consultarCliente(localStorage.idCliente);
        consultarPerfilClientes(localStorage.idCliente);

    $('#cmbTipoParametro').change(function() {

        $('#valorNumerico').hide();
        var idTipoParametro = $(this).val();
        consultarParametrosPorTipoParametro(idTipoParametro);
        $('#cmbValorParametro').html('<option value="" selected disabled hidden>Valor Parámetro</option>');
        $('#cmbValorParametro').selectpicker('refresh');
        

    });
    $('#cmbParametro').change(function() {
        var idValorParametro = $(this).val();
        consultarValorParametroPorParametro(idValorParametro);

    });

    $('#cmbValorParametro').change(function() {
        var idClasificacion = $(this).val();
        consultarClasificacionValorParametro(idClasificacion);

    });


    $('#btnAnadirValor').click(function() {
        var tipoParametro = $('#cmbTipoParametro option:selected');
        var parametro = $('#cmbParametro option:selected');
        var valorParametro = $('#cmbValorParametro option:selected');
        var valor = $('#txtValor').val();

        // Voy a añadir un insumo, elimino la fila "No se han añadido insumos"
        $('#sinValor').remove();
        

        // Añade un insumo a la tabla
        $('#tablaPerfil tbody').append(
            '<tr data-id-tipo-parametro="' + tipoParametro.val() + '" data-id-parametro="' +
            parametro.val() + '" data-id-valor-parametro="' + valorParametro.val() + '">' +
                '<td>' + tipoParametro.text() + '</td>' +
                '<td>' + parametro.text() + '</td>' +
                '<td>' + valorParametro.text() + '</td>' +
               
                (($('#valorNumerico').is(':hidden')) ?  '<td></td> <td class="centrado"> <button type="button" rel="tooltip"  title="Eliminar" class="btn btn-danger btn-circle waves-effect waves-circle waves-float perfil-tabla" onclick=""> <i class="material-icons">delete</i> </button> </td>' :  '<td>' + valor + ' Kg.</td> <td class="centrado"> <button type="button" rel="tooltip"  title="Editar" class="btn btn-primary btn-circle waves-effect waves-circle waves-float perfil-tabla" onclick=""> <i class="material-icons">edit</i> </button></td>')+
               
            '</tr>'
        );
        limpiarValorParametro();

            var idValorParametroPorAnadir = $('#tablaPerfil tbody tr:last-child').data('id-valor-parametro');
            var idClientePorAnadir = $('#tablaPerfil tbody tr:last-child').data('id-cliente');
            var valorPorAnadir = $('#tablaPerfil tbody tr:last-child').data('valor');

            values[idValorParametroPorAnadir] = {
                id_valor_parametro: idValorParametroPorAnadir,
                id_cliente: idClientePorAnadir,
                valor_concreto: valorPorAnadir
            }
            valuesPerfilPorEliminar[idValorParametroPorAnadir] = false;





        $('[rel="tooltip"]').tooltip();                                         

        // Manejo del evento 'click' que elimina una fila de la tabla
        $('.perfil-tabla').click(function() {
            // Elimina la fila que contiene al icono 'delete' cliqueado
            $(this).parent().parent().remove();

            // Este if evalúa si el tbody de la tabla se quedó sin filas
            if($('#tablaPerfil tbody tr').length === 0) {
                $('#tablaPerfil tbody').html(
                    '<tr id="sinValor">' +
                        '<td class="centrado" colspan="5">' +
                            '<i>No se han añadido datos al perfil</i>' +
                        '</td>' +
                    '</tr>'
                );
            }
            // Fin if
        });
        // Fin $('.perfil-tabla').click(...)
    });
  // Evento click del botón para guardar el detalle de la sesión
   });



function consultarTipoParametros() {
     
    $.ajax(
    {
        url: HOST + '/api/tipo_parametro',
        type: 'GET',
        success: function(result) {
            $('#cmbTipoParametro').html('<option value="" selected disabled hidden>Tipo Parámetro</option>');
            $.each(result.data, function(index, data) {
                $('#cmbTipoParametro').append('<option data-id-tipo-parametro="' + data.id_tipo_parametro + '" value="' + data.id + '">' + data.descripcion + '</option>')
            });
            $('#cmbTipoParametro').selectpicker('refresh');
        },
        error: function(xhr) {
            console.log('Ocurrió un error: ' + xhr.status + ' ' + xhr.statusText);
        }
      }
    );

}

function consultarParametrosPorTipoParametro(id) {
       
    $.ajax(
    {

        url: HOST + '/api/tipo_parametro/'+id,
        type: 'GET',
        success: function(result) {
            $('#cmbParametro').html('<option value="" selected disabled hidden>Parámetro</option>');
            $.each(result.data.parametro, function(index, data) {
                $('#cmbParametro').append('<option value="' + data.id + '">' + data.descripcion + '</option>')
            });
            $('#cmbParametro').selectpicker('refresh');
        },
        error: function(xhr) {
            console.log('Ocurrió un error: ' + xhr.status + ' ' + xhr.statusText);
        }
      }
    );

}

function consultarValorParametroPorParametro(id) {
       
    $.ajax(
    {

        url: HOST + '/api/parametro/'+id,
        type: 'GET',
        success: function(result) {
            $('#cmbValorParametro').html('<option value="" selected disabled hidden>Valor Parámetro</option>');
            $.each(result.data.valor_parametro, function(index, data) {
                $('#cmbValorParametro').append('<option value="' + data.id + '">' + data.descripcion + '</option>')
            });
            $('#cmbValorParametro').selectpicker('refresh');

        },
        error: function(xhr) {
            console.log('Ocurrió un error: ' + xhr.status + ' ' + xhr.statusText);
        }
      }

    );

}

function consultarClasificacionValorParametro(id){
       
    $.ajax(
    {

        url: HOST + '/api/valor_parametro/'+id,
        type: 'GET',
        success: function(result) {
           if(result.data.clasificacion_parametro.descripcion == "Real"){
                   $('#valorNumerico').show();
           }
                 
        },
        error: function(xhr) {
            console.log('Ocurrió un error: ' + xhr.status + ' ' + xhr.statusText);
        }
      }

    );

}

function consultarCliente(idCliente){
    $.ajax(
        {
            url: HOST + '/api/cliente/'+ idCliente,
            type: 'GET',
            success: function(result) {
               

                        $('#spanNombre').text( result.data.nombres+' '+ result.data.apellidos);
                        $('#spanCedula').text( result.data.cedula);
                        $('#spanDireccion').text(result.data.direccion);
                        $('#spanFechaNacimiento').text(formatFecha(result.data.fecha_nacimiento));
                        $('#spanTelefono').text(result.data.telefono);
                        $('#imgImagen').attr('src',result.data.imagen);
                  

                

               
            },
            error: function(xhr) {
                swal('Ocurrió un error',
                        xhr.status + ' ' + xhr.statusText + ': ' +
                        xhr.responseJSON.data.message,
                        'error');
            }
        }
    );
}

function consultarPerfilClientes(idCliente) {
    $.ajax(
        {
            url: HOST + '/api/cliente/' + idCliente,
            type: 'GET',
            success: function(result) {
                //Destruir la tabla para luego reinicializarla más adelante
                if($.fn.DataTable.isDataTable("#tablaPerfil")) {
                    $('#tablaPerfil').DataTable().clear().destroy();
                }
                //$('#tablaPerfil tbody').empty();
                $.each(result.data.perfil, function(index, data) {
                        
                    $('#tablaPerfil tbody').append(
                        '<tr data-id-perfil="' + data.id + '">' +
                            '<td>' + data.valor_parametro.parametro.tipo_parametro.descripcion + '</td>' +
                            '<td>' + data.valor_parametro.parametro.descripcion+ '</td>' +
                            '<td>' + data.valor_parametro.descripcion+ '</td>' +
                            '<td data-valor="' + data.valor_concreto + '">' + ((data.valor_concreto != null) ? data.valor_concreto : '') + '</td>' +
                            '<td data-unidad="' + data.valor_parametro.id_unidad + '">' + ((data.valor_parametro.id_unidad != null) ? data.valor_parametro.unidad.abreviatura : '') + '</td>' +

                        '</tr>'
                    );

                });
                //Aquí se reinicializa la tabla
                $(".js-basic-example").dataTable({
                    responsive: true
                });

            
            },
            error: function(xhr) {
                swal('Ocurrió un error',
                        xhr.status + ' ' + xhr.statusText + ': ' +
                        xhr.responseJSON.data.message,
                        'error');
            }
        }
    );
}
/*function consultarPerfil() {
    $.ajax(
        {
            url: HOST + '/api/perfil',
            type: 'GET',
            success: function(result) {
                $('#tablaPerfil tbody').empty();
                $.each(result.data, function(index, data) {
                    
                        
                    $('#tablaPerfil tbody').append(
                        
                       '<tr id="' + data.id + '">' +
                            '<td>' + data.tipo_parametro.descripcion + '</td>' +
                            '<td>' + data.parametro.descripcion + '</td>' +
                            '<td>' + data.valor_parametro.descripcion + '</td>' +
                            '<td>' + data.clasificacion_parametro.descripcion + '</td>' +
                           '<td>' +
                            '</tr>'
                    );
                });
                $('#tablaPerfil tbody tr').css('cursor', 'pointer');
                $('td[data-estatus="1"]').addClass('habilitado');
                $('td[data-estatus="0"]').addClass('deshabilitado');
            },
            error: function(xhr) {
                swal('Ocurrió un error',
                        xhr.status + ' ' + xhr.statusText + ': ' +
                        xhr.responseJSON.data.message,
                        'error');
            }
        }
    );
}*/


function formatFecha(date) {
    //Ejemplo: '2018-04-27T04:34:03.687Z'
    var fechaHoraStr = String(date).split('T'); //Separa la fecha de la hora
    var fecha = fechaHoraStr[0].split('-'); //Separa año, mes y día

    //Concatena cada valor de fecha en formato dd/mm/yyyy
    return fecha[2] + '/' + fecha[1] + '/' + fecha[0];
}


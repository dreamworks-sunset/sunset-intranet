

$(document).ready(function() {

	limpiarCampos();
	consultarParametro();
	consultarTipo_parametro();

	$('#btnGuardar').click(function() {
		var form = document.getElementById("formParametro");
		var postData = new FormData(form);
		var IdTipo_parametro = $('#txtIdTipo_parametro').attr('data-id-tipo_parametro');
		postData.append('id_tipo_parametro', IdTipo_parametro);
		console.log(IdTipo_parametro);
		$.ajax(
			{
				url: HOST + '/api/parametro',
				type: 'POST',
				dataType: 'json',
				data: postData,
				processData: false,
				contentType: false,
				success: function(data) {
					swal('Registro exitoso', '', 'success');
					limpiarCampos();
					consultarParametro();
				},
				error: function(xhr) {
					if(xhr.responseJSON == undefined) {
						swal('Ocurrió un error', 'No se pudo conectar con el servidor', 'error');
						return;
					}
					swal('Ocurrió un error',
							xhr.status + ' ' + xhr.statusText + ': ' +
							xhr.responseJSON.data.message,
							'error');
				}
			}
		);
	});

	$('#btnModificar').click(function() {
		var form = document.getElementById("formParametro");
		var putData = new FormData(form);
		var IdTipo_parametro = $('#txtIdTipo_parametro').attr('data-id-tipo_parametro');
		putData.append('id_tipo_parametro', IdTipo_parametro);
		var id = $('#txtId').val();
		$.ajax(
			{
				url: HOST + '/api/parametro/' + id,
				type: 'PUT',
				dataType: 'json',
				data: putData,
				processData: false,
				contentType: false,
				success: function(data) {
					swal('Modificación exitosa', '', 'success');
					limpiarCampos();
					consultarParametro();
				},
				error: function(xhr) {
					swal('Ocurrió un error',
							xhr.status + ' ' + xhr.statusText + ': ' +
							xhr.responseJSON.data.message,
							'error');
				}
			}
		);
	});

	$('#btnHabilitar').click(function() {
		var putData = new FormData();
		putData.append('estatus', '1');
		var id = $('#txtId').val();
		$.ajax(
			{
				url: HOST + '/api/parametro/' + id,
				type: 'PUT',
				dataType: 'json',
				data: putData,
				processData: false,
				contentType: false,
				success: function(data) {
					swal('Habilitación exitosa', '', 'success');
					limpiarCampos();
					consultarParametro();
				},
				error: function(xhr) {
					swal('Ocurrió un error',
							xhr.status + ' ' + xhr.statusText + ': ' +
							xhr.responseJSON.data.message,
							'error');
				}
			}
		);
	});

	$('#btnDeshabilitar').click(function() {
		var putData = new FormData();
		putData.append('estatus', '0');
		var id = $('#txtId').val();
		$.ajax(
			{
				url: HOST + '/api/parametro/' + id,
				type: 'PUT',
				dataType: 'json',
				data: putData,
				processData: false,
				contentType: false,
				success: function(data) {
					swal('Deshabilitación exitosa', '', 'success');
					limpiarCampos();
					consultarParametro();
				},
				error: function(xhr) {
					swal('Ocurrió un error',
							xhr.status + ' ' + xhr.statusText + ': ' +
							xhr.responseJSON.data.message,
							'error');
				}
			}
		);
	});

	$('#btnCancelar').click(function() {
		limpiarCampos();
	});

});

function seleccionarFila(el) {
	var idParametro = $(el).attr('data-id-parametro');
	$('#txtId').val(idParametro);
	$('#txtDescripcion').val($(el).children('td').eq(0).text());
	$('#txtIdTipo_parametro').val($(el).children('td').eq(1).text());
	$('#txtIdTipo_parametro').attr('data-id-tipo_parametro', $(el).attr('data-id-tipo_parametro'));
	var habilitado = $(el).children('td').eq(2).data('estatus');
	if(habilitado == 1) {
		$('#btnDeshabilitar').show();
		$('#btnHabilitar').hide();
	} else {
		$('#btnDeshabilitar').hide();
		$('#btnHabilitar').show();
	}
	$('#btnGuardar').hide();
	$('#btnModificar').show();
}

function seleccionarTipo_parametro(el) {
	$('#txtIdTipo_parametro').attr('data-id-tipo_parametro', $(el).attr('data-id-tipo_parametro'));
	$('#txtIdTipo_parametro').val($(el).children('td').eq(0).text());
	$('#ModalAsignarTipoParametro').modal('toggle');

}

function consultarParametro() {
	$.ajax(
		{
			url: HOST + '/api/parametro',
			type: 'GET',
			success: function(result) {
				//Destruir la tabla para luego reinicializarla más adelante
                if($.fn.DataTable.isDataTable("#tablaParametro")) {
                    $('#tablaParametro').DataTable().clear().destroy();
                }
				//$('#tablaParametro tbody').empty();
				$.each(result.data, function(index, data) {
					$('#tablaParametro tbody').append(
						'<tr data-id-parametro="' + data.id + '" data-id-tipo_parametro="' + data.id_tipo_parametro + '"onclick="seleccionarFila(this)">' +
							'<td>' + data.descripcion + '</td>' +
							'<td>' + data.tipo_parametro.descripcion + '</td>' +
							'<td data-estatus="' + data.estatus + '">' + ((data.estatus == 1) ? 'Sí' : 'No') + '</td>' +
						'</tr>'
					);
				});
				$('.js-exportable').DataTable({
                    dom: 'Bfrtip',
                    responsive: true,
                    buttons: [
                        'copy', 'csv', 'excel', 'pdf', 'print'
                    ]
                });
				$('#tablaParametro tbody tr').css('cursor', 'pointer');
				$('td[data-estatus="1"]').addClass('habilitado');
				$('td[data-estatus="0"]').addClass('deshabilitado');
			},
			error: function(xhr) {
				swal('Ocurrió un error',
						xhr.status + ' ' + xhr.statusText + ': ' +
						xhr.responseJSON.data.message,
						'error');
			}
		}
	);
}

function consultarTipo_parametro(){
	$.ajax(
		{
			url: HOST + '/api/tipo_parametro',
			type: 'GET',
			success: function(result) {
				$('#tablaTipo_parametro tbody').empty();
				$.each(result.data, function(index, data) {
					$('#tablaTipo_parametro tbody').append(
						'<tr data-id-tipo_parametro ="' + data.id + '" onclick="seleccionarTipo_parametro(this)">' +
							'<td>' + data.descripcion + '</td>' +
						
			

						'</tr>'
					);
				});
				$('#tablaTipo_parametro tbody tr').css('cursor', 'pointer');
			},
			error: function(xhr) {
				swal('Ocurrió un error',
						xhr.status + ' ' + xhr.statusText + ': ' +
						xhr.responseJSON.data.message,
						'error');
			}
		}
	);
}

function limpiarCampos() {
	$('#txtId').val("")
	$('#txtDescripcion').val("");
	$('#txtIdTipo_parametro').val("");
	$('#btnGuardar').show();
	$('#btnModificar, #btnHabilitar, #btnDeshabilitar').hide();
}

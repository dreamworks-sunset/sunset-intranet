$(document).ready(function() {

    $('input[type="radio"][name="reclamoProcede"]').change(function() {
        if($('#reclamoSi').prop("checked")) {
            $('#agendarGarantia').show();
        }
    
        if($('#reclamoNo').prop("checked")) {
            $('#agendarGarantia').hide();
        }
    });
    
});
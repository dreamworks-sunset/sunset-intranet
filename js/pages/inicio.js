$(document).ready(function() {

    $('[rel="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();

    $('[rel="tooltip"]').css("cursor", "pointer");

    $('input[type="radio"][name="reclamoProcede"]').change(function() {
        if($('#reclamoSi').prop("checked")) {
            $('#agendarGarantia').show();
        }
    
        if($('#reclamoNo').prop("checked")) {
            $('#agendarGarantia').hide();
        }
    });
    

    $('#btn-solicitudes').click(function() {
        $('#tabla-solicitudes').show();
        $('#tabla-incidencias').hide();
        $('#tabla-reclamos').hide();
        $('#tabla-sugerencias').hide();
    });

    $('#btn-incidencias').click(function() {
        $('#tabla-solicitudes').hide();
        $('#tabla-incidencias').show();
        $('#tabla-reclamos').hide();
        $('#tabla-sugerencias').hide();
    });

    $('#btn-reclamos').click(function() {
        $('#tabla-solicitudes').hide();
        $('#tabla-incidencias').hide();
        $('#tabla-reclamos').show();
        $('#tabla-sugerencias').hide();
    });

    $('#btn-sugerencias').click(function() {
        $('#tabla-solicitudes').hide();
        $('#tabla-incidencias').hide();
        $('#tabla-reclamos').hide();
        $('#tabla-sugerencias').show();
    });

});

$(document).ready(function() {

	consultarOrdenServicio();
	
});


function consultarCita(idOrdenServicio) {
	$.ajax(

		{
			url: HOST + '/api/cita',
			type: 'GET',
			success: function(result) {
				$('#tablaCita tbody').empty();
				var cont = 0;
				$.each(result.data, function(index, data) {
					var botonPendiente =  '<button type="button" id="btnRegistrarDetalleSesion' + data.id + '" '+
										  'class="btn btn-warning btn-circle waves-effect waves-circle waves-float">' +
                                    	  '<i class="material-icons">content_paste</i></button>'	;
					
					var boton =  '<button type="button" id="btnVerDetalleSesion' + data.id +'" ' +
								 'class="btn btn-success btn-circle waves-effect waves-circle waves-float">' +
                                 '<i class="material-icons">search</i></button>'	;

					if(data.orden_servicio.id == idOrdenServicio){
						var estatus = '';

						if(data.estatus == 1){
	                    	estatus = 'Pendiente';
	                    }else if(data.estatus == 2){
	                    	estatus = 'Finalizada';
	                    	cont++;
	                    }else{
	                    	estatus = 'Suspendida';
	                    }

						$('#tablaCita tbody').append(
						'<tr data-id-cita="' + data.id + '" data-id-sesion="'+ data.sesion.id +'" data-id-orden-servicio="' + data.orden_servicio.id + '" data-id-empleado="' + data.id_empleado + '">' +
							'<td>' + formatFechaHora(data.fecha) + '</td>' +
								'<td>' + data.agenda.horario_empleado.empleado.nombres + ' ' + data.agenda.horario_empleado.empleado.apellidos+ '</td>' +
							'<td>' + estatus + '</td>'+
							'<td>' + ((data.estatus == 1) ? botonPendiente : boton) + '</td>'+
						'</tr>'
						);
					}
					
				
					$('#btnVerDetalleSesion'+ data.id).click(function(){
							localStorage.idCita = $(this).parent().parent().data('id-cita');
							localStorage.verSesion =1
							window.location.href = "RegistrarDetalleSesion.html";

						});

					$('#btnRegistrarDetalleSesion'+ data.id).click(function(){
							localStorage.idCita = $(this).parent().parent().data('id-cita');
							localStorage.verSesion = 0
							window.location.href = "RegistrarDetalleSesion.html";

						});
						$('#spanCantidadSesionesRealizadas').text(cont);

				$('#tablaCita tbody tr').css('cursor', 'pointer');
				});

				

			},
			error: function(xhr) {
				swal('Ocurrió un error',
						xhr.status + ' ' + xhr.statusText + ': ' +
						xhr.responseJSON.data.message,
						'error');
			}
		}
	);
}



	

function consultarOrdenServicio() {
	$.ajax(
		{
			url: HOST + '/api/orden_servicio',
			type: 'GET',
			success: function(result) {
				//Destruir la tabla para luego reinicializarla más adelante
                if($.fn.DataTable.isDataTable("#tablaOrdenServicio")) {
                    $('#tablaOrdenServicio').DataTable().clear().destroy();
                }
				//$('#tablaOrdenServicio tbody').empty();
				$.each(result.data, function(index, data) {
					if (data.estatus == 2){
						
					$('#tablaOrdenServicio tbody').append(
						'<tr id="' + data.id + '" data-id-servicio="' + data.solicitud.servicio.id + '" data-cantidad="' + data.solicitud.servicio.cantidad_sesiones + '">' +
							'<td>' + data.id + '</td>' +
							'<td>' + data.solicitud.cliente.nombres + ' ' + data.solicitud.cliente.apellidos + '</td>' +
							'<td>' + data.solicitud.servicio.nombre + '</td>' +
							'<td>' + formatFechaHora(data.fecha_creacion) + '</td>' +
							'<td>' +
							    '<button type="button" rel="tooltip" data-toggle="modal" ' +
										'data-target="#modalSesion" data-placement="top" title="Ver Sesiones" ' +
										'data-container="body" title="Responder" ' +
										'onclick="cargarOrdenServicio(this)" ' +
										'class="btn btn-success btn-circle waves-effect waves-circle waves-float">' +
                                    '<i class="material-icons">search</i>' +
                                '</button>' +              
                            '</td>'+
						'</tr>'
					);
				
				}

				$('#tablaOrdenServicio tbody tr').css('cursor', 'pointer');

				
				});

				//Aquí se reinicializa la tabla
                $(".js-basic-example").dataTable({
                    responsive: true
                });

			
			},
			error: function(xhr) {
				swal('Ocurrió un error',
						xhr.status + ' ' + xhr.statusText + ': ' +
						xhr.responseJSON.data.message,
						'error');
			}
		}
	);
}


function formatFecha(date) {
    //Ejemplo: '2018-04-27T04:34:03.687Z'
    var fechaHoraStr = String(date).split('T'); //Separa la fecha de la hora
    var fecha = fechaHoraStr[0].split('-'); //Separa año, mes y día

    //Concatena cada valor de fecha en formato dd/mm/yyyy
    return fecha[2] + '/' + fecha[1] + '/' + fecha[0];
}
function formatFechaHora(date) {
    //Ejemplo: '2018-04-27T04:34:03.687Z'
    var fechaHoraStr = String(date).split('T'); //Separa la fecha de la hora
    var fecha = fechaHoraStr[0].split('-'); //Separa año, mes y día

    //Concatena cada valor de fecha en formato dd/mm/yyyy
    var f = fecha[2] + '/' + fecha[1] + '/' + fecha[0];

    var hora = fechaHoraStr[1].split(':'); //Separa hora, minuto y segundo
    var h = parseInt(hora[0]); //Convierte hora a entero en formato 24h
    var min = parseInt(hora[1]);
    var a = (h < 12) ? 'AM' : 'PM'; //Si h es menor a 12 es AM, si no, PM
    if(h == 0) {
        h = 12;
    } else if(h > 12) {
        h = (h % 12 < 10) ? '0' + (h % 12) : (h % 12);
    }
    min = (min < 10) ? '0' + min : min;
    return f + ' ' + h + ':' + min + ' ' + a;
}


function cargarOrdenServicio(el) {
	var id = $(el).parent().parent().attr('id');
	var cantidadSesiones = $(el).parent().parent().attr('data-cantidad');
	consultarCita(id);
	var fila = $(el).parent().parent().children();
	var nroOrden = $(fila).eq(0).text();
	var cliente = $(fila).eq(1).text();
	var servicio= $(fila).eq(2).text();
	$('#spanCliente').text(cliente);
	$('#spanServicio').text(servicio);
	$('#spanNroOrden').text(nroOrden);
	$('#spanCantidadSesiones').text(cantidadSesiones);

}   


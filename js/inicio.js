

$(document).ready(function() {
	consultarSolicitud();
	consultarReclamo();
	consultarIncidencia();
    consultarCitas();
    generarGrafica();
	
});

function generarGrafica() {
    $.ajax(
        {
            url: HOST + '/api/vista_dashboard1',
            type: 'GET',
            success: function(result) {
                var exitosas = result.data[0].ordenes_exitosas;
                var no_exitosas = result.data[0].ordenes_no_exitosas;
                $('.sparkline-pie').text(no_exitosas + ',' + exitosas);
                $('.sparkline-pie').sparkline('html', {
                    type: 'pie',
                    offset: 90,
                    width: '150px',
                    height: '150px',
                    sliceColors: ['#E91E63', '#00BCD4', '#FFC107']
                })
            },
            error: function(xhr) {
                swal('Ocurrió un error',
                        xhr.status + ' ' + xhr.statusText + ': ' +
                        xhr.responseJSON.data.message,
                        'error');
            }
        }
    );
}


function consultarSolicitud(){

    $.ajax(
        {
            url: HOST + '/api/vista_dashboard1',
            type: 'GET',
            success: function(result) {
            	
               

                        $('#spanSolicitudh').text( result.data[0].solicitudes_hoy);
                        $('#spanSolicituda').text( result.data[0].solicitudes_ayer);
                        $('#spanSolicitudl').text( result.data[0].solicitudes_semana_pasada);
                        $('#spanSolicitude').text( result.data[0].solicitudes_mes_pasado);
                        $('#spanSolicitudt').text( result.data[0].total_solicitudes);
						
                  

                

               
            },
            error: function(xhr) {
                swal('Ocurrió un error',
                        xhr.status + ' ' + xhr.statusText + ': ' +
                        xhr.responseJSON.data.message,
                        'error');
            }
        }
    );
}


function consultarReclamo(){

    $.ajax(
        {
            url: HOST + '/api/vista_dashboard1',
            type: 'GET',
            success: function(result) {
            	
               

                        $('#spanComentariosh').text( result.data[0].comentario_hoy);
                        $('#spanComentariosa').text( result.data[0].comentarios_ayer);
                        $('#spanComentariosl').text( result.data[0].comentarios_semana_pasada);
                        $('#spanComentariose').text( result.data[0].comentarios_mes_pasado);
                        $('#spanComentariost').text( result.data[0].total_comentarios);
						
                  

                

               
            },
            error: function(xhr) {
                swal('Ocurrió un error',
                        xhr.status + ' ' + xhr.statusText + ': ' +
                        xhr.responseJSON.data.message,
                        'error');
            }
        }
    );
}

function consultarIncidencia(){

    $.ajax(
        {
            url: HOST + '/api/vista_dashboard1',
            type: 'GET',
            success: function(result) {
            	
               

                        $('#spanReclamosh').text( result.data[0].reclamos_hoy);
                        $('#spanReclamosa').text( result.data[0].reclamos_ayer);
                        $('#spanReclamosl').text( result.data[0].reclamos_semana_pasada);
                        $('#spanReclamose').text( result.data[0].reclamos_mes_pasado);
                        $('#spanReclamost').text( result.data[0].total_reclamos);
						
                  

                

               
            },
            error: function(xhr) {
                swal('Ocurrió un error',
                        xhr.status + ' ' + xhr.statusText + ': ' +
                        xhr.responseJSON.data.message,
                        'error');
            }
        }
    );
}

function consultarCitas() {
	$.ajax(
		{
			url: HOST + '/api/vista_citas_del_dia',
			type: 'GET',
			success: function(result) {
                //Destruir la tabla para luego reinicializarla más adelante
                if($.fn.DataTable.isDataTable("#tablaVistaCitas")) {
                    $('#tablaVistaCitas').DataTable().clear().destroy();
                }
				//$('#tablaVistaCitas tbody').empty();
				$.each(result.data, function(index, data) {
					$('#tablaVistaCitas tbody').append(
						'<tr>'+
                            '<td>' + data.id_orden_servicio + '</td>' +
							'<td>' + data.nombres_cliente + '  ' + data.apellidos_cliente + '</td>' +
							'<td>' + data.nombre + '</td>' +
							'<td>' + data.nombres_empleado + '  ' + data.apellidos_empleado + '</td>' +
							
						'</tr>'
					);
                });
                //Aquí se reinicializa la tabla
                $(".js-basic-example").dataTable({
                    responsive: true
                });
				$('#tablaVistaCitas tbody tr').css('cursor', 'pointer');
				$('td[data-estatus="1"]').addClass('habilitado');
				$('td[data-estatus="0"]').addClass('deshabilitado');
			},
			error: function(xhr) {
				swal('Ocurrió un error',
						xhr.status + ' ' + xhr.statusText + ': ' +
						xhr.responseJSON.data.message,
						'error');
			}
		}
	);
}








$(document).ready(function() {

        consultarTipoServicios();
        consultarReporte();        
        consultarSexo(); 
        consultarEdad();   
        //getMorris('donut', 'donut_chart');


         $('#cmbTipoServicio').change(function() {

         var idTipoServicio = $(this).val();
         consultarServiciosPorTipoServicio(idTipoServicio);
                 });

          $('#cmbServicio').change(function() {

         var idServicio = $(this).val();
         consultarPromocionesPorServicio(idServicio);
                 });



        $("#tablaReporte").hide();

        $('#btnGenerar').click(function() {
            $("#tablaReporte").show();
            consultarReporte();
            
        });
         $('#btnCancelar').click(function(){
            $("#tablaReporte").hide();

        });

      


});




function consultarReporte() {
    $.ajax(
        {
            url: HOST + '/api/vista_reporte_estructurado_solicitud_servicio',
            type: 'GET',
            success: function(result) {
                 //var colors = {};

                //Destruir la tabla para luego reinicializarla más adelante
                if($.fn.DataTable.isDataTable("#tablaReporteSolicitud")) {
                    $('#tablaReporteSolicitud').DataTable().clear().destroy();
                }
                //$('#tablaReporteSolicitud tbody').empty();
                    
             


                $.each(result.data, function(index, data) {
               
                var Servicio = $('#cmbServicio').val();
                var idPromocion = $('#cmbPromocion option:selected').val();
                var Edad = $('#cmbEdad').val();
                var Sexo = $('#cmbSexo').val();
                var fecha_ini = formatFechaDMYToYMD($('#dtpFechaInicial').val());
                var fecha_fin = formatFechaDMYToYMD($('#dtpFechaFinal').val());
                var fecha_creacion = new Date(data.fecha);

   
                //Filtrar por fecha
                if(fecha_creacion.getTime() < fecha_ini.getTime() || fecha_creacion.getTime() > fecha_fin.getTime()) {
                    return true; //continúa en la siguiente iteración
                }

                //Filtrar por servicio
                if(Servicio != 0 && $('#cmbServicio option:selected').text() != data.servicio) {
                    return true; //continúa en la siguiente iteración
                }
                 //Filtrar por promocion
                if(idPromocion != 0 && idPromocion != data.id_promocion) {
                    return true; //continúa en la siguiente iteración
                }
                //Filtrar por rano edad
                if(Edad != 0 && $('#cmbEdad option:selected').text() != data.rango) {
                    return true; //continúa en la siguiente iteración
                }
                //Filtrar por sexo
                if(Sexo != 0 && $('#cmbSexo option:selected').text() != data.sexo) {
                    return true; //continúa en la siguiente iteración
                }
             

                    $('#tablaReporteSolicitud tbody').append(
                        '<tr >' +
                            '<td>' + ((data.tipo_de_servicio != null) ? data.tipo_de_servicio : '') + '</td>' +
                            '<td>' + ((data.servicio != null) ? data.servicio : '') + '</td>' +
                            '<td>' + ((data.sexo != null) ? data.sexo : '') + '</td>' + 
                            '<td>' + ((data.rango != null) ? data.rango : '') + '</td>'+ 
                            '<td>' + ((data.nombre_promocion != null) ? data.nombre_promocion : '') + '</td>' +
                          //  '<td>' + formatFecha(data.fecha_creacion) + '</td>' +
                        '</tr>'
                    );
                    
                   

                 


                });
                $('.js-exportable').DataTable({
                    dom: 'Bfrtip',
                    responsive: true,
                    buttons: [
                        'copy', 'csv', 'excel', 'pdf', 'print'
                    ]
                });
             
            },
            error: function(xhr) {
                swal('Ocurrió un error',
                        xhr.status + ' ' + xhr.statusText + ': ' +
                        xhr.responseJSON.data.message,
                        'error');
            }
        }
    );
}

function consultarTipoServicios() {
     
    $.ajax(
    {
        url: HOST + '/api/tipo_servicio',
        type: 'GET',
        success: function(result) {
            $('#cmbTipoServicio').html('<option value="" selected disabled hidden>Tipo Servicio</option>');
            $.each(result.data, function(index, data) {
                $('#cmbTipoServicio').append('<option data-id-tipo-servicio="' + data.id_tipo_servicio + '" value="' + data.id + '">' + data.descripcion + '</option>')
            });
            $('#cmbTipoServicio').selectpicker('refresh');
        },
        error: function(xhr) {
            console.log('Ocurrió un error: ' + xhr.status + ' ' + xhr.statusText);
        }
      }
    );

}

function consultarServiciosPorTipoServicio(id) {
        $('#cmbServicio').html('<option value="0">Todos</option>');
       
    $.ajax(
    {

        url: HOST + '/api/tipo_servicio/'+id,
        type: 'GET',
        success: function(result) {
            $.each(result.data.servicio, function(index, data) {
                $('#cmbServicio').append('<option value="' + data.id + '">' + data.nombre + '</option>')
            });
            $('#cmbServicio').selectpicker('refresh');
        },
        error: function(xhr) {
            console.log('Ocurrió un error: ' + xhr.status + ' ' + xhr.statusText);
        }
      }
    );

}
function consultarPromocionesPorServicio(id) {
        $('#cmbPromocion').html('<option value="0">Todos</option>');
       
    $.ajax(
    {

        url: HOST + '/api/servicio/'+id,
        type: 'GET',
        success: function(result) {
            $.each(result.data.promocion, function(index, data) {
                $('#cmbPromocion').append('<option value="' + data.id + '">' + data.nombre + '</option>')
            });
            $('#cmbPromocion').selectpicker('refresh');
        },
        error: function(xhr) {
            console.log('Ocurrió un error: ' + xhr.status + ' ' + xhr.statusText);
        }
      }
    );

}
function consultarSexo() {
        $('#cmbSexo').html('<option value="0">Todos</option>');
       
    $.ajax(
    {

        url: HOST + '/api/parametro/'+1,
        type: 'GET',
        success: function(result) {
            $.each(result.data.valor_parametro, function(index, data) {
                $('#cmbSexo').append('<option value="' + data.id + '">' + data.descripcion+ '</option>')
            });
            $('#cmbSexo').selectpicker('refresh');
        },
        error: function(xhr) {
            console.log('Ocurrió un error: ' + xhr.status + ' ' + xhr.statusText);
        }
      }
    );

}
function consultarEdad() {
        $('#cmbEdad').html('<option value="0">Todos</option>');
       
    $.ajax(
    {

        url: HOST + '/api/parametro/'+3,
        type: 'GET',
        success: function(result) {
            $.each(result.data.valor_parametro, function(index, data) {
                $('#cmbEdad').append('<option value="' + data.id + '">' + data.descripcion+ '</option>')
            });cmbEdad
            $('#cmbEdad').selectpicker('refresh');
        },
        error: function(xhr) {
            console.log('Ocurrió un error: ' + xhr.status + ' ' + xhr.statusText);
        }
      }
    );

}

function formatFecha(date) {
    //Ejemplo: '2018-04-27T04:34:03.687Z'
    var fechaHoraStr = String(date).split('T'); //Separa la fecha de la hora
    var fecha = fechaHoraStr[0].split('-'); //Separa año, mes y día

    //Concatena cada valor de fecha en formato dd/mm/yyyy
    return fecha[2] + '/' + fecha[1] + '/' + fecha[0];
}
function getRandomColor() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 3; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
  }
  function formatFechaDMYToYMD(date) {
    var fecha = date.split('/');
    var fec = new Date(fecha[2], parseInt(fecha[1]) - 1, fecha[0]);
    return fec;
}
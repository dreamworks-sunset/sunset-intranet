$(document).ready(function () {
    consultarServiciosSolicitados();
    $("#tablaReporte").hide();
    
    $("#grafica").hide();


    $('#btnGenerar').click(function() {
        $("#tablaReporte").show();
        $("#grafica").show();
        $('#bar_chart').empty();
        consultarReporteDemandaDeServicios();
        
    });
     $('#btnCancelar').click(function() {
        $("#tablaReporte").hide();
        $("#grafica").hide();
    });
});

function getMorris(type, element, datos) {
     if (type === 'bar') {
        Morris.Bar({
            element: element,
            /*data: [{
                rango: ' <21',
                y: 24,
                z: 2,
             

            },
            {
                rango: ' 22-60',
                y: 50,
                z: 4,
             

            },
             {
                    rango: ' >60',
                    y: 20,
                    z: 5,
                }],*/
            data: datos,
            xkey: 'rango',
            ykeys: ['mujeres', 'hombres'],
            labels: ['mujeres','hombres'],
            barColors: ['rgb(233, 30, 99)', 'rgb(0, 188, 212)'],
        });
    }
}

function consultarReporteDemandaDeServicios(){
    $.ajax(
        {
            url: HOST + '/api/vista_reporte_demanda_de_servicios',
            type: 'GET',
            destroy: true,
            async: false,
            success: function(result) {
                var datos = [];
                var criterios = {};
                //var colors = {};

                //Destruir la tabla para luego reinicializarla más adelante
                if($.fn.DataTable.isDataTable("#tablaReporteDemandaServicios")) {
                    $('#tablaReporteDemandaServicios').DataTable().clear().destroy();
                }
                //$('#tablaReporteDemandaServicios tbody').empty(); //Esta línea ya no es necesario con lo anterior

                var annoInicial = $('#annoInicial').val();
                var mesInicial = $('#mesInicial').val();
                var annoFinal = $('#annoFinal').val();
                var mesFinal = $('#mesFinal').val();
                var fechaIni = new Date(annoInicial, parseInt(mesInicial) - 1);
                var fechaFin = new Date(annoFinal, parseInt(mesFinal) - 1);
                $.each(result.data, function(index, data) {
                    var fecha = new Date(data.anno, parseInt(data.mes) - 1);
                    if(fecha.getTime() < fechaIni.getTime() || fecha.getTime() > fechaFin.getTime()) {
                        return true;
                    }

                    var idServicio = $('#cmbServicio').val();
                    if(idServicio != 0 && $('#cmbServicio option:selected').text() != data.servicio) {
                        return true;
                    }
                    $('#tablaReporteDemandaServicios tbody').append(
                        '<tr>' +
                            '<td>' + data.anno + '</td>' +
                            '<td>' + data.mes + '</td>' +
                            '<td>' + data.rango + '</td>' +
                            '<td>' + data.servicio + '</td>' +
                            '<td>' + data.hombres + '</td>' +
                            '<td>' + data.mujeres + '</td>' +
                        '</tr>'
                    );
                    if(criterios[data.rango] == undefined) {
                        criterios[data.rango] = {
                            'mujeres' : parseInt(data.mujeres),
                            'hombres' : parseInt(data.hombres)
                        }
                    } else {
                        criterios[data.rango]['mujeres'] += parseInt(data.mujeres);
                        criterios[data.rango]['hombres'] += parseInt(data.hombres);
                    }
                    //fechas[data.anno + '-' + ((data.mes < 10) ? '0' + data.mes : data.mes)] = data.anno + '-' + ((data.mes < 10) ? '0' + data.mes : data.mes);
                    //colors[data.rango] = getRandomColor();                    
                });

                //Aquí se reinicializa la tabla
                $(".js-basic-example").dataTable({
                    responsive: true
                });
                $.each(criterios, function(index, data) {
                    var obj = {};
                    obj['rango'] = index;
                    obj['mujeres'] = data.mujeres;
                    obj['hombres'] = data.hombres;
                    datos.push(obj);
                });
                getMorris('bar', 'bar_chart', datos);
                
            },
            error: function(xhr) {
                swal('Ocurrió un error',
                        xhr.status + ' ' + xhr.statusText + ': ' +
                        xhr.responseJSON.data.message,
                        'error');
            }
        }
    );

}

function getRandomColor() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 3; i++) {
      color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}

function consultarServiciosSolicitados() {
    $('#cmbServicio').html('<option value="0">Todos</option>');
    $.ajax(
    {
        url: HOST + '/api/solicitud',
        type: 'GET',
        success: function(result) {
            $.each(result.data, function(index, data) {
                var servicio = data.servicio;
                if($('#cmbServicio option[value="' + servicio.id + '"]').length == 0) {
                    $('#cmbServicio').append(
                        '<option value="' + servicio.id + '">' + servicio.nombre + '</option>'
                    );
                }
            });
            $('#cmbServicio').selectpicker('refresh');
        },
        error: function(xhr) {
            swal('Ocurrió un error',
                    xhr.status + ' ' + xhr.statusText + ': ' +
                    xhr.responseJSON.data.message,
                    'error');
        }
    });
}
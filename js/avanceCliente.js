

let valuesAvance = {}; //Diccionario donde se almacenarán los valores parametros de un cliente 
let valuesAvancePorEliminar = {} //Diccionario donde se almacenarán los valores parametrosde un cliente que serán removidos de la base de datos


$(document).ready(function() {

        consultarSesion(localStorage.idNewSesion);
        console.log(localStorage.idNewSesion);
        consultarTipoAvance();
        
        $('#cmbTipoParametro').change(function() {

        $('#valorNumerico').hide();
        var idTipoParametro = $(this).val();
        consultarParametrosPorTipoParametro(idTipoParametro);
        $('#cmbValorParametro').html('<option value="" selected disabled hidden>Valor Parámetro</option>');
        $('#cmbValorParametro').selectpicker('refresh');
        

    });

    $('#cmbParametro').change(function() {
        var idValorParametro = $(this).val();
        consultarValorParametroPorParametro();

    });

    $('#btnGuardarAvanceCliente').click(function() {
        var form = document.getElementById("formAvance");
        var putData = new FormData(form);
        id = $('#txtIdSesion').val();
        var avance = Object.values(valuesAvance);
        var valoresAvance = [];
        avance.forEach(valor => {
            if(valor) {
                valoresAvance.push(valor);
            }
        });
        console.log('avance =',valoresAvance);
        putData.append('avance', JSON.stringify(valoresAvance));

        $.ajax(
            {
                url: HOST + '/api/sesion/' + id,
                type: 'PUT',
                dataType: 'json',
                data: putData,
                processData: false,
                contentType: false,
                success: function(data) {
                   swal({
                    title: "¡Cambios guardados exitosamente!",
                    type: "success",
                    text: "En breve será redirigido a la agenda",
                    timer: 2000,
                    showConfirmButton: false
                }, function () {                
                   window.location.replace('Agenda.html');
                });
                },
                error: function(xhr) {
                    if(xhr.responseJSON == undefined) {
                        swal('Ocurrió un error', 'No se pudo conectar con el servidor', 'error');
                        return;
                    }
                    swal('Ocurrió un error',
                            xhr.status + ' ' + xhr.statusText + ': ' +
                            xhr.responseJSON.data,
                            'error');
                }
            }
        );
    });


    $('#btnAnadirValor').click(function() {
        var tipoAvance = $('#cmbTipoAvance option:selected');
        var parametro = $('#cmbParametroAvance option:selected');
        var valorParametro = $('#cmbValorParametro option:selected');
        var valor = $('#txtValor').val();

        // Voy a añadir un insumo, elimino la fila "No se han añadido insumos"
        $('#sinValor').remove();
        

        // Añade un insumo a la tabla
        $('#tablaAvance tbody').append(
            '<tr data-id-tipo-avance="' + tipoAvance.val() + '" data-id-parametro="' +
            parametro.val() + '" data-id-valor-parametro="' + valorParametro.val() + '">' +
                '<td>' + tipoAvance.text() + '</td>' +
                '<td>' + parametro.text() + '</td>' +
                '<td>' + valorParametro.text() + '</td>' +
               
                (($('#valorNumerico').is(':hidden')) ?  '<td></td> <td class="centrado"> <button type="button" rel="tooltip"  title="Eliminar" class="btn btn-danger btn-circle waves-effect waves-circle waves-float perfil-tabla" onclick=""> <i class="material-icons">delete</i> </button> </td>' :  '<td>' + valor + ' </td> <td class="centrado"> <button type="button" rel="tooltip"  title="Editar" class="btn btn-primary btn-circle waves-effect waves-circle waves-float perfil-tabla" onclick=""> <i class="material-icons">edit</i> </button></td>')+
               
            '</tr>'
        );
        limpiarValorParametro();

            var idTipoAvancePorAnadir = $('#tablaAvance tbody tr:last-child').data('id-tipo-avance');
            var idValorPorAnadir = $('#tablaAvance tbody tr:last-child').data('id-valor-parametro');
            var idClientePorAñadir = $('#txtIdCliente').val()
            var idSesionPorAñadir = $('#txtIdSesion').val()
            var numeroSesionPorAñadir = $('#txtNumeroSesion').val()

            valuesAvance[idValorPorAnadir] = {
                id_valor_parametro: idValorPorAnadir,
                id_tipo_avance: idTipoAvancePorAnadir,
                id_cliente : idClientePorAñadir,
                id_sesion : idSesionPorAñadir,
                numero_sesion : numeroSesionPorAñadir 
            }
            valuesAvancePorEliminar[idTipoAvancePorAnadir] = false;




        $('[rel="tooltip"]').tooltip();                                         

        // Manejo del evento 'click' que elimina una fila de la tabla
        $('.perfil-tabla').click(function() {
            // Elimina la fila que contiene al icono 'delete' cliqueado
            $(this).parent().parent().remove();

            // Este if evalúa si el tbody de la tabla se quedó sin filas
            if($('#tablaPerfil tbody tr').length === 0) {
                $('#tablaPerfil tbody').html(
                    '<tr id="sinValor">' +
                        '<td class="centrado" colspan="5">' +
                            '<i>No se han añadido datos al perfil</i>' +
                        '</td>' +
                    '</tr>'
                );
            }
            // Fin if
        });
        // Fin $('.perfil-tabla').click(...)
    });
  // Evento click del botón para guardar el detalle de la sesión


});

function consultarTipoAvance() {
     
    $.ajax(
    {
        url: HOST + '/api/tipo_avance',
        type: 'GET',
        success: function(result) {
            $('#cmbTipoAvance').html('<option value="0" selected disabled hidden>Tipo Avance</option>');
            $.each(result.data, function(index, data) {
                $('#cmbTipoAvance').append('<option value="' + data.id + '">' + data.descripcion + '</option>')
            });
            $('#cmbTipoAvance').selectpicker('refresh');
        },
        error: function(xhr) {
            console.log('Ocurrió un error: ' + xhr.status + ' ' + xhr.statusText);
        }
      }
    );
}

function consultarTipoParametro(idServicio) {
     
    $.ajax(
    {
        url: HOST + '/api/tipo_parametro/4',
        type: 'GET',
        success: function(result) {
            $('#cmbParametroAvance').html('<option value="0" selected disabled hidden>Parametro</option>');
           
            $.each(result.data.parametro, function(index, data) {
                $.each(data.valor_parametro, function(index, data2) {
                    var encontrado = false;
                    $.each(data2.servicio_avance, function(index, data3) {
                       if(data3.id == idServicio){
                            encontrado =true;
                            //return false;
                       }
               
                     })
                     if(encontrado && $('#cmbParametroAvance option[value="' + data.id + '"]').length == 0) {
                            $('#cmbParametroAvance').append('<option value="' + data.id + '">' + data.descripcion + '</option>')
                            $('#cmbParametroAvance').selectpicker('refresh');
                    }
                })

                });
        },
        error: function(xhr) {
            console.log('Ocurrió un error: ' + xhr.status + ' ' + xhr.statusText);
        }
      }
    );
}





function consultarSesion(idSesion){
    $.ajax(
        {
            url: HOST + '/api/sesion/'+ idSesion,
            type: 'GET',
            success: function(result) {
                $('#txtNumeroSesion').val(result.data.numero_sesion)
                $('#txtIdCliente').val(result.data.cita.orden_servicio.solicitud.cliente.id)
                $('#txtIdSesion').val(idSesion)

                $('#txtNombre').text(result.data.cita.orden_servicio.solicitud.cliente.nombres +' '+result.data.cita.orden_servicio.solicitud.cliente.apellidos);
                $('#txtCedula').text('V-'+result.data.cita.orden_servicio.solicitud.cliente.cedula);
                $('#imgImagen').attr('src',result.data.cita.orden_servicio.solicitud.cliente.imagen);
                $('#spanServicio').text(result.data.cita.orden_servicio.solicitud.servicio.nombre);
                $('#spanEmpleadoAsignado').text(result.data.cita.agenda.horario_empleado.empleado.nombres +' '+result.data.cita.agenda.horario_empleado.empleado.apellidos);
                $('#spanFecha').text(formatFechaHora(result.data.cita.fecha));
                $('#spanNroSesion').text(result.data.numero_sesion);  
                idServicio = result.data.cita.orden_servicio.solicitud.servicio.id
                consultarTipoParametro(idServicio)
                
                
                $('#cmbParametroAvance').change(function() {
                    var idParametro = $(this).val();
                     consultarValorParametroAvancePorServicio(idServicio,idParametro);  

                });
               
            },
            error: function(xhr) {
                swal('Ocurrió un error',
                        xhr.status + ' ' + xhr.statusText + ': ' +
                        xhr.responseJSON.data.message,
                        'error');
            }
        }
    );
}


function consultarValorParametroAvancePorServicio(id,parametro){
       
    $.ajax(
    {

        url: HOST + '/api/servicio/'+id,
        type: 'GET',
        success: function(result) {
            console.log('paramteo cambio',parametro)
            $('#cmbValorParametro').html('<option value="0" selected disabled hidden> Valor parametro </option>');

            $.each(result.data.valor_parametro_avance, function(index, data) {

                if(parametro == data.parametro.id){
                    $('#cmbValorParametro').append('<option value="' + data.id + '">' + data.descripcion + '</option>');
                    $('#cmbValorParametro').selectpicker('refresh');
                    }
                 });
        },
        error: function(xhr) {
            console.log('Ocurrió un error: ' + xhr.status + ' ' + xhr.statusText);
        }
      }

    );

}

function formatHora(time) {
    //Ejemplo: '04:34:00'
    var hora = time.split(':'); //Separa hora, minuto y segundo
    var h = parseInt(hora[0]); //Convierte hora a entero en formato 24h
    var min = parseInt(hora[1]);
    var a = (h < 12) ? 'AM' : 'PM'; //Si h es menor a 12 es AM, si no, PM
    if(h == 0) {
        h = 12;
    } else if(h > 12) {
        h = (h % 12 < 10) ? '0' + (h % 12) : (h % 12);
    }
    min = (min < 10) ? '0' + min : min;
    return h + ':' + min + ' ' + a;
}


function formatFecha(date) {
    //Ejemplo: '2018-04-27T04:34:03.687Z'
    var fechaHoraStr = String(date).split('T'); //Separa la fecha de la hora
    var fecha = fechaHoraStr[0].split('-'); //Separa año, mes y día

    //Concatena cada valor de fecha en formato dd/mm/yyyy
    return fecha[2] + '/' + fecha[1] + '/' + fecha[0];
}
function formatFechaHora(date) {
    //Ejemplo: '2018-04-27T04:34:03.687Z'
    var fechaHoraStr = String(date).split('T'); //Separa la fecha de la hora
    var fecha = fechaHoraStr[0].split('-'); //Separa año, mes y día

    //Concatena cada valor de fecha en formato dd/mm/yyyy
    var f = fecha[2] + '/' + fecha[1] + '/' + fecha[0];

    var hora = fechaHoraStr[1].split(':'); //Separa hora, minuto y segundo
    var h = parseInt(hora[0]); //Convierte hora a entero en formato 24h
    var min = parseInt(hora[1]);
    var a = (h < 12) ? 'AM' : 'PM'; //Si h es menor a 12 es AM, si no, PM
    if(h == 0) {
        h = 12;
    } else if(h > 12) {
        h = (h % 12 < 10) ? '0' + (h % 12) : (h % 12);
    }
    min = (min < 10) ? '0' + min : min;
    return f + ' ' + h + ':' + min + ' ' + a;
}

function limpiarValorParametro() {
    $('#txtValor').val("");
    $('#cmbTipoAvance').selectpicker('val', 0);
    $('#cmbParametroAvance').selectpicker('val', 0);
    $('#cmbValorParametro').selectpicker('val', 0);
    
}